package com.homecaretimesheet.apidemo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class User {
    public int id = 0;
    public String userType;
    public int userSet;
    public String email;
    public List<SessionData> sessions;
    public List<CareOption> careOpts;
    public Agency agency;
    public File signature = null;
    public String amazonUrl;
    public List<Recipient> recipients;
    public List<Ratio> ratios;
    public List<Service> services;
    public List<SharedCareLocation> sharedCareLocations;
    public PCA pca;

    public void setUser(UserData userData) {
        this.id = userData.id;
        this.userType = userData.userType.toUpperCase();
        this.email = userData.email;
        if (this.userType.equals("PCA")) {
            this.userSet = 1;
        }
        else {
            this.userSet = 2;
        }
    }
    public void setCareOpts(List<CareOption> careOpts) {
        this.careOpts = careOpts;
    }

    public void setSignature(File file) {
        this.signature = file;
    }
    public void setSessions(List<SessionData> sessions) {
        if (this.userSet == 1) {
            this.sessions = sessions;
        }
        else
        {
            ArrayList<SessionData> sessionArr = new ArrayList<>();
            if (sessions.size() > 0) {
                for (SessionData session : sessions) {
                    TimeSheet timeSheet = session.timesheets.get(session.timesheets.size() - 1);
                    if (timeSheet.pca_signature != null && timeSheet.finished.equals(0) && timeSheet.file.timesheetPcaSig != null) {
                        sessionArr.add(session);
                    }
                }
            }
            this.sessions = sessionArr;
        }
    }

    public void setServices(List<Service> services) { this.services = services; }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    public void setAmazon(String amazonUrl) {
        this.amazonUrl = amazonUrl;
    }

    public void setRecips(List<Recipient> recips) {
        this.recipients = recips;
    }

    public void setRatios(List<Ratio> ratios) {
        this.ratios = ratios;
    }

    public void setCareLocations(List<SharedCareLocation> locs) {
        this.sharedCareLocations = locs;
    }

    public void setPCA(PCA pca) {
        this.pca = pca;
    }
}