package com.homecaretimesheet.apidemo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.mime.TypedFile;


public class SignAll extends Activity  implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public TimeSheetStorage store;
    public Context context;
    public int sdkVersion;
    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
    private ApiService apiService;
    private ResponseHandler respHandler;
    private User user;
    protected ApiService.DataService datafetch;
    protected String api_token;
    public GoogleApiClient mGoogleApiClient;
    public Integer fileRowId = 0;
    private Location currentLocation = null;
    private String mLatitudeText = null;
    private String mLongitudeText = null;
    private int i = 0;
    private globalHCTS.ServiceChoice serviceChoice;
    private globalHCTS globals;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = SignAll.this;
        store = new TimeSheetStorage(this);
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        setContentView(R.layout.activity_pdf);
        respHandler = new ResponseHandler();
        globals = (globalHCTS) getApplicationContext();
        user = globals.user;
        serviceChoice = globals.serviceChoice;
        api_token = store.getDataString(TSDatum.API_TOKEN);
        getData();
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void getData() {
        apiService = new ApiService(api_token);
        datafetch = apiService.restAdapter.create(ApiService.DataService.class);
        datafetch.getData(dataResponse);
    }

    Callback<JsonObject> dataResponse = new Callback<JsonObject>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                Toast.makeText(context, "Login Failed", sdkVersion).show();
            }
        }
        @Override
        public void success(JsonObject loginResponse, retrofit.client.Response response) {
            respHandler.getResp(loginResponse, context);
            signAll();
        }
    };

    public void sendImage() {
        ApiService.SendRSig sendSig = apiService.restAdapter.create(ApiService.SendRSig.class);
        File image = getFileStreamPath("signatureC.png");
        user.setSignature(image);
        TypedFile imageFile = new TypedFile("multipart/form-data", image);
        sendSig.sendSig(store.getDataInt(TSDatum.FILE_ROW_ID), store.getDataInt(TSDatum.CURRENT_TIME_SHEET), imageFile, sentSig);
    }
    Callback<ApiService.FileRespP> sentSig = new Callback<ApiService.FileRespP>() {
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            } else {
                Toast.makeText(context, "Save Failed", sdkVersion).show();
                retrofit.client.Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
            }
        }


        @Override
        public void success(ApiService.FileRespP sessionResponse, retrofit.client.Response response) {
            String location = sessionResponse.uploadedFileLocation.get(0);
            store.setDataString(TSDatum.REC_SIG, location);
            store.setDataBoolean(TSDatum.CLIENT_SIGNED, true);
            sendSigInfo();
        }
    };

    public void signAll() {
        if (user.sessions.size() > 0) {
            if (i < user.sessions.size()) {
                respHandler.setCurrentSessionRec(context, i);
                fileRowId = store.getDataInt(TSDatum.FILE_ROW_ID);
                serviceChoice = globals.serviceChoice;
                if (!(fileRowId > 0)) {
                    getFileRow();
                } else {
                    boolean hasSigned = store.getDataBoolean(TSDatum.CLIENT_SIGNED);
                    if (hasSigned) {
                        i++;
                        signAll();
                    } else {
                        sendImage();
                    }
                }
            } else {
                finishAll();
            }
        }
    }

    public void sendSigInfo() {
        ApiService.RecipientSignature recSig = apiService.restAdapter.create(ApiService.RecipientSignature.class);
        ApiService.RecipientSigReq sendSig = apiService.new RecipientSigReq();
        JsonObject recSigData = new JsonObject();
        Calendar c = Calendar.getInstance();
        String currentTime = serverDateFormat.format(c.getTime());
        recSigData.addProperty("timesheet", store.getDataInt(TSDatum.CURRENT_TIME_SHEET));
        recSigData.addProperty("recipientSigTime", currentTime);
        if (serviceChoice.equals(globalHCTS.ServiceChoice.PCA)) {
            if (currentLocation != null) {
                recSigData.addProperty("recipientSigAddress", globals.getAddress(context, currentLocation));
                recSigData.addProperty("latitude", mLatitudeText);
                recSigData.addProperty("longitude", mLongitudeText);
            }
        }
        else {
            recSigData.addProperty("recipientSigAddress", "");
            recSigData.addProperty("latitude", "0");
            recSigData.addProperty("longitude", "0");
        }
        recSigData.addProperty("recipientPhotoTime", currentTime);
        store.setDataString(TSDatum.CLIENT_SIG_TIME, currentTime);
        sendSig.homecare_homecarebundle_recipientsignature = recSigData;
        recSig.sendRecSig(sendSig, getSigResp);
    }

    Callback<ApiService.RecipientSigResp> getSigResp = new Callback<ApiService.RecipientSigResp>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            } else {
                retrofit.client.Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
                else if (r != null && r.getStatus() == 409) {
                    i++;
                    signAll();
                }
            }
        }

        @Override
        public void success(ApiService.RecipientSigResp sessionResponse, retrofit.client.Response response) {
            store.setDataBoolean(TSDatum.CLIENT_SIGNED, true);
            i++;
            signAll();
        }

    };

    public void getFileRow() {
        ApiService.getFileRow getFileId = apiService.restAdapter.create(ApiService.getFileRow.class);
        ApiService.FileRowReq getFileIdBody = apiService.new FileRowReq();
        getFileId.createFileRow(getFileIdBody, newFileRow);
    }

    Callback<ApiService.FileRowResp> newFileRow = new Callback<ApiService.FileRowResp>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                retrofit.client.Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
                if (r != null && r.getStatus() == 409) {
                    i++;
                    signAll();
                }
            }
        }

        @Override
        public void success(ApiService.FileRowResp sessionResponse, retrofit.client.Response response) {
            store.setDataInt(TSDatum.FILE_ROW_ID, sessionResponse.createdFilesId);
            sendImage();
        }
    };



    @Override
    public void onConnected(Bundle connectionHint) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (currentLocation != null) {
            mLatitudeText = String.valueOf(currentLocation.getLatitude());
            mLongitudeText = String.valueOf(currentLocation.getLongitude());
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {}

    @Override
    public void onConnectionFailed(@Nullable ConnectionResult arg0) {}

    public void finishAll() {
       startActivity(new Intent(context, FinishAll.class));
    }

    public void returnToLogin() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

}
