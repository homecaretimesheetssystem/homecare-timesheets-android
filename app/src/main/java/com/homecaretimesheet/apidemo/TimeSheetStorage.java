package com.homecaretimesheet.apidemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class TimeSheetStorage {
		public static final int INVALID_INT_VALUE = -1;
		public static final String INVALID_STRING_VALUE = "ERROR";

		private final SharedPreferences mPrefs;
		private static final String TIME_SHEET_PREFERENCES =
				"TimeSheetPreferences";
	            
		public TimeSheetStorage(Context context) {
			mPrefs = context.getSharedPreferences(
							TIME_SHEET_PREFERENCES,
							Context.MODE_PRIVATE);
		}
	    
	    
		public void setDataInt(TSDatum datum, int data) {
	        Editor editor = mPrefs.edit();
	        editor.putInt(getFieldKey(datum), data);
	        editor.commit();
		}

	    public int getDataInt(TSDatum datum) {
	    	return mPrefs.getInt(getFieldKey(datum), 
	    			INVALID_INT_VALUE);
	    }
	    
	    public Boolean hasDataInt(TSDatum datum) {
	    	return (mPrefs.getInt(getFieldKey(datum), 
	    			INVALID_INT_VALUE) != INVALID_INT_VALUE);
	    }
		
		public void setDataBoolean(TSDatum datum, Boolean data) {
	        Editor editor = mPrefs.edit();
	        editor.putBoolean(getFieldKey(datum), data);
	        editor.commit();
		}
		
	    public Boolean getDataBoolean(TSDatum datum) {
	    	return mPrefs.getBoolean(getFieldKey(datum), false);
	    }
	    
	    public void setDataString(TSDatum datum, String value) {
	        Editor editor = mPrefs.edit();
	        editor.putString(getFieldKey(datum), value);
	        editor.commit();
	    }

	    public String getDataString(TSDatum datum) {
	    	return mPrefs.getString(getFieldKey(datum),
	    			INVALID_STRING_VALUE);
	    }
	    
	    public Boolean hasDataString(TSDatum datum) {
	    	return (!mPrefs.getString(getFieldKey(datum), 
	    			INVALID_STRING_VALUE).equals(INVALID_STRING_VALUE));
	    }
	    
		public void clearData() {
			Editor editor = mPrefs.edit();
			for (TSDatum datum : TSDatum.values()) {
				editor.remove(getFieldKey(datum));
			}
			editor.commit();
		}
		
		public void clearDatum(TSDatum datum) {
			Editor editor = mPrefs.edit();
			editor.remove(getFieldKey(datum));
			editor.commit();
		}
		
		private String getFieldKey(TSDatum datum) {
			return "KEY_" + Integer.toString(datum.ordinal());
		}

	}

