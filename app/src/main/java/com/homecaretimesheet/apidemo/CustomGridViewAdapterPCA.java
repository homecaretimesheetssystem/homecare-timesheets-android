package com.homecaretimesheet.apidemo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.homecaretimesheet.apidemo.globalHCTS.ServiceChoice;

 class CustomGridViewAdapterPCA extends ArrayAdapter {
    Context context;
    ArrayList<SessionData> sessions;
    User user;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy", Locale.US);
    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
    private SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa", Locale.US);

    CustomGridViewAdapterPCA(Context context, ArrayList<SessionData> sessions)
    {
        super(context, 0);
        this.context=context;
        this.sessions = sessions;
    }
    public int getCount()
    {
        return sessions.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        globalHCTS globals = (globalHCTS) context.getApplicationContext();
        user = globals.user;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.activity_show_pdf, parent, false);
            SessionData curSession = sessions.get(position);
            TimeSheet timesheet = curSession.timesheets.get(curSession.timesheets.size() - 1);
            String rectext = "Unknown";
            if (timesheet.recipient != null) {
                rectext = "Recipient: " + timesheet.recipient.first_name.toUpperCase() + " " + timesheet.recipient.last_name.toUpperCase();
            }

            String datetext = "--/--/--";
            String timeIn = "--:-- --";
            Date date = new Date();
            if (curSession.timeIn != null) {
                try {
                    date = serverDateFormat.parse(curSession.timeIn.timeIn);
                    datetext = dateFormat.format(date);
                    timeIn = timeFormat.format(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            String timeInText = "Time In: " + timeIn;
            String timeOutText = "Time Out: --:-- --";
            String totalText = "Total: 0:00";
            if (curSession.timeOut != null) {
                try {
                    Date outDate = serverDateFormat.parse(curSession.timeOut.timeOut);
                    long shiftLength = outDate.getTime() - date.getTime();
                    Integer shiftLeng = ((int) (long) shiftLength) / 1000;
                    Integer minutes = (shiftLeng / 60 % 60) - (shiftLeng / 60 % 15);
                    Integer hours = shiftLeng / (60 * 60);
                    String hour = hours.toString();
                    String minute;
                    if (minutes > 10) {
                        minute = minutes.toString();
                    } else if (minutes == 0) {
                        minute = "00";
                    } else {
                        minute = "0" + minutes.toString();
                    }
                    timeOutText = "Time Out: " + timeFormat.format(outDate);
                    totalText = "Total: " + hour + ":" + minute;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            ImageView underView = row.findViewById(R.id.under);
            TextView textView = (TextView) row.findViewById(R.id.worker);
            TextView dateView = (TextView) row.findViewById(R.id.date);
            TextView timeInView = (TextView) row.findViewById(R.id.timein);
            TextView timeOutView = (TextView) row.findViewById(R.id.timeout);
            TextView totalView = (TextView) row.findViewById(R.id.total);
            textView.setText(rectext);
            dateView.setText(datetext);
            timeInView.setText(timeInText);
            timeOutView.setText(timeOutText);
            totalView.setText(totalText);
            ServiceChoice currentService = null;
            if (curSession.service != null) {
                globals.serviceChoice = ServiceChoice.fromInt(curSession.service.service_number);
                currentService = ServiceChoice.fromInt(curSession.service.service_number);
            }
            if (currentService != null) {
                switch (currentService.getiValue()) {
                    case 1:
                        underView.setImageResource(R.drawable.pca);
                        break;
                    case 3:
                        underView.setImageResource(R.drawable.respite);
                        break;
                    case 5:
                        underView.setImageResource(R.drawable.environmentalmodifications);
                        break;
                    case 8:
                        underView.setImageResource(R.drawable.consumersupport);
                        break;
                    case 4:
                        underView.setImageResource(R.drawable.caregivingexpense);
                        break;
                    case 10:
                        underView.setImageResource(R.drawable.treatmentandtraining);
                        break;
                    case 7:
                        underView.setImageResource(R.drawable.selfdirectionsupport);
                        break;
                    case 6:
                        underView.setImageResource(R.drawable.personalsupport);
                        break;
                    case 9:
                        underView.setImageResource(R.drawable.personalassistance);
                        break;
                    case 2:
                        underView.setImageResource(R.drawable.homemaking);
                        break;
                    default:
                        underView.setImageResource(R.drawable.pca);
                        break;
                }
            }
        }
        return row;
    }

}
