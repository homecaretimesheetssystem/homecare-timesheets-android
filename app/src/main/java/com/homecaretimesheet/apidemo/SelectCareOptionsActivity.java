package com.homecaretimesheet.apidemo;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.homecaretimesheet.apidemo.ApiService.DeleteCareOptions;
import com.homecaretimesheet.apidemo.ApiService.DeleteCareOptionsResp;
import com.homecaretimesheet.apidemo.ApiService.SendCareOptions;
import com.homecaretimesheet.apidemo.ApiService.SendCareOptionsReq;
import com.homecaretimesheet.apidemo.ApiService.SendCareOptionsResp;
import com.homecaretimesheet.apidemo.ApiService.UpdateSession;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionReq;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionResp;

public class SelectCareOptionsActivity extends AppCompatActivity {
	
	private Context context;
	private TimeSheetStorage store;
	private ApiService apiService;
	private ListView careOpts;
	private ListView iCareOpts;
	private MenuItem menuNext;
    public  List<CareOption> listOpts;
    public ArrayList<Integer> selected;
	public Integer careNum = 0;
	public Integer iCareNum = 0;
	public Boolean careOptSaved = false;
	public List<String> iChecked;
	public List<String> oChecked;
	public ArrayAdapter<String> adapter;
	public ArrayAdapter<String> iAdapter;
	public globalHCTS.ServiceChoice serviceChoice;
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_care_options);
		context = SelectCareOptionsActivity.this;
		store = new TimeSheetStorage(this);
    	setActiveTime();
        globalHCTS global = (globalHCTS) getApplicationContext();
		serviceChoice = global.serviceChoice;
		if (store.getDataBoolean(TSDatum.CARE_OPTIONS_SET)) {
		careOptSaved = true;	
		}
		
		apiService = new ApiService(store.getDataString(TSDatum.API_TOKEN));
		dataSubmit();

		careOpts = (ListView) findViewById(R.id.carelist);
		iCareOpts = (ListView) findViewById(R.id.icarelist);
		if (serviceChoice.equals(globalHCTS.ServiceChoice.PCA)) {
			careOpts.setVisibility(View.VISIBLE);
		}
		careOpts.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		iCareOpts.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		String careOptions = store.getDataString(TSDatum.CARE_OPTIONS);
		TypeToken<List<CareOption>> token = new TypeToken<List<CareOption>>(){};
        final List<CareOption> listOptions = new Gson().fromJson(careOptions, token.getType());
        listOpts = listOptions;
        List<String> iCareItems = new ArrayList<>();
        List<String> careItems = new ArrayList<>();
        ArrayList<Integer> selOpts = null;
	    String selCareOpts = store.getDataString(TSDatum.CURRENT_CARE_OPTIONS);
	    if (!selCareOpts.equals("ERROR") && !selCareOpts.equals("[]")) {
	    	String[] selCareOptArray = selCareOpts.replaceAll("\\[|\\]| ", "").split(",");
	    	if (selCareOptArray.length > 0) {
	    		selOpts = new ArrayList<>();
				for (String aSelCareOptArray : selCareOptArray) {
					try {
						Integer addNum = Integer.valueOf(aSelCareOptArray);
						selOpts.add(addNum);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
	    	}
	    }
        iChecked = new ArrayList<>();
        oChecked = new ArrayList<>();
        for (CareOption element : listOptions) {
            if (element.is_iadl) {
                iCareItems.add(element.care_option.toUpperCase());
                if (selOpts != null) {
                    if (selOpts.contains(element.id)) {
                        iChecked.add(element.care_option.toUpperCase());
                    }
                }
            } else {
                careItems.add(element.care_option.toUpperCase());
                if (selOpts != null) {
                    if (selOpts.contains(element.id)) {
                        oChecked.add(element.care_option.toUpperCase());
                    }
                }
            }
        }
		adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice, careItems);
		careOpts.setAdapter(adapter);
		iAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_multiple_choice, iCareItems);
		iCareOpts.setAdapter(iAdapter);
		careOpts.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ListView lv = (ListView) parent;
				Boolean addUp = lv.isItemChecked(position);
				if (addUp) {
					careNum++;
				}
				else {
					careNum--;
				}
	        	setActiveTime();
				checkSubmit();
			}
		});	
		iCareOpts.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ListView lv = (ListView) parent;
				Boolean addUp = lv.isItemChecked(position);
				if (addUp){
					iCareNum++;
				}
				else {
					iCareNum--;
				}
	        	setActiveTime();
				checkSubmit();
			}
		});	
		repopLists();
	}

	
	
	public void repopLists() {
		if (oChecked.size() > 0) {
				for(int i=0; i < oChecked.size(); i++) {
					careOpts.setItemChecked(adapter.getPosition(oChecked.get(i)), true);
					careNum++;
				}
			}
			if (iChecked.size() > 0) {
				for(int i=0; i < iChecked.size(); i++) {
					iCareOpts.setItemChecked(iAdapter.getPosition(iChecked.get(i)), true);
					iCareNum++;
				}
			}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		repopLists();
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.major, menu);
		menuNext = menu.findItem(R.id.next_bar);
		return true;
	}
	
	public void  nextActivity() {
    	setActiveTime();
		store.setDataInt(TSDatum.PAGE_NUMBER, 6);
		startActivity(new Intent(context, PDFShowActivity.class));
		}
	
	public void checkSubmit() {
		if (menuNext != null) {
			Integer allChecked = careNum + iCareNum;
			if (allChecked > 0) {
				menuNext.setEnabled(true);
			}
			else {
			menuNext.setEnabled(false);
			}
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.next_bar) {
			collectSubmit();
			return true;
		}
		if (id == android.R.id.home) {
			onBackPressed();
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onBackPressed() {
		Intent intent = new Intent(context, TimePunchActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}
	
	public void collectSubmit() {
		 selected = new ArrayList<>();
		 int cntChoice = careOpts.getCount();
		 int iCntChoice = iCareOpts.getCount();
		 setActiveTime();
         SparseBooleanArray careArray = careOpts.getCheckedItemPositions();
         for(int i = 0; i < cntChoice; i++){
             if(careArray.get(i)) {
            	 String string = careOpts.getItemAtPosition(i).toString();
                 for (CareOption element : listOpts) {
                     if (string.equals(element.care_option.toUpperCase())) {
                         selected.add(element.id);
                     }
                 }
            }
         }
         SparseBooleanArray iCareArray = iCareOpts.getCheckedItemPositions();
         for(int i = 0; i < iCntChoice; i++){
             if(iCareArray.get(i)) {
            	 String string = iCareOpts.getItemAtPosition(i).toString();
                 for (CareOption element : listOpts) {
                     if (string.equals(element.care_option.toUpperCase())) {
                         selected.add(element.id);
                     }
                 }
            }
         }
         if (careOptSaved) {
        	 
        	 String storedOpts = store.getDataString(TSDatum.CURRENT_CARE_OPTIONS);
        	 if (storedOpts.equals(selected.toString())) {
        		 nextActivity();
        	 }
        	 else {
        		 store.setDataString(TSDatum.CURRENT_CARE_OPTIONS, selected.toString());
        		 removeCareOptions();
        	 }
         }
         else {
    		 store.setDataString(TSDatum.CURRENT_CARE_OPTIONS, selected.toString());
    		 store.setDataBoolean(TSDatum.CARE_OPTIONS_SET, true);
    		 saveCareOptions();
         }
	}
	
	public void removeCareOptions() {
		if (store.getDataInt(TSDatum.CURRENT_TIME_SHEET) > 0) {
		DeleteCareOptions removeCO = apiService.restAdapter.create(DeleteCareOptions.class);
		removeCO.removeCareOptions(store.getDataInt(TSDatum.CURRENT_TIME_SHEET), careOptsRemoved);
		}
		else {
			saveCareOptions();
		}
	}
	
	Callback<DeleteCareOptionsResp> careOptsRemoved = new Callback<DeleteCareOptionsResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}	
			}
		}


		@Override
		public void success(DeleteCareOptionsResp sessionResponse, Response response) {
	    saveCareOptions();
		}
	};
	
	
	public void saveCareOptions() {
		SendCareOptions sendCareOpts = apiService.restAdapter.create(SendCareOptions.class);
		SendCareOptionsReq careOptionData = apiService.new SendCareOptionsReq();
		JsonObject careObject = new JsonObject();
		careObject.addProperty("timesheet", store.getDataInt(TSDatum.CURRENT_TIME_SHEET));
		Gson gson = new GsonBuilder().create();
		
		JsonArray careOptJson = gson.toJsonTree(selected).getAsJsonArray();
		careObject.add("careOptions", careOptJson);
		careOptionData.homecare_homecarebundle_careoptiontimesheet = careObject;
		sendCareOpts.setCareOptions(careOptionData, careOptionResp);
	}
	
	Callback<SendCareOptionsResp> careOptionResp = new Callback<SendCareOptionsResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}	
			}
		}

		@Override
		public void success(SendCareOptionsResp sessionResponse, Response response) {
		    nextActivity();
		}
		
	};
	
	public void dataSubmit() {
		UpdateSession updateSession = apiService.restAdapter.create(UpdateSession.class);
		UpdateSessionReq sendSession = apiService.new UpdateSessionReq();
		Integer pageNum = 5;
		JsonObject sessionData = new JsonObject();
		sessionData.addProperty("continueTimesheetNumber", pageNum);
		sendSession.homecare_homecarebundle_sessiondata = sessionData;
		updateSession.updateSession(store.getDataInt(TSDatum.SESSION_ID), sendSession, updateResponse);
	}
	
	Callback<UpdateSessionResp> updateResponse = new Callback<UpdateSessionResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}	
			}
		}


		@Override
		public void success(UpdateSessionResp sessionResponse, Response response) {
			checkSubmit();
		}
	};


	@Override
	protected void onRestart(){
		super.onRestart();
		Integer restartTime = (int) (long) System.currentTimeMillis() - 120000;
		Integer lastTime = store.getDataInt(TSDatum.LAST_ACTIVITY);
		if (lastTime < restartTime) {
		returnToLogin();
		}
	}
	
	
	public void returnToLogin() {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}
	
	public void setActiveTime() {
		Integer time = (int) (long) System.currentTimeMillis();
		store.setDataInt(TSDatum.LAST_ACTIVITY, time);
	}
	
}
