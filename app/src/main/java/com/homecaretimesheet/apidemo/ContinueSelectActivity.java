package com.homecaretimesheet.apidemo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class ContinueSelectActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

	private Context context;
	private TimeSheetStorage store;
	private Button create;
	private Button continuing;
	private ApiService apiService;
	private User user;
	private ResponseHandler responseHandler;
	private String api_token;
	protected ApiService.DataService datafetch;
	public globalHCTS globals;
	public boolean skipRatio = false;
	private ApiService.CreateSession makeSession;
	private ApiService.UpdateSession updateSession;
	public LocationRequest mLocationRequest;
	public Boolean needVerif = false;
	public String verifFile = "verifImg";
	public ApiService.GetVerif getVerif;
	public ApiService.SetVerif setVerif;
	private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

	public int vId = 0;
	private static final int IMAGE_REQUEST = 1337;
	private int LOC_REQUEST = 11011;
	private int UP_REQUEST = 11012;
	private int CONNECT_REQUEST = 11013;
	private int CAMERA_REQUEST = 11014;
	public GoogleApiClient mGoogleApiClient;
	private Location currentLocation;
	public PermissionService permService;
	public TypedFile setOutput = null;
	public File getOutput = null;
	public int STORAGE_REQUEST = globalHCTS.STORAGE_REQUEST;
	public Recipient current_recip;


	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_continue);
		context = ContinueSelectActivity.this;
		store = new TimeSheetStorage(this);
		globals = (globalHCTS) getApplicationContext();
		user = globals.user;
		apiService = new ApiService(store.getDataString(TSDatum.API_TOKEN));
		makeSession = apiService.restAdapter.create(ApiService.CreateSession.class);
		updateSession = apiService.restAdapter.create(ApiService.UpdateSession.class);
		setActiveTime();
        if (store.getDataInt(TSDatum.USER_TYPE) > 1) {
        	returnToLogin();
        }
		api_token = store.getDataString(TSDatum.API_TOKEN);
		responseHandler = new ResponseHandler();
		create = (Button) findViewById(R.id.CreateSession);
		permService = new PermissionService();
		create.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				setActiveTime();
				nextActivity();
				create.setEnabled(false);
			}
		});	
		
        continuing = (Button) findViewById(R.id.ContinueSession);
		if (user.sessions != null && user.sessions.size() > 0) {
			continuing.setEnabled(true);
		}
		else {
			continuing.setEnabled(false);
		}
		continuing.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (user.sessions.size() < 2) {
					continueTimesheet(0);
				}
				else {
					Intent intent = new Intent(context, DisplayCurrentTimesheets.class);
					intent.putExtra("delete", 0);
					startActivity(intent);
					finish();
				}
				setActiveTime();
				continuing.setEnabled(false);
			}
		});
		int from_id = getIntent().getIntExtra("FROM_ID", 0);
		if (from_id == 0) {
			checkUserData();
		}
	}

	public boolean onPrepareOptionsMenu(Menu menu) {

		boolean b = false;
		if (user.sessions != null) {
			b = user.sessions.size() > 0;
		}
		menu.findItem(R.id.del_bar).setEnabled(b);
		return true;
	}

	public void checkButtons() {
		if (user.sessions.size() > 0) {
			continuing.setEnabled(true);
			invalidateOptionsMenu();
		}
	}

	public void checkUserData() {
		apiService = new ApiService(api_token);
		datafetch = apiService.restAdapter.create(ApiService.DataService.class);
		datafetch.getData(dataResponse);
	}

	Callback<JsonObject> dataResponse = new Callback<JsonObject>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Data Check Failed", Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		public void success(JsonObject loginResponse, Response response) {
			responseHandler.getResp(loginResponse, context);
			checkButtons();
		}
	};

	public void continueTimesheet(int sessionId) {
		ResponseHandler responseHandler = new ResponseHandler();
		responseHandler.setCurrentSessionPCA(context, sessionId);
		continueSubmit();
	}

    void continueSubmit() {
		if (skipRatio) {
			setElement(current_recip);
		}
		else {
			setActiveTime();
			if (store.getDataBoolean(TSDatum.CLIENT_SIGNED)) {
				context.startActivity(new Intent(context, PDFShowActivity.class));
				finish();
			} else {
				Integer pageNum = store.getDataInt(TSDatum.PAGE_NUMBER);
				Class<?> activityClass = SelectServiceActivity.class;
				switch (pageNum) {
					case 1:
						activityClass = SelectServiceActivity.class;
						break;
					case 2:
						activityClass = RatioLocationActivity.class;
						break;
					case 3:
						activityClass = SelectRecipientActivity.class;
						break;
					case 4:
						activityClass = TimePunchActivity.class;
						break;
					case 5:
						activityClass = SelectCareOptionsActivity.class;
						break;
					case 6:
						activityClass = PDFShowActivity.class;
						break;
				}
				Intent intent = new Intent(context, activityClass);
				startActivity(intent);
			}
		}
	}
    
    void nextActivity() {
		store.clearDatum(TSDatum.PAGE_NUMBER);
		store.clearDatum(TSDatum.TIME_SHEETS);
		store.clearDatum(TSDatum.DATE_ENDING);
		store.clearDatum(TSDatum.HOSPITAL_DATES);
		store.clearDatum(TSDatum.CURRENT_RATIO);
		store.clearDatum(TSDatum.CURRENT_SERVICES);
		store.clearDatum(TSDatum.CURRENT_CARE_LOCATION);
		store.clearDatum(TSDatum.CURRENT_TIME_SHEET);
		store.clearDatum(TSDatum.CURRENT_CARE_OPTIONS);
		store.clearDatum(TSDatum.EXPECTED_SHIFT_LENGTH);
		store.clearDatum(TSDatum.TIME_IN_PHOTO_ID);
		store.clearDatum(TSDatum.TIME_OUT_PHOTO_ID);
		store.clearDatum(TSDatum.SHARED_CARE_LOCATION);
		store.clearDatum(TSDatum.TIME_IN);
		store.clearDatum(TSDatum.TIME_OUT);
		store.clearDatum(TSDatum.TIME_IN_ADD);
		store.clearDatum(TSDatum.TIME_OUT_ADD);
		store.clearDatum(TSDatum.SESSION_DATA);
		store.clearDatum(TSDatum.SESSION_ID);
		store.clearDatum(TSDatum.FILE_ROW_ID);
		store.clearDatum(TSDatum.SESSION_FILE_ROW_ID);
		store.clearDatum(TSDatum.SELECTED_RECIPIENTS);
		store.setDataBoolean(TSDatum.PUNCHED_OUT, false);
		store.setDataBoolean(TSDatum.PUNCHED_IN, false);
		store.setDataInt(TSDatum.FINISHED, 0);
		store.setDataBoolean(TSDatum.PCA_SIGNED, false);
		store.setDataBoolean(TSDatum.CLIENT_SIGNED, false);
		store.clearDatum(TSDatum.CURRENT_SERVICE_INT);

		setActiveTime();
        if (user.services.isEmpty()) {
            user.services = user.agency.services;
        }

		if (user.services.size() > 1) {
			Intent intent = new Intent(context, SelectServiceActivity.class);
			startActivity(intent);
			finish();
		}
		else if (user.services.size() == 1) {
			skipRatio = (user.recipients.size() == 1);
            Service selection = user.services.get(0);
			int serviceInt = selection.service_number;
			store.setDataInt(TSDatum.CURRENT_SERVICES, selection.id);
			setServiceChoice(serviceInt);
			store.setDataString(TSDatum.CURRENT_SERVICES_TEXT, selection.service_name.toUpperCase());
			dataRowCreate();
		}
		else {
            Toast.makeText(context, "No Services Set, Contact Agency Admin", Toast.LENGTH_LONG).show();
            returnToLogin();
        }
    }

	public void setServiceChoice(int choice) {
		globals.serviceChoice = globalHCTS.ServiceChoice.fromInt(choice);
		if (skipRatio) {
			store.setDataInt(TSDatum.CURRENT_TIME_SHEET_NUM, 1);
			String locations = store.getDataString(TSDatum.LOCATIONS);
			TypeToken<List<SharedCareLocation>> sToken = new TypeToken<List<SharedCareLocation>>(){};
			final List<SharedCareLocation> listLocations = new Gson().fromJson(locations, sToken.getType());
			String ratios = store.getDataString(TSDatum.RATIOS);
			TypeToken<List<Ratio>> token = new TypeToken<List<Ratio>>(){};
			final List<Ratio> listRatios = new Gson().fromJson(ratios, token.getType());
			for(Ratio rat : listRatios) {
				if (rat.ratio.equals("1:1")) {
					store.setDataInt(TSDatum.CURRENT_RATIO, rat.id);
					store.setDataInt(TSDatum.RATIO_INT, 1);
				}
			}
			for(SharedCareLocation loc : listLocations) {
				if (loc.location.equals("N/A")) {
					store.setDataInt(TSDatum.CURRENT_CARE_LOCATION, loc.id );
					store.setDataString(TSDatum.CURRENT_LOCATION_TEXT, loc.location.toUpperCase());
				}
			}
		}
	}
	public void dataRowCreate() {
		if (store.getDataInt(TSDatum.SESSION_ID) < 1) {
			ApiService.CreateSessionReq getSession = apiService.new CreateSessionReq();
			makeSession.createSession(getSession, sessionResponse);
		}
		else {
			dataSubmit();
		}
	}


	public void dataSubmit() {
		ApiService.UpdateSessionReq sendSession = apiService.new UpdateSessionReq();
		Integer pageNum = 2;
		JsonObject sessionData = new JsonObject();
		if (skipRatio) {
			sessionData.addProperty("ratio", store.getDataInt(TSDatum.CURRENT_RATIO));
			sessionData.addProperty("sharedCareLocation", store.getDataInt(TSDatum.CURRENT_CARE_LOCATION));
			pageNum = 4;
			String recs = store.getDataString(TSDatum.RECIPIENTS);
			TypeToken<List<Recipient>> token = new TypeToken<List<Recipient>>() {
			};
			final List<Recipient> recipList = new Gson().fromJson(recs, token.getType());
			Recipient recip = recipList.get(0);
			String name = recip.first_name.toUpperCase() + " " + recip.last_name.toUpperCase();
			int selected = recip.id;
			store.setDataInt(TSDatum.CURRENT_RECIPIENT, selected);
			store.setDataString(TSDatum.CURRENT_RECIPIENT_NAME, name);
			current_recip = recip;
		}
		sessionData.addProperty("pca", store.getDataInt(TSDatum.PCA_ID));
		sessionData.addProperty("service", store.getDataInt(TSDatum.CURRENT_SERVICES));
		sessionData.addProperty("continueTimesheetNumber", pageNum);
        store.setDataInt(TSDatum.PAGE_NUMBER, pageNum);
		sendSession.homecare_homecarebundle_sessiondata = sessionData;
		updateSession.updateSession(store.getDataInt(TSDatum.SESSION_ID), sendSession, updateResponse);
	}


	Callback<ApiService.CreateSessionResp> sessionResponse = new Callback<ApiService.CreateSessionResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Session Start Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(ApiService.CreateSessionResp sessionResponse, Response response) {
			store.setDataInt(TSDatum.SESSION_ID, sessionResponse.createdSessionData.id);
			dataRowCreate();
		}

	};

	public void setElement(Recipient element) {
		store.setDataBoolean(TSDatum.RECIPIENT_HAS_VERIF, !element.skip_verification);
		store.setDataBoolean(TSDatum.RECIPIENT_HAS_GPS, !element.skip_gps);
		globals.needsGPS = !element.skip_gps;
		globals.needsVerif = !element.skip_verification;
		if (globals.needsGPS) {
			final LocationManager manager2 = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			if (!manager2.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				buildAlertMessageNoGps();
			}
			buildGoogleApiClient();
			mGoogleApiClient.connect();
		}
		if (globals.needsVerif) {
			needVerif = true;
            timeSheetRowCreate();
		}
		else {
			skipRatio = false;
			continueSubmit();
		}
	}

	Callback<ApiService.UpdateSessionResp> updateResponse = new Callback<ApiService.UpdateSessionResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Time Entry Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(ApiService.UpdateSessionResp sessionResponse, Response response) {
			continueSubmit();
		}
	};


	public void setActiveTime() {
			Integer time = (int) (long) System.currentTimeMillis();
			store.setDataInt(TSDatum.LAST_ACTIVITY, time);
	}

	@Override
	protected void onRestart(){
		super.onRestart();
		Integer restartTime = (int) (long) System.currentTimeMillis() - 120000;
		Integer lastTime = store.getDataInt(TSDatum.LAST_ACTIVITY);
		String agencyImg = store.getDataString(TSDatum.AGENCY_ICON);
		if (lastTime < restartTime) {
			returnToLogin();
		}
		else if (!agencyImg.equals("ERROR")) {
			if (store.getDataBoolean(TSDatum.HAS_IMG)) {
				Bitmap agencyImage = loadImageFromStorage(store.getDataString(TSDatum.LOCAL_PATH));
				ImageView imageView = (ImageView) findViewById(R.id.agency_icon);
				imageView.setImageBitmap(agencyImage);
			}
		}
	}
	
	public void returnToLogin() {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}
	@Override
	protected void onStart() {
		super.onStart();
		if (store.getDataBoolean(TSDatum.HAS_IMG)) {
			Bitmap agencyImage = loadImageFromStorage(store.getDataString(TSDatum.LOCAL_PATH));
			ImageView imageView = (ImageView) findViewById(R.id.agency_icon);
			imageView.setImageBitmap(agencyImage);
		}
		if (mGoogleApiClient != null) {
			mGoogleApiClient.connect();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.cont, menu);
		MenuItem menuDel = menu.findItem(R.id.del_bar);
		if (user.sessions != null && user.sessions.size() > 0) {
			menuDel.setEnabled(true);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.del_bar) {
			Intent intent = new Intent(context, DisplayCurrentTimesheets.class);
			intent.putExtra("delete", 1);
			startActivity(intent);
			finish();
			return true;
		}
		if (id == android.R.id.home) {
			returnToLogin();
		}
		return true;
	}

	private Bitmap loadImageFromStorage(String path)
	{
		Bitmap b = null;
		try {
			File f=new File(path, "agency.png");
			b = BitmapFactory.decodeStream(new FileInputStream(f));
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return b;
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}

	private void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
				.setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
						startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
					}
				});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == IMAGE_REQUEST) {
			if (resultCode == RESULT_OK) {
				if (getOutput.exists()) {
					sendVImage();
				} else {
					getImage(verifFile, IMAGE_REQUEST);
				}
			}
		}
	}


	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();
	}

	@Override
	public void onConnectionSuspended(int arg0) {
	}

	protected void onStop() {
		if (mGoogleApiClient != null) {
			mGoogleApiClient.disconnect();
		}
		super.onStop();
	}

	@Override
	public void onConnectionFailed(@Nullable ConnectionResult arg0) {
		Toast.makeText(context, "Location Service Failed", Toast.LENGTH_SHORT).show();
		mGoogleApiClient.connect();
	}


	@Override
	public void onRequestPermissionsResult(int requestCode,
										   @NonNull String permissions[], @NonNull int[] grantResults) {
		if (requestCode == CONNECT_REQUEST) {
			if (grantResults.length > 0	&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				mGoogleApiClient.connect();
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == LOC_REQUEST) {
			if (grantResults.length > 0	&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				createLocationRequest();
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == UP_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				startLocationUpdates();
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == STORAGE_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				getImage(verifFile, IMAGE_REQUEST);
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Storage");
				alert.show();
			}
		}
		else if (requestCode == CAMERA_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				getImage(verifFile, IMAGE_REQUEST);
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Camera");
				alert.show();
			}
		}
	}

	protected void createLocationRequest() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOC_REQUEST);
			return;
		}
		int UPDATE_INTERVAL = 10000; // 10 sec
		int FATEST_INTERVAL = 5000; // 5 sec
		int DISPLACEMENT = 100; // 100 meters
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT); // 100 meters
	}

	protected void startLocationUpdates() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, UP_REQUEST);
			return;
		}
		LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
	}

	@Override
	public void onLocationChanged(Location location) {
		currentLocation = location;
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.ACCESS_FINE_LOCATION},	CONNECT_REQUEST);
			return;
		}
		createLocationRequest();
		startLocationUpdates();
		if (currentLocation == null) {
			LocationServices.FusedLocationApi.flushLocations(mGoogleApiClient);
			currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		}
	}


	public void getImage(String fileName, Integer ReqCode) {
		if (permService.getStoragePermission(context) && permService.getCameraPermission(context)) {
			Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
			//noinspection ResultOfMethodCallIgnored
			dir.mkdirs();
			File output = new File(dir, fileName);
			if (output.exists()) {
				//noinspection ResultOfMethodCallIgnored
				output.delete();
			}
			getOutput = output;
			setOutput = new TypedFile("multipart/form-data", output);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));
			intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
			startActivityForResult(intent, ReqCode);
		}
		else if (permService.getStoragePermission(context)) {
			getCameraPermission();
		}
		else {
			getStoragePermission();
		}
	}
	protected void sendVImage() {
		ApiService.sendVerifImage sendupOut = apiService.restAdapter.create(ApiService.sendVerifImage.class);
		sendupOut.sendUpOutImage(vId, setOutput, vImageResp);
	}

	Callback<ApiService.FileResp> vImageResp = new Callback<ApiService.FileResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Image Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(ApiService.FileResp inFileResp, Response response) {
			Toast.makeText(context, "Image Saved", Toast.LENGTH_SHORT).show();
			setVerifTimesheet();
		}
	};

	public void getCameraPermission() {
		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);
	}

	public void getStoragePermission() {
		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_REQUEST);
	}

	public void setVerifTimesheet() {
		ApiService.SetTimeSheet saveTimeSheet = apiService.restAdapter.create(ApiService.SetTimeSheet.class);
		ApiService.TimeSheetReq saveTimesheetReq = apiService.new TimeSheetReq();
		JsonObject timeSheetData = new JsonObject();
		timeSheetData.addProperty("verification", vId);
		saveTimesheetReq.homecare_homecarebundle_timesheet = timeSheetData;
		int timeSheetId = store.getDataInt(TSDatum.CURRENT_TIME_SHEET);
		saveTimeSheet.setTimeSheet(timeSheetId, saveTimesheetReq, saveVTimesheetResp);
	}

	Callback<ApiService.TimeSheetResp> saveVTimesheetResp = new Callback<ApiService.TimeSheetResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Create Timesheet Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}


		@Override
		public void success(ApiService.TimeSheetResp sheetResponse, Response response) {
			nextActivity();
		}

	};
	public void sendVerif() {
		Calendar c = Calendar.getInstance();
		Date timeVerif = c.getTime();
		String outTime = serverDateFormat.format(timeVerif);
		JsonObject sendVerif = new JsonObject();
		if (currentLocation != null) {
			sendVerif.addProperty("address", globals.getAddress(context, currentLocation));
			sendVerif.addProperty("latitude", currentLocation.getLatitude());
			sendVerif.addProperty("longitude", currentLocation.getLongitude());
		}
		sendVerif.addProperty("time", outTime);
		ApiService.VerifData sendData = apiService.new VerifData();
		sendData.homecare_homecarebundle_verification = sendVerif;
		setVerif = apiService.restAdapter.create(ApiService.SetVerif.class);
		setVerif.setVerif(vId, sendData, sendVerifResp);
	}

	Callback<ApiService.VerifDataResp> sendVerifResp = new Callback<ApiService.VerifDataResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Verification Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(ApiService.VerifDataResp fResp, Response response) {
			showDisclaim();
		}
	};


	public void showDisclaim() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		LayoutInflater inflation = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		@SuppressLint("InflateParams")
		View dialogLayout = inflation.inflate(R.layout.dialog_image_remind, null);

		alertDialogBuilder.setView(dialogLayout);

		alertDialogBuilder.setCancelable(false);
		final Button saveButton = (Button) dialogLayout.findViewById(R.id.done);

		final AlertDialog alertDialog = alertDialogBuilder.create();
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getImage(verifFile, IMAGE_REQUEST);
				alertDialog.dismiss();
			}

		});
		alertDialog.show();
	}

	public void getVerifData() {
		getVerif = apiService.restAdapter.create(ApiService.GetVerif.class);
		int rec_id = store.getDataInt(TSDatum.CURRENT_RECIPIENT);
		int pca_id = store.getDataInt(TSDatum.PCA_ID);
		getVerif.checkVerif(pca_id, rec_id, checkVerif);

	}


	Callback<ApiService.VerifResp> checkVerif = new Callback<ApiService.VerifResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Verification Check Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(ApiService.VerifResp vResp, Response response) {
			if (vResp.verification_needed) {
				needVerif = true;
				vId = vResp.id;
				sendVerif();
			} else {
				skipRatio = false;
				continueSubmit();
			}
		}
	};


    public void timeSheetRowCreate() {
        if (store.getDataInt(TSDatum.CURRENT_TIME_SHEET) > 0) {
            setTimeSheet();
        } else {
            ApiService.GetTimeSheet timeSheetGet = apiService.restAdapter.create(ApiService.GetTimeSheet.class);
            ApiService.GetTimeSheetReq timeSheetReq = apiService.new GetTimeSheetReq();
            timeSheetGet.getTimeSheetId(timeSheetReq, getTimeSheetId);
        }
    }

    Callback<ApiService.GetTimeSheetResp> getTimeSheetId = new Callback<ApiService.GetTimeSheetResp>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Create Time Entry Failed", Toast.LENGTH_SHORT).show();
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
            }
        }


        @Override
        public void success(ApiService.GetTimeSheetResp sheetResponse, Response response) {
            store.setDataInt(TSDatum.CURRENT_TIME_SHEET, sheetResponse.createdTimesheet.id);
            setTimeSheet();
        }

    };

    public void setTimeSheet() {
        ApiService.SetTimeSheet saveTimeSheet = apiService.restAdapter.create(ApiService.SetTimeSheet.class);
        ApiService.TimeSheetReq saveTimesheetReq = apiService.new TimeSheetReq();
        JsonObject timeSheetData = new JsonObject();
        Integer selected = store.getDataInt(TSDatum.CURRENT_RECIPIENT);
        timeSheetData.addProperty("recipient", selected);
        timeSheetData.addProperty("sessionData", store.getDataInt(TSDatum.SESSION_ID));
        saveTimesheetReq.homecare_homecarebundle_timesheet = timeSheetData;
        int timeSheetId = store.getDataInt(TSDatum.CURRENT_TIME_SHEET);
        saveTimeSheet.setTimeSheet(timeSheetId, saveTimesheetReq, saveTimesheetResp);
    }

    Callback<ApiService.TimeSheetResp> saveTimesheetResp = new Callback<ApiService.TimeSheetResp>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Time Entry Save Failed", Toast.LENGTH_SHORT).show();
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
            }
        }

        @Override
        public void success(ApiService.TimeSheetResp sheetResponse, Response response) {
            if (needVerif) {
                getVerifData();
            } else {
				skipRatio = false;
                continueSubmit();
            }
        }

    };

}
