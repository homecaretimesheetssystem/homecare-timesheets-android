package com.homecaretimesheet.apidemo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

@SuppressWarnings("unused")
public class ApiService {
	Boolean new_api = true;

	globalHCTS globals = new globalHCTS();
    Boolean DEBUG = globals.testing;
	public static TimeSheetStorage store;
	private final static String API_PATH = "https://homecare-timesheet.herokuapp.com/api/v1/";
	private final static String LOG_PATH = "https://homecare-timesheet.herokuapp.com/oauth/v1/";
	private final static String TEST_API = "http://homecare-timesheet-staging.herokuapp.com/api/v1/";
	private final static String TEST_LOG = "http://homecare-timesheet-staging.herokuapp.com/oauth/v1/";
	private final static String NEW_API = "https://staging.homecaretimesheetsapp.com/api/v1";
    public RestAdapter restAdapter;

	ApiService() {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String logPath = LOG_PATH;
        retrofit.RestAdapter.LogLevel logLevel = RestAdapter.LogLevel.NONE;
        if (new_api) {
        	logPath = NEW_API;
        	logLevel = RestAdapter.LogLevel.FULL;
		}
        else if (DEBUG) {
            logPath = TEST_LOG;
            logLevel = RestAdapter.LogLevel.FULL;
        }
		restAdapter = new RestAdapter.Builder()
		.setEndpoint(logPath)
		.setLogLevel(logLevel)
		.setConverter(new GsonConverter(gson))
		.build();
	}
	
	ApiService(final String api_token) {
		RequestInterceptor requestInterceptor = new RequestInterceptor() {
			@Override
			public void intercept(RequestFacade request) {
				request.addHeader("Authorization", "Bearer " + api_token);

			}
		};
        String endPath = API_PATH;
        retrofit.RestAdapter.LogLevel logLevel = RestAdapter.LogLevel.NONE;
        if (new_api){
        	endPath = NEW_API;
        	logLevel = RestAdapter.LogLevel.FULL;
		}
        else if (DEBUG) {
            endPath = TEST_API;
            logLevel = RestAdapter.LogLevel.FULL;
		}
	    restAdapter = new RestAdapter.Builder()
	    .setEndpoint(endPath)
	    .setLogLevel(logLevel)
	    .setRequestInterceptor(requestInterceptor)
	    .build();
	}


	interface LoginService {
		@GET("/token")
		void authenticate(@Query("client_id") String client_id,
						  @Query("client_secret") String client_secret,
						  @Query("grant_type") String grant_type,
						  @Query("username") String username,
						  @Query("password") String password,
						  Callback<AuthResponse> response);
    }


    interface LoginServiceNew {
		@POST("/login")
		void authenticate(@Body AuthBody authBody, Callback<AuthResponse> response);
	}

	public class AuthBody {
		String username;
		String password;
		String client_id;
	}
    class AuthResponse {
    	String access_token;
    	String refresh_token;
    	String error;
    	String error_description;
    }
    interface DataService {
		@GET("/data")
		void getData(Callback<JsonObject> response);
	}

	interface GetShifts {
		@GET("/agency/{agency_id}/requests")
		void getShifts(@Path("agency_id") int agency_id, Callback<JsonArray> response);
	}

	public class GetShiftsResp {
		public boolean result;
	}

	interface GetPCA {
		@GET("/pcas/{pca_id}")
		void getData(@Path("pca_id") int pca_id, Callback<JsonObject> response);
	}

	public class GetPCAResp {
		public Boolean result;
		public JsonObject pca;
	}

	interface SetPCAProfile {
		@PATCH("/pcas/{pca_id}")
		void setProfile(@Path("pca_id") int pca_id, @Body SetPCAProfileBody body, Callback<SetPCAProfileResp> response);
	}

	public class SetPCAProfileBody {
		JsonObject homecare_profile;
	}

	public class SetPCAProfileResp {
		boolean result;
	}

	interface SetPCAImage {
		@Multipart
		@PATCH("/pcas/{pca_id}")
		void setProfileImage(@Path("pca_id") int pca_id, @Part("homecare_profile") SetPCAImageBody body, Callback<SetPCAImageResp> response);
	}

	class SetPCAImageBody {
		 JsonObject homecare_profile;
	}

	class ProfileImage {
		ImageFile image;
	}

    class ImageFile {
	}

	class SetPCAImageResp {
		boolean result;
	}

	interface SetPage {
	    @PATCH("/page")
	  	void setPage(@Body SendPage body, Callback<SetPageResponse> response);
	}

  	public class SetPageResponse {
	    public Boolean result;
  	}

	public class SendPage {
	  	final Integer pageNum;
	  	final Integer sessionId;
	  
	 	SendPage(Integer pageNum, Integer sessionId) {
			this.pageNum = pageNum;
		 	this.sessionId = store.getDataInt(TSDatum.SESSION_ID);
	 	}
  	}


 	interface CreateTimeIn {
		@POST("/create/time/in")
	 	void createTimeIn(@Body CreateTimeInReq body, Callback<CreateTimeInResp> response);
 	}
	
 	public class CreateTimeInResp {
	 	public Boolean result;
	 	public Integer TimeInId;
 	}
 
 	public class CreateTimeInReq {
		public String homecare_homecarebundle_timein = "";
 	}

 	interface CreateTimeOut {
	 	@POST("/create/time/out")
	 	void createTimeIn(@Body CreateTimeOutReq body, Callback<CreateTimeOutResp> response);
 	}
	
 	public class CreateTimeOutResp {
	 	public Boolean result;
	 	public Integer TimeOutId;
 	}
 
 	public class CreateTimeOutReq {
	 	public String homecare_homecarebundle_timeout = "";
 	}

	interface CreateSession {
		@POST("/create/sessiondata")
		void createSession(@Body CreateSessionReq body, Callback<CreateSessionResp> response);
	}

	public class CreateSessionResp {
	 	public Boolean result;
	 	public CreatedSessionData createdSessionData;
	}

	public class CreatedSessionData {
		public int id;
	}

	public class CreateSessionReq {
	 	public String homecare_homecarebundle_sessiondata = "";
	}

	interface UpdateSession {
		@PATCH("/sessiondata/{id}")
		void updateSession(@Path("id") int id, @Body UpdateSessionReq body, Callback<UpdateSessionResp> response);
	}

	public class UpdateSessionReq {
		public JsonObject homecare_homecarebundle_sessiondata;
	}

	public class UpdateSessionResp {
		Boolean result;
	}

	interface getSessionFileRow {
		@POST("/create/session/files")
		void createFileRow(@Body FileRowReq body, Callback<SessionFileRowResp> response);
	}

	public class FileRowReq {}

	public class SessionFileRowResp {
		public int createdSessionFilesId;
	}

	interface getFileRow {
		@POST("/create/files")
		void createFileRow(@Body FileRowReq body, Callback<FileRowResp> response);
	}

	public class FileRowResp {
		public int createdFilesId;
	}

	public class FileResp {
		public Boolean result;
		public String uploadedFileLocation;
	}

	public class FileRespP {
		public Boolean result;
		public ArrayList<String> uploadedFileLocation;
	}

	interface getTimeIn {
		@POST("/create/time/in")
		void createTimeIn(@Body GetTimeInReq body, Callback<GetTimeInResp> response);
	}

	public class GetTimeInReq {
		public String homecare_homecarebundle_timein = "";
	}

	public class GetTimeInResp {
		public CreatedTimeIn createdTimeIn;
	}

	public class CreatedTimeIn {
		public int id;
	}

	interface getTimeOut {
		@POST("/create/time/out")
		void createTimeOut(@Body GetTimeOutReq body, Callback<GetTimeOutResp> response);
	}

	public class GetTimeOutReq {
		public String homecare_homecarebundle_timeout = "";
	}

	public class GetTimeOutResp {
		public CreatedTimeOut createdTimeOut;
	}

	public class CreatedTimeOut {
		public int id;
	}

	interface sendTimeIn {
		@PATCH("/timein/{id}")
		void sendTimeInData(@Path ("id") int id, @Body SendTimeInReq body, Callback<SendTimeInResp> response);
	}

	public class SendTimeInReq {
		public JsonObject homecare_homecarebundle_timein;
	}

	public class SendTimeInResp {
		public Boolean result;
		public int seconds;
	}

	interface sendTimeOut {
		@PATCH("/timeout/{id}")
		void sendTimeOutData(@Path ("id") int id, @Body SendTimeOutReq body, Callback<SendTimeOutResp> response);
	}

	public class SendTimeOutReq {
		public JsonObject homecare_homecarebundle_timeout;
	}

	public class SendTimeOutResp {
		public Boolean result;
	}


	interface GetTimeSheet {
		@POST("/create/timesheet")
		void getTimeSheetId(@Body GetTimeSheetReq body, Callback<GetTimeSheetResp> response);
	}


	public class GetTimeSheetReq {
		public String homecare_homecarebundle_timesheet = "";
	}

	public class GetTimeSheetResp {
		public Boolean result;
		public CreatedTimeSheet createdTimesheet;
	}

	public class CreatedTimeSheet {
		public int id;
	}


	interface SetTimeSheet {
		@PATCH("/timesheet/{id}")
		void setTimeSheet(@Path("id") int id, @Body TimeSheetReq body, Callback<TimeSheetResp> response);
	}

	public class TimeSheetReq {
		public JsonObject homecare_homecarebundle_timesheet;
	}

	public class TimeSheetResp {
		public Boolean result;
	}

	interface SendCareOptions {
		@POST("/careoptions")
		void setCareOptions(@Body SendCareOptionsReq body, Callback<SendCareOptionsResp> response);
	}

	public class SendCareOptionsReq {
		public JsonObject homecare_homecarebundle_careoptiontimesheet;
	}

	public class SendCareOptionsResp {
		public Boolean result;
	}

	interface DeleteCareOptions {
		@DELETE("/careoptions/{id}/delete")
		void removeCareOptions(@Path("id") int id, Callback<DeleteCareOptionsResp> response);
	}

	public class DeleteCareOptionsResp {
		public Boolean result;
	}

	interface RecipientSignature {
		@POST("/create/recipient/signature")
		void sendRecSig(@Body RecipientSigReq body, Callback<RecipientSigResp> response);
	}

	public class RecipientSigReq {
		public JsonObject homecare_homecarebundle_recipientsignature;
	}

	public class RecipientSigResp {
		public Boolean result;
	}

	interface PcaSignature {
		@POST("/create/pca/signature")
		void sendPcaSig(@Body PCASigReq body, Callback<PCASigResp> response);
	}

	public class PCASigReq {
		public JsonObject homecare_homecarebundle_pcasignature;
	}

	public class PCASigResp {
		public Boolean result;
	}

	interface SendPDF {
		@Multipart
		@POST("/files/{id}")
		void sendPdf(@Path("id") int id, @Part(value = "timesheet") int timesheetId, @Part(value = "timesheetPdfFile") TypedFile pdfFile, Callback<FileRespP> response);
	}

	interface SendCSV {
		@Multipart
		@POST("/files/{id}")
		void sendCsv(@Path("id") int id, @Part(value = "timesheet") int timesheetId, @Part(value = "timesheetCsvFile") TypedFile csvFile, Callback<FileRespP> response);
	}

	interface SendPSig {
		@Multipart
		@POST("/files/{id}")
		void sendSig(@Path("id") int id, @Part(value = "timesheet") int timesheetId, @Part(value = "timesheetPcaSig") TypedFile file, Callback<FileRespP> response);
	}

	interface SendRSig {
		@Multipart
		@POST("/files/{id}")
		void sendSig(@Path("id") int id, @Part(value = "timesheet") int timesheetId, @Part(value = "timesheetRecSig") TypedFile file, Callback<FileRespP> response);
	}

	interface FinishTimeSheet {
		@POST("/finish/timesheets")
		void finishTimesheet(@Body TimeSheetReq body, Callback<TimeSheetResp> response);
	}

	interface DeleteTimeSheets {
		@POST("/delete/timesheets")
		void deleteTimeSheets(@Body TimeSheetReq body, Callback<TimeSheetResp> response);
	}

	interface GetVerif {
		@GET("/pca/{pca_id}/verify/{rec_id}")
		void checkVerif(@Path("pca_id") int pId, @Path("rec_id") int rId, Callback<VerifResp> response);
	}

	class VerifResp {
		boolean result;
		boolean verification_needed;
		int id;
	}

	interface SetVerif {
		@PATCH("/verify/{id}")
		void setVerif(@Path("id") int rId, @Body VerifData body, Callback<VerifDataResp> response);
	}

	class VerifData {
		JsonObject homecare_homecarebundle_verification;
	}

	class VerifDataResp {
		boolean result;
	}

	interface sendVerifImage {
		@Multipart
		@POST("/verifcation/{id}/photo")
		void sendUpOutImage(@Path("id") int id, @Part(value = "verificationPhoto") TypedFile verifPhoto, Callback<FileResp> response);
	}

	interface RecAuth {
		@POST("/recipientpassword")
		void sendAuth(@Body RecAuthData body, Callback<RecAuthResp> response);
	}

	class RecAuthData {
		String password;
		int pcaId;
		int recipientId;
	}

	class RecAuthResp {
		boolean result;
		String status;
		String error;
	}

}
