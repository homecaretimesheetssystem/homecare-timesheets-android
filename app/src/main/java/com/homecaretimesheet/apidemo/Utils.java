package com.homecaretimesheet.apidemo;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
// import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
public class Utils {
	
	@SuppressLint("NewApi")
	static void verifyNetworkConnection(final Context context, int version) {
		if (!isNetworkConnected(context)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Network Disconnected")
			.setMessage("This app needs a Wi-Fi or mobile data connection." +
					"Would you like to check your settings?")
			.setNeutralButton("3G", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					Intent intentThreeG = new Intent( Settings.ACTION_DATA_ROAMING_SETTINGS);
					context.startActivity(intentThreeG);
					dialog.cancel();
				}
			})
			.setPositiveButton("Wi-Fi", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intentWiFi = new Intent( Settings.ACTION_WIFI_SETTINGS);
					context.startActivity(intentWiFi);
					dialog.cancel();					
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel(); 
				}
			});
			
			AlertDialog alert = builder.create();
			
			if (((Activity)context).isFinishing()) {
				return;
			}

			if ((version > android.os.Build.VERSION_CODES.JELLY_BEAN) &&
				((Activity)context).isDestroyed()) {
				return;
			}
			
			alert.show();
		}
	}
	
/*	public static boolean canDisplayPdf(Context context) {
	    PackageManager packageManager = context.getPackageManager();
	    Intent testIntent = new Intent(Intent.ACTION_VIEW);
	    testIntent.setType("application/pdf");
        return packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY).size() > 0;
	}
*/
	public static Boolean isNetworkConnected(Context context) {
		ConnectivityManager connManager = (ConnectivityManager) 
				context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mMobile = connManager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        return mWifi.isConnected() || mMobile.isConnected();
	}
}
