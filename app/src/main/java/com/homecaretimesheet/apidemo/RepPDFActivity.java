package com.homecaretimesheet.apidemo;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RepPDFActivity extends AppCompatActivity {
    public TimeSheetStorage store;
    public Context context;
    public int sdkVersion;

    private GridView gridView;
    private User user;
    private ResponseHandler respHandler;
    private ApiService apiService;
    private String api_token;
    private Dialog dialog;
    private boolean signAll = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = RepPDFActivity.this;
        store =  new TimeSheetStorage(this);
        setContentView(R.layout.activity_rec_pdf_show);
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        globalHCTS globals = (globalHCTS) getApplicationContext();
        user = globals.user;
        respHandler = new ResponseHandler();
        api_token = store.getDataString(TSDatum.API_TOKEN);
        apiService = new ApiService(api_token);
        getData();
        gridView = (GridView) findViewById(R.id.grid_layout);
    }

    public void getData() {
        apiService = new ApiService(api_token);
        ApiService.DataService datafetch = apiService.restAdapter.create(ApiService.DataService.class);
        datafetch.getData(dataResponse);
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    Callback<JsonObject> dataResponse = new Callback<JsonObject>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
                dialog.dismiss();
                returnToLogin();
            }
            else {
                dialog.dismiss();
                returnToLogin();
            }
        }
        @Override
        public void success(JsonObject loginResponse, Response response) {
            dialog.dismiss();
            respHandler.getResp(loginResponse, context);
            if (user.sessions.size() > 0) {
                signAll = true;
            }
            buildGrid();
        }
    };

    private void buildGrid() {
            ArrayList<SessionData> sessionArr = new ArrayList<>();
            for (SessionData session : user.sessions) {
                sessionArr.add(session);
            }
            SessionData session = new SessionData();
            sessionArr.add(session);
            final int lastItem = sessionArr.size() - 1;
            CustomGridViewAdapter adapter = new CustomGridViewAdapter(context, sessionArr);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position < lastItem) {
                        respHandler.setCurrentSessionRec(context, position);
                        Intent intent = new Intent(context, PDFShowActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else if (signAll) {
                        respHandler.setCurrentSessionRec(context, 0);
                        Intent intent = new Intent(context, GetClientSig.class);
                        intent.putExtra("sign", 1);
                        startActivity(intent);
                        finish();
                    }
                }
            });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        returnToLogin();
    }
    public void returnToLogin() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

}
