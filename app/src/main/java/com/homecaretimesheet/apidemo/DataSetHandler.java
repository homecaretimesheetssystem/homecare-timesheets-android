package com.homecaretimesheet.apidemo;

import com.google.gson.JsonArray;

import java.io.File;
import java.util.List;

class Agency {
	public Integer id;
	public String agency_name;
	public String phone_number;
	public State state;
	public String image_name;
	public List<Service> services;
}

@SuppressWarnings("unused")
class LoginPcaResp2 {
    public Boolean result;
    public String amazonUrl;
    public UserData user;
    public List<SessionData> sessionData;
    public Agency agency;
    public List<CareOption> careOptions;
    public List<Ratio> ratios;
    public List<Recipient> recipients;
    public List<SharedCareLocation> sharedCareLocations;
    public PCA pca;
    public String userType;
}

@SuppressWarnings("unused")
class LoginRecResp2 {
	public Boolean result;
	public String amazonUrl;
	public String userType;
	public List<PCA> pcas;
	public List<SessionData> sessionData;
	public UserData user;
	public Agency agency;
	public List<CareOption> careOptions;
}

@SuppressWarnings("unused")
class ServiceList {
	public List<Service> serviceList;
}

@SuppressWarnings("unused")
class UserData {
	public Integer id;
	public String email;
	public String userType;
}

class SharedCareLocation {
	public Integer id;
	public String location;
}

class Recipient {
	public Integer id;
	public String first_name;
	public String last_name;
	public String ma_number;
	public boolean skip_verification;
	public boolean skip_gps;
}

class Ratio {
	public Integer id;
	public String ratio;
}

class PCA {
	public Integer id;
	public String first_name;
	public String last_name;
	public String umpi;
	public List<Service> services;
	public List<Qualification> qualifications;
}

class onDemandPCA {
	public Integer id;
	public String first_name;
	public String last_name;
	public String phoneNumber;
	public String umpi;
	public String company_assigned_id;
	public Image image;
	public String home_address;
	public Integer gender;
	public List<Service> services;
	public List<Qualification> qualification_list;
	public List<Qualification> qualifications;
	public Boolean available;
	public Integer years_of_experience;
}

class PCAProfile {
	public Integer id;
	public String first_name;
	public String last_name;
	public String phoneNumber;
	public String umpi;
	public String company_assigned_id;
	public String image_url;
	public String local_image_path;
	JsonArray raw_qualification_ids;
	public File image;
	public String home_address;
	public Integer gender;
	public List<Service> services;
	public List<Qualification> qualification_list;
	public List<Qualification> qualifications;
	public Boolean available;
	public Integer years_of_experience;
	public double home_latitude;
	public double home_longitude;
	public boolean has_image;
}
class Image {
	public Integer id;
	public String path;
	public String original_name;
	public String mime_type;
	public File file;
}
class Qualification {
	public int id;
	public String qualification;
}
class CareOption {
	public Integer id;
	public String care_option;
	public boolean is_iadl;
}

class State {
	public Integer id;
	public String state_name;
}

class Service {
	public Integer id;
	public String service_name;
	public Integer service_number;
}

@SuppressWarnings("unused")
class SessionData {
	public Integer id;
	public Integer currentTimesheetId;
	public String dateEnding;
	public Integer continueTimesheetNumber;
	public Boolean flagForFurtherInvestigation;
	public PCA pca;
	public Ratio ratio;
	public Service service;
	public Integer currentTimesheetNumber;
	public SessionFileSet sessionFiles;
	public SharedCareLocation sharedCareLocation;
	public List<TimeSheet> timesheets;
	public TimeIn timeIn;
	public TimeOut timeOut;
	public List<TScareOption> careOptionTimesheets;
}

@SuppressWarnings("unused")
class TimeSheet {
	public Integer id;
	public List<TScareOption> careOptionTimesheets;
    public Integer finished;
    public Recipient recipient;
    public FileInfo file;
    public PCASig pca_signature;
	public Ratio ratio;
    public RecSig recipient_signature;
}

class TScareOption {
	public CareOption careOption;
}

@SuppressWarnings("unused")
class TimeIn {
	public Integer id;
	public Integer estimatedShiftLength;
	public String latitude;
	public String longitude;
	public String timeIn;
	public String timeInAddress;
	public String timeInPictureTime;
}

@SuppressWarnings("unused")
class TimeOut {
	public Integer id;
	public Integer billableHours;
	public String latitude;
	public String longitude;
	public String timeOut;
	public String timeOutAddress;
	public String timeOutPictureTime; 
	public Integer totalHours;
}

@SuppressWarnings("unused")
class SessionFileSet {
	public Integer id;
	public String timeInPhoto;
	public String timeOutPhoto;
}

class FileInfo {
	public Integer id;
	public String timesheetPcaSig;
	public String timesheetRecSig;
}

@SuppressWarnings("unused")
class PCASig {
	public Integer id;
	public String pcaSigTime;
	public String pcaSigAddress;
	public String latitude;
	public String longitude;
	public String pcaPhotoTime;
}

@SuppressWarnings("unused")
class RecSig {
	public Integer id;
	public String recipientSigTime;
	public String recipientSigAddress;
	public String latitude;
	public String longitude;
}