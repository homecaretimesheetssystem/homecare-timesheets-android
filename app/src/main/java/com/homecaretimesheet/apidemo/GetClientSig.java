package com.homecaretimesheet.apidemo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonObject;
import com.homecaretimesheet.apidemo.ApiService.FileRowReq;
import com.homecaretimesheet.apidemo.ApiService.FileRowResp;
import com.homecaretimesheet.apidemo.ApiService.RecAuth;
import com.homecaretimesheet.apidemo.ApiService.RecAuthData;
import com.homecaretimesheet.apidemo.ApiService.RecAuthResp;
import com.homecaretimesheet.apidemo.ApiService.RecipientSigReq;
import com.homecaretimesheet.apidemo.ApiService.RecipientSigResp;
import com.homecaretimesheet.apidemo.ApiService.RecipientSignature;
import com.homecaretimesheet.apidemo.ApiService.SendRSig;
import com.homecaretimesheet.apidemo.ApiService.getFileRow;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;



@SuppressLint("InflateParams")
public class GetClientSig extends AppCompatActivity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
	GestureOverlayView gestureOverlayView;
	Boolean disclaimed = false;
	Context context;
	private int sdkVersion;
	private TimeSheetStorage store;
	public Integer fileRowId = 0;
	private Location currentLocation = null;
	private String mLatitudeText = null;
	private String mLongitudeText = null;
	private ApiService apiService;
	private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
	public AlertDialog passAlert;
	private String passwd;
	public Button signed;
	private int signAll = 0;
	private User user;
	private boolean needsGps;
	private boolean isPca;
    private globalHCTS globals;

	private GoogleApiClient mGoogleApiClient;

	private LocationRequest mLocationRequest;
    private int CON_REQUEST = globalHCTS.CONNECT_REQUEST;
	private int UP_REQUEST = globalHCTS.UPDATE_REQUEST;
	private int SIGN_REQUEST = globalHCTS.TRY_REQUEST;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		store = new TimeSheetStorage(this);
		context = GetClientSig.this;
		sdkVersion = android.os.Build.VERSION.SDK_INT;
		setContentView(R.layout.activity_get_signer_sig);
		TextView heading = (TextView) findViewById(R.id.signTitle);
		heading.setText(R.string.rec_sign);
		gestureOverlayView = (GestureOverlayView) findViewById(R.id.gestureOverlayView1);
		signAll = getIntent().getIntExtra("sign", 0);
		globals = (globalHCTS) context.getApplicationContext();
		user = globals.user;
		needsGps = globals.needsGPS;
        isPca = globals.serviceChoice.getiValue() == 1;
		apiService = new ApiService(store.getDataString(TSDatum.API_TOKEN));
		signed = (Button) findViewById(R.id.done);
		signed.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				v.setEnabled(false);
				if (needsGps) {
					if (currentLocation != null) {
						onSignSave();
					}
					else {
						showLocationAlert();
						signed.setEnabled(true);
					}
				}
				else {
					onSignSave();
				}
			}
		});
		Button clear = (Button) findViewById(R.id.cLear);
		clear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onClear();
			}
		});
		if (needsGps) {
			final LocationManager manager2 = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			if (!manager2.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				buildAlertMessageNoGps();
			}
			buildGoogleApiClient();
			mGoogleApiClient.connect();
			createLocationRequest();
		}
		fileRowId = store.getDataInt(TSDatum.FILE_ROW_ID);

		if (!(fileRowId > 0)) {
			getFileRow();
		}
		Integer userType = user.userSet;
		if (userType < 2) {
			getCreds();
		} else if (!disclaimed) {
			showDisclaim();
		}

	}

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();
	}

	public void onClear() {
		gestureOverlayView.clear(false);
	}

	public void saveFile(Context context, Bitmap b, String picName) {
		FileOutputStream fos;
		try {
			String pngName = picName + ".png";
			fos = context.openFileOutput(pngName, Context.MODE_PRIVATE);
			b.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void onSignSave() {
		String fileName = "signatureC";
		gestureOverlayView.setDrawingCacheEnabled(true);
		Bitmap bm = Bitmap.createBitmap(gestureOverlayView.getDrawingCache());
		saveFile(this, bm, fileName);
		if (signAll > 0){
			startActivity(new Intent(context, SignAll.class));
			finish();
		}else {
			RecipientSignature recSig = apiService.restAdapter.create(RecipientSignature.class);
			RecipientSigReq sendSig = apiService.new RecipientSigReq();
			JsonObject recSigData = new JsonObject();
			Calendar c = Calendar.getInstance();
			String currentTime = serverDateFormat.format(c.getTime());
			recSigData.addProperty("timesheet", store.getDataInt(TSDatum.CURRENT_TIME_SHEET));
			recSigData.addProperty("recipientSigTime", currentTime);
			if (currentLocation != null && needsGps) {
				recSigData.addProperty("recipientSigAddress", globals.getAddress(context, currentLocation));
				recSigData.addProperty("latitude", mLatitudeText);
				recSigData.addProperty("longitude", mLongitudeText);
			}
			else {
				recSigData.addProperty("recipientSigAddress", "");
				recSigData.addProperty("latitude", 0);
				recSigData.addProperty("longitude", 0);
			}
			recSigData.addProperty("recipientPhotoTime", currentTime);
			store.setDataString(TSDatum.CLIENT_SIG_TIME, currentTime);
			sendSig.homecare_homecarebundle_recipientsignature = recSigData;
			recSig.sendRecSig(sendSig, getSigResp);
			signed.setClickable(false);
			signed.setBackground(ContextCompat.getDrawable(context, R.drawable.border_disabled));
		}
	}


	Callback<RecipientSigResp> getSigResp = new Callback<RecipientSigResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
			} else {

				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
				else if (r != null && r.getStatus() == 409){
					if (signAll > 0){
						nextActivity();
					}
					else {
						store.setDataBoolean(TSDatum.CLIENT_SIGNED, true);
						sendImage();					}
				}else {
					Toast.makeText(context, "Save Failed", sdkVersion).show();
				}
			}
			signed.setClickable(true);
			signed.setBackground(ContextCompat.getDrawable(context, R.drawable.border));
		}

		@Override
		public void success(RecipientSigResp sessionResponse, Response response) {
			store.setDataBoolean(TSDatum.CLIENT_SIGNED, true);
			sendImage();
		}

	};


	public void showDisclaim() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		String mssgDisclaim;
		if (isPca) {
			mssgDisclaim = getResources().getString(R.string.rec_disclaim_pca);
		} else {
			mssgDisclaim = getResources().getString(R.string.rec_disclaim);
		}
		alertDialogBuilder.setTitle("Proceed")
				.setMessage(mssgDisclaim)
				.setCancelable(false)
				.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						disclaimed = true;
						dialog.dismiss();
					}
				})

				.setNegativeButton(R.string.decline, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						declinePressed();
					}
				});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public void getCreds() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		LayoutInflater inflation = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View dialogLayout = inflation.inflate(R.layout.client_auth, null);
		alertDialogBuilder.setView(dialogLayout);

		alertDialogBuilder.setCancelable(false);
		final Button logButton = (Button) dialogLayout.findViewById(R.id.login);
		final Button backButton = (Button) dialogLayout.findViewById(R.id.goback);
		final AlertDialog alertDialog = alertDialogBuilder.create();
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, PDFShowActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				finish();
			}
		});

		logButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sendCreds();
			}
		});

		final EditText EditPassword = (EditText) dialogLayout.findViewById(R.id.EditPassword);

		EditPassword.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
									  int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				passwd = EditPassword.getText().toString();
			}
		});
		passAlert = alertDialog;
		passAlert.show();
	}

	public void sendCreds() {
		RecAuth sendAuth = apiService.restAdapter.create(RecAuth.class);
		RecAuthData authData = apiService.new RecAuthData();
		authData.password = passwd;
		authData.recipientId = store.getDataInt(TSDatum.CURRENT_RECIPIENT);
		authData.pcaId = store.getDataInt(TSDatum.PCA_ID);
		sendAuth.sendAuth(authData, getAuth);
	}

	Callback<RecAuthResp> getAuth = new Callback<RecAuthResp>() {
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
			} else {
				Toast.makeText(context, "Authorization Failed", sdkVersion).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(RecAuthResp sessionResponse, Response response) {
			if (sessionResponse.error == null) {
				passAlert.dismiss();
				if (!disclaimed) {
					showDisclaim();
				}
			} else {
				Toast.makeText(context, "Authorization Failed", sdkVersion).show();
			}
		}
	};

	public void sendImage() {
		SendRSig sendSig = apiService.restAdapter.create(SendRSig.class);
		File image = getFileStreamPath("signatureC.png");
		user.setSignature(image);
		TypedFile imageFile = new TypedFile("multipart/form-data", image);
		sendSig.sendSig(store.getDataInt(TSDatum.FILE_ROW_ID), store.getDataInt(TSDatum.CURRENT_TIME_SHEET), imageFile, sentSig);
	}


	Callback<ApiService.FileRespP> sentSig = new Callback<ApiService.FileRespP>() {
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
			signed.setClickable(true);
			signed.setBackground(ContextCompat.getDrawable(context, R.drawable.border));
		}


		@Override
		public void success(ApiService.FileRespP sessionResponse, Response response) {
			String location = sessionResponse.uploadedFileLocation.get(0);
			store.setDataString(TSDatum.REC_SIG, location);
			store.setDataBoolean(TSDatum.CLIENT_SIGNED, true);
			nextActivity();
		}
	};

	public void nextActivity() {
 		if (signAll > 0 && user.sessions.size() > 1) {
            Intent intent = new Intent(context, SignAll.class);
			startActivity(intent);
			finish();
		}
		else {
			Intent intent = new Intent(context, PDFShowActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
			startActivity(intent);
			finish();
		}
	}

	@Override
	public void onConnectionSuspended(int arg0) {}

	public void getFileRow() {
	   	getFileRow getFileId = apiService.restAdapter.create(getFileRow.class);
	   	FileRowReq getFileIdBody = apiService.new FileRowReq();
	   	getFileId.createFileRow(getFileIdBody, newFileRow);
	}
	    
	Callback<FileRowResp> newFileRow = new Callback<FileRowResp>() {
	   	@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
			signed.setClickable(true);
			signed.setBackground(ContextCompat.getDrawable(context, R.drawable.border));
		}

		@Override
		public void success(FileRowResp sessionResponse, Response response) {
			store.setDataInt(TSDatum.FILE_ROW_ID, sessionResponse.createdFilesId);
		}
	};

	@Override
	public void onBackPressed() {
		showSure();
	}


	public void showSure() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		alertDialogBuilder.setTitle("Warning")
				.setMessage(R.string.sig_sure)
				.setCancelable(false)
				.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
						Intent intent = new Intent(context, PDFShowActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(intent);
						finish();
					}
				})

				.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public void declinePressed() {
		Intent intent = new Intent(context, PDFShowActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}

	public void returnToLogin() {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}

	protected void onStart() {
		super.onStart();
		if (mGoogleApiClient != null) {
			mGoogleApiClient.connect();
		}
	}

	protected void onStop() {
		if (mGoogleApiClient != null) {
			mGoogleApiClient.disconnect();
		}
		super.onStop();
	}

	@Override
	public void onConnectionFailed(@Nullable ConnectionResult arg0) {
		Toast.makeText(context, "Location Service Failed", sdkVersion).show();
		mGoogleApiClient.connect();
	}


	@Override
	public void onRequestPermissionsResult(int requestCode,
										   @NonNull String permissions[], @NonNull int[] grantResults) {
		if (requestCode == CON_REQUEST) {
			if (grantResults.length > 0	&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				mGoogleApiClient.connect();
			} else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == SIGN_REQUEST) {
			if (grantResults.length > 0	&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				onSignSave();
			} else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Storage");
				alert.show();
			}
		}
		else if (requestCode == UP_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				startLocationUpdates();
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
	}

	protected void createLocationRequest() {
		int UPDATE_INTERVAL = 10000; // 10 sec
		int FATEST_INTERVAL = 5000; // 5 sec
		int DISPLACEMENT = 100; // 100 meters
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT); // 100 meters
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, UP_REQUEST);
		}
	}

	protected void startLocationUpdates() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, UP_REQUEST);
			return;
		}
		LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
	}

	@Override
	public void onLocationChanged(Location location) {
		currentLocation = location;
		if (currentLocation != null) {
			mLatitudeText = String.valueOf(currentLocation.getLatitude());
			mLongitudeText = String.valueOf(currentLocation.getLongitude());
		}
	}

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},	CON_REQUEST);
            return;
        }
        createLocationRequest();
        startLocationUpdates();
        if (currentLocation == null) {
            LocationServices.FusedLocationApi.flushLocations(mGoogleApiClient);
            currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
        else {
            mLatitudeText = String.valueOf(currentLocation.getLatitude());
            mLongitudeText = String.valueOf(currentLocation.getLongitude());
        }
    }

	@Override
	public void onRestart() {
		if (mGoogleApiClient.isConnected()) {
			startLocationUpdates();
		}
		else {
			buildGoogleApiClient();
			createLocationRequest();
		}
		super.onRestart();
	}

	private void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
				.setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
						startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
					}
				});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	public void showLocationAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("No Location Detected")
				.setMessage(R.string.get_location)
				.setCancelable(false)
				.setPositiveButton(R.string.cont, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

}
