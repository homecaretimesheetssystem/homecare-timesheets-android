package com.homecaretimesheet.apidemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.opencsv.CSVWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class FinishAll extends Activity {
    public TimeSheetStorage store;
    public Context context;
    public int sdkVersion;

    private Date timeInSet = null;
    private Date timeOutSet = null;
    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
    private SimpleDateFormat timePunchFormat = new SimpleDateFormat("hh:mm", Locale.US);
    private SimpleDateFormat textDateFormat = new SimpleDateFormat("hh:mm aa", Locale.US);
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
    private Bitmap sigC;
    private Bitmap sigP;
    private ApiService apiService;
    private String recFirst;
    private String recLast;
    private String recMA;
    private String ratioText = "";
    private Integer fileRowId;
    private Integer timesheetId;
    private ResponseHandler respHandler;
    private int curIndex;
    private List<SessionData> sessions;
    private String api_token;
    private ArrayList<CareOption> listCareOpts;
    private ArrayList<Integer> selOpts;
    private globalHCTS globals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = FinishAll.this;
        store = new TimeSheetStorage(this);
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        setContentView(R.layout.activity_pdf);
        setActiveTime();
        api_token = store.getDataString(TSDatum.API_TOKEN);
        respHandler = new ResponseHandler();
        getData();
        curIndex = 0;
        globals = (globalHCTS) getApplicationContext();
    }

    public void getData() {
        apiService = new ApiService(api_token);
        ApiService.DataService datafetch = apiService.restAdapter.create(ApiService.DataService.class);
        datafetch.getData(dataResponse);
    }

    Callback<JsonObject> dataResponse = new Callback<JsonObject>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            } else {
                Toast.makeText(context, "Login Failed", sdkVersion).show();
            }
        }

        @Override
        public void success(JsonObject loginResponse, retrofit.client.Response response) {
            respHandler.getResp(loginResponse, context);
            startPdfs();
        }
    };

    public void startDownload() {
        String pca_image = store.getDataString(TSDatum.AMAZON_URL) + store.getDataString(TSDatum.PCA_SIG);
        pca_image = pca_image.replace("\"", "");
        new DownloadImageTaskP()
                .execute(pca_image);
        String rec_image = store.getDataString(TSDatum.AMAZON_URL) + store.getDataString(TSDatum.REC_SIG);
        rec_image = rec_image.replace("\"", "");
        new DownloadImageTaskR()
                .execute(rec_image);
    }


    private class DownloadImageTaskP extends AsyncTask<String, Bitmap, Bitmap> {
        public DownloadImageTaskP() {}

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            sigP = result;
            updateActivity();
        }
    }

    private class DownloadImageTaskR extends AsyncTask<String, Bitmap, Bitmap> {
        public DownloadImageTaskR() {}

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            sigC = result;
            updateActivity();
        }
    }

    public void updateActivity() {
        if (sigP != null && sigC != null) {
            continuePDF();
        }
    }

    public void startPdfs() {
        User user = globals.user;
        sessions = user.sessions;
        respHandler.setCurrentSessionRec(context, curIndex);
        startDownload();
    }

    public void continuePDF() {
        globalHCTS.ServiceChoice serviceChoice = globals.serviceChoice;
        if (serviceChoice == null) {
            serviceChoice = globalHCTS.ServiceChoice.fromInt(store.getDataInt(TSDatum.CURRENT_SERVICE_INT));
        }
        boolean needsGps = globals.needsGPS;
        Bitmap bitmap = Bitmap.createBitmap(600, 725, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(ContextCompat.getColor(context, R.color.clear));
        Paint paint = new Paint();
        Canvas canvas = new Canvas(bitmap);
        paint.setColor(ContextCompat.getColor(context, android.R.color.black));
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setTypeface(Typeface.SANS_SERIF);
        String location = "";
        String timeInAddress = "";
        String timeOutAddress = "";
        paint.setTextSize(16);
        String agencyName = store.getDataString(TSDatum.AGENCY_NAME);
        String agencyPhone = store.getDataString(TSDatum.AGENCY_PHONE);
        canvas.drawText(agencyName, 120f, 122f, paint);
        canvas.drawText(agencyPhone, 442f, 122f, paint);
        String allCareOpts = store.getDataString(TSDatum.CARE_OPTIONS);
        TypeToken<ArrayList<CareOption>> token = new TypeToken<ArrayList<CareOption>>() {
        };
        listCareOpts = new Gson().fromJson(allCareOpts, token.getType());
        selOpts = new ArrayList<>();

        if (serviceChoice != null && serviceChoice.hasDL()) {
          addCareOptions(canvas, paint);
        }
        String timeIn = store.getDataString(TSDatum.TIME_IN);
        String timeOut = store.getDataString(TSDatum.TIME_OUT);
        try {
            timeInSet = serverDateFormat.parse(timeIn);
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        try {
            timeOutSet = serverDateFormat.parse(timeOut);
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        String dateEnding = dateFormat.format(timeOutSet);
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);

        String weekDay = dayFormat.format(timeOutSet);
        canvas.drawText(dateEnding, 208, 160, paint);
        Calendar c = Calendar.getInstance();
        c.setTime(timeInSet);
        int amPmIn = c.get(Calendar.AM_PM);
        int topR;
        int leftR;
        int bottomR;
        int rightR;
        if (amPmIn == Calendar.AM) {
            topR = 388;
            leftR = (int) 511.2;
            bottomR = 398;
            rightR = (int) 533.2;
        }
        else {
            topR = 398;
            leftR = 512;
            bottomR = 408;
            rightR = 532;
        }
        String timeInString = textDateFormat.format(timeInSet);
        String timeOutString = textDateFormat.format(timeOutSet);
        String timeInPunch = timePunchFormat.format(timeInSet);
        String timeOutPunch = timePunchFormat.format(timeOutSet);
        canvas.drawText(timeInPunch, 462,(int) 404.2, paint);
        canvas.drawText(timeOutPunch, 462, 432, paint);

        RectF oval = new RectF(leftR, topR, rightR, bottomR);
        paint.setStyle(Paint.Style.STROKE);

        canvas.drawOval(oval, paint);
        c.setTime(timeOutSet);
        int amPmOut = c.get(Calendar.AM_PM);
        int topR2;
        int leftR2;
        int bottomR2;
        int rightR2;

        if (amPmOut == Calendar.AM) {
            topR2 = (int) 414.4;
            leftR2 = (int) 511.2;
            bottomR2 = (int) 424.4;
            rightR2 = (int) 533.2;
        }
        else {
            topR2 = (int) 424.4;
            leftR2 = 512;
            bottomR2 = (int) 434.4;
            rightR2 = 532;
        }

        RectF oval2 = new RectF(leftR2, topR2, rightR2, bottomR2);
        canvas.drawOval(oval2, paint);
        if (serviceChoice != null && serviceChoice.hasRatio()) {
            int ratio = store.getDataInt(TSDatum.RATIO_INT);
            if (ratio > 0) {
                int topR3 = 320;
                int bottomR3 = 348;
                int leftR3;
                int rightR3;

                if (ratio == 3) {
                    leftR3 = 507;
                    rightR3 = 535;
                    ratioText = "1:3";
                } else if (ratio == 2) {
                    leftR3 = 476;
                    rightR3 = 504;
                    ratioText = "1:2";
                } else {
                    leftR3 = (int) 439.5;
                    rightR3 = (int) 467.5;
                    ratioText = "1:1";
                }
                RectF oval3 = new RectF(leftR3, topR3, rightR3, bottomR3);
                canvas.drawOval(oval3, paint);
            }

            paint.setTypeface(Typeface.DEFAULT);
            int xLoc = 467;
            int yLoc = 370;
            location = store.getDataString(TSDatum.CURRENT_LOCATION_TEXT);
            paint.setTextSize(24);
            paint.setStyle(Paint.Style.FILL);
            if (location.equals("HOME")) {
                paint.setTextSize(24);
                xLoc = 456;
                yLoc = 370;
            } else if (location.equals("COMMUNITY")) {
                paint.setTextSize(17);
                xLoc = (int) 442.5;
                yLoc = 376;
            }
            canvas.drawText(location, xLoc, yLoc, paint);
        }
        paint.setStyle(Paint.Style.FILL);
        String shiftString = getFullShift();
        paint.setTextSize(20);
        canvas.drawText(shiftString, 458, (int) 465.5, paint);
        paint.setTextSize(16);
        if (needsGps) {
            timeInAddress = store.getDataString(TSDatum.TIME_IN_ADD);
            timeOutAddress = store.getDataString(TSDatum.TIME_OUT_ADD);
            if (timeInAddress != null) {
                canvas.drawText(timeInAddress, 135, (int) 554.5, paint);
            }
            if (timeOutAddress != null) {
                canvas.drawText(timeOutAddress, 149, (int) 588.5, paint);
            }
        }
        String allRecs = store.getDataString(TSDatum.RECIPIENTS);
        TypeToken<ArrayList<Recipient>> token3 = new TypeToken<ArrayList<Recipient>>(){};
        ArrayList<Recipient> listRecs = new Gson().fromJson(allRecs, token3.getType());
        int currentRec = store.getDataInt(TSDatum.CURRENT_RECIPIENT);
        for (Recipient ele : listRecs) {
            if (ele.id == currentRec) {
                recFirst = ele.first_name.toUpperCase();
                recLast = ele.last_name.toUpperCase();
                String name = recFirst + " " + recLast;
                name = fixText(name, 150, paint);
                recMA = ele.ma_number;
                String localMaNum = fixText(ele.ma_number, 90, paint);
                canvas.drawText(name, 70, 680, paint);
                canvas.drawText(localMaNum, 232, 680, paint);
            }
        }
        String pcaFirst = store.getDataString(TSDatum.PCA_FIRST);
        String pcaLast = store.getDataString(TSDatum.PCA_LAST);
        String pcaUmpi = store.getDataString(TSDatum.PCA_UMPI);
        String pca_name =  pcaFirst + " " + pcaLast ;
        pca_name = fixText(pca_name, 150, paint);
        pcaUmpi = fixText(pcaUmpi, 90, paint);
        canvas.drawText(pca_name, 70, 708, paint);
        canvas.drawText(pcaUmpi, 235, 708, paint);
        String pcaSigT = store.getDataString(TSDatum.PCA_SIG_TIME);
        String clientSigT = store.getDataString(TSDatum.CLIENT_SIG_TIME);
        Date pcaSigCal = null;
        Date clientSigCal = null;
        try {
            pcaSigCal = serverDateFormat.parse(pcaSigT);
        } catch (ParseException e3) {
            e3.printStackTrace();
        }
        try {
            clientSigCal = serverDateFormat.parse(clientSigT);
        } catch (ParseException e3) {
            e3.printStackTrace();
        }
        String pcaSigTime = dateFormat.format(pcaSigCal);
        String clientSigTime = dateFormat.format(clientSigCal);
        paint.setTextSize(13);
        if (pcaSigCal != null) {
            canvas.drawText(pcaSigTime, 485, 708, paint);
        }
        if (clientSigCal != null) {
            canvas.drawText(clientSigTime, 485, 680, paint);
        }
        canvas.save();

        FileOutputStream streamOut;
        try {
            streamOut = context.openFileOutput("inserted.png", Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, streamOut);
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
        try {
            java.io.InputStream f;
            if (serviceChoice == null) {
                f = getAssets().open("pca.pdf");
            }
            else if (serviceChoice.equals(globalHCTS.ServiceChoice.PCA)) {
                f = getAssets().open("pca.pdf");
            } else if (serviceChoice.equals(globalHCTS.ServiceChoice.HOMEMAKING)) {
                f = getAssets().open("homemaking.pdf");
            } else if (serviceChoice.equals(globalHCTS.ServiceChoice.RESPITE)) {
                f = getAssets().open("respite.pdf");
            } else if (serviceChoice.equals(globalHCTS.ServiceChoice.ENVIRONMENTAL)) {
                f = getAssets().open("environmentalmodifications.pdf");
            } else if (serviceChoice.equals(globalHCTS.ServiceChoice.CSUPPORT)) {
                f = getAssets().open("consumersupport.pdf");
            } else if (serviceChoice.equals(globalHCTS.ServiceChoice.CAREGIVING)) {
                f = getAssets().open("caregivingexpense.pdf");
            } else if (serviceChoice.equals(globalHCTS.ServiceChoice.TREATMENT)) {
                f = getAssets().open("treamentandtraining.pdf");
            } else if (serviceChoice.equals(globalHCTS.ServiceChoice.SDSUPPORT)) {
                f = getAssets().open("selfdirectionsupport.pdf");
            } else if (serviceChoice.equals(globalHCTS.ServiceChoice.PSUPPORT)) {
                f = getAssets().open("personalsupport.pdf");
            } else if (serviceChoice.equals(globalHCTS.ServiceChoice.ASSISTANCE)) {
                f = getAssets().open("personalassistance.pdf");
            } else {
                f = getAssets().open("pdfraw.pdf");
            }
            PdfReader pdfReader = new PdfReader(f);
            String fname = "TimeSheet.pdf";
            FileOutputStream out = context.openFileOutput(fname, Context.MODE_PRIVATE);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, out);
            File overLay = getFileStreamPath("inserted.png");
            Image image = Image.getInstance(overLay .getAbsolutePath());
            PdfContentByte content = pdfStamper.getOverContent(1);
            image.setAbsolutePosition(0f, 50f);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            sigC.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image imageSigC = Image.getInstance(stream.toByteArray());
            imageSigC.scaleAbsolute(150, 21);
            imageSigC.setAbsolutePosition(331, 91);
            content.addImage(imageSigC);
            ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
            sigP.compress(Bitmap.CompressFormat.PNG, 100, stream2);
            Image imageSigP = Image.getInstance(stream2.toByteArray());
            imageSigP.scaleAbsolute(150, 21);
            imageSigP.setAbsolutePosition(331, 62);
            content.addImage(imageSigP);
            content.addImage(image);
            pdfStamper.close();
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        }

        apiService = new ApiService(store.getDataString(TSDatum.API_TOKEN));
        CSVWriter writer;
        try {
            File csvFile = new File(context.getFilesDir() + "/timesheetCsv.csv");
            writer = new CSVWriter(new FileWriter(csvFile), ',');
            String headString = "state,service,agency_name,phone_number,day_of_service,date_of_service,recipient_first_name,recipient_last_name,";
            headString += "recipient_ma_number,recipient_date_signed,pca_first_name,pca_last_name,pca_npi_umpi,pca_date_signed,";
            headString += "dressing,grooming,bathing,eating,transfers,mobility,positioning,toileting,health_related,behavior,light_housekeeping,laundry,other,";
            headString += "visit_one_ratio,visit_one_location,visit_one_time_in,visit_one_time_in_address,visit_one_time_out,visit_one_time_out_address,";
            headString += "recipient_signature_image_timestamp,pca_signature_image_timestamp,daily_total,company_assigned_id";
            String[] heading = headString.split(",");
            writer.writeNext(heading);
            String writeString = "";
            writeString += store.getDataString(TSDatum.AGENCY_STATE) + "#";
            writeString += store.getDataString(TSDatum.CURRENT_SERVICES_TEXT) + "#";
            writeString += agencyName + "#";
            writeString += agencyPhone + "#";
            writeString += weekDay + "#";
            writeString += dateEnding + "#";
            writeString += recFirst + "#";
            writeString += recLast + "#";
            writeString += recMA + "#";
            writeString += dateEnding + "#";
            writeString += pcaFirst + "#";
            writeString += pcaLast + "#";
            writeString += pcaUmpi + "#";
            writeString += dateEnding + "#";
            for (CareOption ele : listCareOpts) {
                if (selOpts.contains(ele.id)) {
                    writeString += "1" + "#";
                } else {
                    writeString += "0" + "#";
                }
            }
            writeString += ratioText + "#";
            writeString += location + "#";
            writeString += timeInString + "#";
            writeString += timeInAddress + "#";
            writeString += timeOutString + "#";
            writeString += timeOutAddress + "#";
            writeString += store.getDataString(TSDatum.CLIENT_SIG_TIME) + "#";
            writeString += store.getDataString(TSDatum.PCA_SIG_TIME) + "#";
            writeString += shiftString + "#";
            writeString += store.getDataInt(TSDatum.AGENCY_ID);

            String[] entries = writeString.split("#");
            writer.writeNext(entries);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ApiService.SendPDF sendPdf = apiService.restAdapter.create(ApiService.SendPDF.class);
        String fname = "TimeSheet.pdf";
        File pdf = getFileStreamPath(fname);
        fileRowId = store.getDataInt(TSDatum.FILE_ROW_ID);
        timesheetId = store.getDataInt(TSDatum.CURRENT_TIME_SHEET);
        TypedFile pdfFile = new TypedFile("multipart/form-data", pdf);
        sendPdf.sendPdf(fileRowId, timesheetId, pdfFile, sendPdfResp);
    }

    Callback<ApiService.FileRespP> sendPdfResp = new Callback<ApiService.FileRespP>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                Toast.makeText(context, "Save Failed PDF", sdkVersion).show();
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
            }
        }

        @Override
        public void success(ApiService.FileRespP fileResp, Response response) {
            sendCSV();
        }
    };

    public void sendCSV() {
        ApiService.SendCSV sendCsvFile = apiService.restAdapter.create(ApiService.SendCSV.class);
        File csv = getFileStreamPath("timesheetCsv.csv");
        TypedFile csvFile = new TypedFile("multipart/form-data", csv);
        sendCsvFile.sendCsv(fileRowId, timesheetId, csvFile, sentCSV);
    }

    Callback<ApiService.FileRespP> sentCSV = new Callback<ApiService.FileRespP>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                Response r = code.getResponse();
                Toast.makeText(context, "Save Failed CSV", sdkVersion).show();

                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
            }
        }

        @Override
        public void success(ApiService.FileRespP fileResp, Response response) {
            nextAction();
        }
    };

    public void nextAction() {
        ApiService.FinishTimeSheet finished = apiService.restAdapter.create(ApiService.FinishTimeSheet.class);
        ApiService.TimeSheetReq finishReq = apiService.new TimeSheetReq();
        JsonObject finishData = new JsonObject();
        Integer sessionId = store.getDataInt(TSDatum.SESSION_ID);
        finishData.addProperty("sessionId", sessionId);
        finishReq.homecare_homecarebundle_timesheet = finishData;
        finished.finishTimesheet(finishReq, finishResp);
    }

    Callback<ApiService.TimeSheetResp> finishResp = new Callback<ApiService.TimeSheetResp>() {
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                Toast.makeText(context, "Save Failed TS", sdkVersion).show();
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
            }
        }

        @Override
        public void success(ApiService.TimeSheetResp fileResp, Response response) {
            Toast.makeText(context, "Time Entry Completed " + Integer.valueOf(curIndex + 1).toString(), sdkVersion).show();
            nextTimesheet();
        }
    };

    public void nextTimesheet() {
        sigC = null;
        sigP = null;
        curIndex++;
        if (sessions.size() > curIndex) {
            respHandler.setCurrentSessionRec(context, curIndex);
            startDownload();
        }
        else {
            nextActivity();

        }
    }
    public void nextActivity(){
        Toast.makeText(context, "Time Entries Completed", Toast.LENGTH_SHORT).show();
        returnToLogin();
    }

    public void setActiveTime() {
        Integer time = (int) (long) System.currentTimeMillis();
        store.setDataInt(TSDatum.LAST_ACTIVITY, time);
    }

    public void returnToLogin() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void addCareOptions(Canvas canvas, Paint paint) {
        String check = Html.fromHtml("&#x2713;").toString();
        paint.setTextSize(18);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        String selCareOpts = store.getDataString(TSDatum.CURRENT_CARE_OPTIONS);
        String[] selCareOptArray = selCareOpts.replaceAll("\\[|\\]| ", "").split(",");
        for (String aSelCareOptArray : selCareOptArray) {
            selOpts.add(Integer.valueOf(aSelCareOptArray));
        }
        int i = 0;

        for (CareOption ele : listCareOpts) {
            int y;
            int x;
            if (selOpts.contains(ele.id)) {
                if (!ele.is_iadl) {
                    y = (int) (211 + (28.2 * i));
                    x = 264;
                } else {
                    y = (int) (211 + (28.2 * (i - 10)));
                    x = 498;
                }
                canvas.drawText(check, x, y, paint);
            }
            i++;
        }
    }

    public String fixText(String text, float width, Paint paint) {
        char[] chars = text.toCharArray();
        int nextPos = paint.breakText(chars, 0, chars.length, width, null);
        text = text.substring(0, nextPos);
        return text;
    }

    public String getFullShift() {
        long shiftLength = timeOutSet.getTime() - timeInSet.getTime();
        Integer fullShift = ((int) (long) shiftLength)/1000;
        Integer minutes = (fullShift / 60 % 60) - (fullShift / 60 % 15);
        Integer hours = fullShift / (60 * 60);
        String hour = hours.toString();
        String minute;
        if (minutes > 10) {
            minute = minutes.toString();
        }
        else if (minutes == 0) {
            minute = "00";
        }
        else {
            minute = "0" + minutes.toString();
        }
        return (hour + ":" + minute);
    }

}
