package com.homecaretimesheet.apidemo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class CustomGridViewAdapter extends ArrayAdapter {
    Context context;
    ArrayList<SessionData> sessions;
    int counter;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yy", Locale.US);
    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
    private SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa", Locale.US);


    public CustomGridViewAdapter(Context context, ArrayList<SessionData> sessions)
    {
        super(context, 0);
        this.context=context;
        this.sessions = sessions;
        this.counter = 0;
    }
    public int getCount()
    {
        return sessions.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            if (position < (sessions.size()-1)) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                row = inflater.inflate(R.layout.activity_show_pdf, parent, false);
                SessionData curSession = sessions.get(position);
                String workertext = "Worker: " + curSession.pca.first_name.toUpperCase() + " " + curSession.pca.last_name.toUpperCase();
                globalHCTS globals = (globalHCTS) context.getApplicationContext();
                String dateString = curSession.timeIn.timeIn;
                String timeInString = "Time In: --:-- --";
                String timeOutString = "Time Out: --:-- --";
                String totalString = "Total: 0:00";
                try {
                    Date date = serverDateFormat.parse(dateString);
                    Date out = serverDateFormat.parse(curSession.timeOut.timeOut);
                    timeInString = "Time In: " + timeFormat.format(date);
                    timeOutString = "Time Out: " + timeFormat.format(out);
                    dateString = dateFormat.format(date);
                    long shiftLength = out.getTime() - date.getTime();
                    Integer shiftLeng = ((int) (long) shiftLength) / 1000;
                    Integer minutes = (shiftLeng / 60 % 60) - (shiftLeng / 60 % 15);
                    Integer hours = shiftLeng / (60 * 60);
                    String hour = hours.toString();
                    String minute;
                    if (minutes > 10) {
                        minute = minutes.toString();
                    } else if (minutes == 0) {
                        minute = "00";
                    } else {
                        minute = "0" + minutes.toString();
                    }
                    totalString = "Total: " + hour + ":" + minute;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                TextView workerView = (TextView) row.findViewById(R.id.worker);
                TextView dateView = (TextView) row.findViewById(R.id.date);
                TextView timeInView = (TextView) row.findViewById(R.id.timein);
                TextView timeOutView = (TextView) row.findViewById(R.id.timeout);
                TextView totalView = (TextView) row.findViewById(R.id.total);
                ImageView underView = (ImageView) row.findViewById(R.id.under);
                workerView.setText(workertext);
                dateView.setText(dateString);
                timeInView.setText(timeInString);
                timeOutView.setText(timeOutString);
                totalView.setText(totalString);

                int serviceSelected = 1;
                if (curSession.service != null) {
                    serviceSelected = curSession.service.service_number;
                }
                globals.serviceChoice = globalHCTS.ServiceChoice.fromInt(serviceSelected);
                switch (serviceSelected) {
                    case 1:
                        underView.setImageResource(R.drawable.pca);
                        break;
                    case 3:
                        underView.setImageResource(R.drawable.respite);
                        break;
                    case 5:
                        underView.setImageResource(R.drawable.environmentalmodifications);
                        break;
                    case 8:
                        underView.setImageResource(R.drawable.consumersupport);
                        break;
                    case 4:
                        underView.setImageResource(R.drawable.caregivingexpense);
                        break;
                    case 10:
                        underView.setImageResource(R.drawable.treatmentandtraining);
                        break;
                    case 7:
                        underView.setImageResource(R.drawable.selfdirectionsupport);
                        break;
                    case 6:
                        underView.setImageResource(R.drawable.personalsupport);
                        break;
                    case 9:
                        underView.setImageResource(R.drawable.personalassistance);
                        break;
                    case 2:
                        underView.setImageResource(R.drawable.homemaking);
                        break;
                    default:
                        underView.setImageResource(R.drawable.pca);
                        break;
                }
                counter++;
            }
            else {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                row = inflater.inflate(R.layout.sign_all_button, parent, false);
            }
        }
        return row;
    }

}
