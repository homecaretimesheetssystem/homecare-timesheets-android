package com.homecaretimesheet.apidemo;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.Settings;

import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class globalHCTS extends Application {
    public User user;
    public static int CONNECT_REQUEST = 10001;
    public static int UPDATE_REQUEST = 10011;
    public static int TRY_REQUEST = 10012;
    public static int STORAGE_REQUEST = 10013;
    public static int NETWORK_REQUEST = 10014;
    public static int INTERNET_REQUEST = 10015;
    public boolean netPerm;
    public boolean inetPerm;
    public boolean locPerm;
    public boolean storPerm;
    public boolean camPerm;
    public boolean needsVerif;
    public boolean needsGPS;
    public ServiceChoice serviceChoice = null;
    public List<Qualification> qualification_list;

    public boolean testing = true;

    public enum ServiceChoice {
        PCA(1),
        RESPITE(3),
        ENVIRONMENTAL(5),
        CSUPPORT(8),
        CAREGIVING(4),
        TREATMENT(10),
        SDSUPPORT(7),
        PSUPPORT(6),
        ASSISTANCE(9),
        HOMEMAKING(2),
        ONDEMAND(11);
        private int _iValue = 1;
        private Integer[] ratioed = {1, 3, 8, 9, 10};
        private Integer[] allDL = {1, 2, 11};

        ServiceChoice(int value) {
            this._iValue = value;
        }

        public static ServiceChoice fromInt(int value) {
            for (ServiceChoice b : ServiceChoice.values()) {
                if (b.getiValue() == value) {
                    return b;
                }
            }
            return null;
        }

        int getiValue() {
            return this._iValue;
        }

        boolean hasDL() {
            return Arrays.asList(allDL).contains(this._iValue);
        }

        boolean hasRatio() {
            return Arrays.asList(ratioed).contains(this._iValue);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        user = new User();
        netPerm = false;
        inetPerm = false;
        locPerm = false;
        storPerm = false;
        camPerm = false;
        needsVerif = true;
        needsGPS = true;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }



    public void setServiceChoice(int choice) {
        serviceChoice = ServiceChoice.fromInt(choice);
    }

    public void setPerms(Context context) {
        PermissionService perService = new PermissionService();
        netPerm = perService.getNetworkPermission(context);
        inetPerm = perService.getInternetPermission(context);
        locPerm = perService.getLocationPermission(context);
        storPerm = perService.getStoragePermission(context);
        camPerm = perService.getCameraPermission(context);
    }


    public String getAddress(Context context, Location locate) {
        String currentAddress = "";
        Geocoder gCoder = new Geocoder(context);
        List<Address> addresses;
        try {
            addresses = gCoder.getFromLocation(locate.getLatitude(), locate.getLongitude(), 1);
        } catch (IOException e) {
            addresses = null;
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            Address cAddress = addresses.get(0);
            String cAdd1 = "";
            String cAdd2 = "";
            if (cAddress.getAddressLine(0) != null) {
                cAdd1 = cAddress.getAddressLine(0).toUpperCase();
                if (cAddress.getAddressLine(1) != null) {
                    cAdd2 = cAddress.getAddressLine(1).toUpperCase();
                }
            }
            currentAddress = cAdd1 + " " + cAdd2;
        }
        return currentAddress;
    }

    AlertDialog buildAlertNoPermission(final Context context, String permName) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.app_name) + " requires " + permName + " Permissions")
                .setCancelable(false)
                .setPositiveButton("Go to Permissions", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Intent i = new Intent();
                        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addCategory(Intent.CATEGORY_DEFAULT);
                        i.setData(Uri.parse("package:" + context.getPackageName()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        context.startActivity(i);
                    }
                });
        final AlertDialog alert = builder.create();
        return alert;
    }


    public boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }


}
