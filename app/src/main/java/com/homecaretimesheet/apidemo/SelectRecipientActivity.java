package com.homecaretimesheet.apidemo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.homecaretimesheet.apidemo.ApiService.FileResp;
import com.homecaretimesheet.apidemo.ApiService.GetTimeSheet;
import com.homecaretimesheet.apidemo.ApiService.GetTimeSheetReq;
import com.homecaretimesheet.apidemo.ApiService.GetTimeSheetResp;
import com.homecaretimesheet.apidemo.ApiService.GetVerif;
import com.homecaretimesheet.apidemo.ApiService.SetTimeSheet;
import com.homecaretimesheet.apidemo.ApiService.SetVerif;
import com.homecaretimesheet.apidemo.ApiService.TimeSheetReq;
import com.homecaretimesheet.apidemo.ApiService.TimeSheetResp;
import com.homecaretimesheet.apidemo.ApiService.UpdateSession;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionReq;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionResp;
import com.homecaretimesheet.apidemo.ApiService.VerifData;
import com.homecaretimesheet.apidemo.ApiService.VerifDataResp;
import com.homecaretimesheet.apidemo.ApiService.VerifResp;
import com.homecaretimesheet.apidemo.ApiService.sendVerifImage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

@SuppressLint("InflateParams")
public class SelectRecipientActivity extends AppCompatActivity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
	private Context context;
	private TimeSheetStorage store;
	private MenuItem menuNext;
	public Boolean recSet = false;
	public Button nextButton;
	public String preRecs = null;
	public ArrayList<Integer> preRecIds;
	public ApiService apiService;
	public GoogleApiClient mGoogleApiClient;
	private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
	public int STORAGE_REQUEST = globalHCTS.STORAGE_REQUEST;

	public PermissionService permService;
	private static final int IMAGE_REQUEST = 1337;
	public TypedFile setOutput = null;
	public File getOutput = null;

	private Location currentLocation;
	public Integer fileRowId = 0;
	public LocationRequest mLocationRequest;
	public Boolean needVerif = false;
	public String verifFile = "verifImg";
	public GetVerif getVerif;
	public SetVerif setVerif;
	public int vId = 0;
	public boolean hasPreRec = false;
	private boolean needsGpsService = true;
	private globalHCTS.ServiceChoice serviceChoice;
	private int LOC_REQUEST = 11011;
	private int UP_REQUEST = 11012;
	private int CONNECT_REQUEST = 11013;
	private int CAMERA_REQUEST = 11014;

	public globalHCTS globals;

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_rec);
		context = SelectRecipientActivity.this;
		store = new TimeSheetStorage(this);
		globals = (globalHCTS) getApplicationContext();
		if (globals.serviceChoice != null) {
			serviceChoice = globals.serviceChoice;
		}
		else {
			if (store.getDataInt(TSDatum.CURRENT_SERVICE_INT) > 0) {
				globals.serviceChoice = globalHCTS.ServiceChoice.fromInt(store.getDataInt(TSDatum.CURRENT_SERVICE_INT));
			}
			else {
				globals.serviceChoice = globalHCTS.ServiceChoice.fromInt(1);
			}
			serviceChoice = globals.serviceChoice;
		}
		setActiveTime();
		permService = new PermissionService();
		Spinner recipSpinner = (Spinner) findViewById(R.id.rec_spinner);
		String recs = store.getDataString(TSDatum.RECIPIENTS);
		preRecIds = new ArrayList<>();
		if (store.hasDataString(TSDatum.SELECTED_RECIPIENTS) && !store.getDataString(TSDatum.SELECTED_RECIPIENTS).equals("ERROR")) {
			preRecs = store.getDataString(TSDatum.SELECTED_RECIPIENTS);
			String[] strings = preRecs.split(",");
			hasPreRec = true;
            for (String string : strings) {
                preRecIds.add(Integer.valueOf(string));
            }
		}

		TypeToken<List<Recipient>> token = new TypeToken<List<Recipient>>() {
		};
		final List<Recipient> recipList = new Gson().fromJson(recs, token.getType());
		List<String> items = new ArrayList<>();
        for (Recipient element : recipList) {
            if (hasPreRec) {
				if (!preRecIds.contains(element.id)) {
					items.add(element.first_name.toUpperCase() + " " + element.last_name.toUpperCase());
				}
			}
			else {
				items.add(element.first_name.toUpperCase() + " " + element.last_name.toUpperCase());
			}
        }
		if (items.size() > 0) {
			ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
			recipSpinner.setAdapter(adapter);
			recipSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					recSet = true;

					String selection = (String) parent.getItemAtPosition(position);
					Integer selected;
                    for (Recipient element : recipList) {
                        String name = element.first_name.toUpperCase() + " " + element.last_name.toUpperCase();
                        if (selection.equals(name)) {
							setElement(element);
                            selected = element.id;
                            store.setDataInt(TSDatum.CURRENT_RECIPIENT, selected);
                            store.setDataString(TSDatum.CURRENT_RECIPIENT_NAME, name);
                        }
                    }
					setActiveTime();
					readySubmit();
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					recSet = false;
					store.setDataInt(TSDatum.CURRENT_RECIPIENT, recipList.get(0).id);
					String name = recipList.get(0).first_name.toUpperCase() + " " + recipList.get(0).last_name.toUpperCase();
					store.setDataString(TSDatum.CURRENT_RECIPIENT_NAME, name);

				}
			});
		}
		nextButton = (Button) findViewById(R.id.next);

		nextButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dataRowCreate();
				setActiveTime();
				nextButton.setEnabled(false);
			}
		});

		apiService = new ApiService(store.getDataString(TSDatum.API_TOKEN));
		dataSubmit();
        if (needsGpsService) {
			final LocationManager manager2 = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			if (!manager2.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				buildAlertMessageNoGps();
			}

			buildGoogleApiClient();
			mGoogleApiClient.connect();
		}
		fileRowId = store.getDataInt(TSDatum.SESSION_FILE_ROW_ID);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.major, menu);
		menuNext = menu.findItem(R.id.next_bar);
		return true;
	}

	public void setElement(Recipient element) {
		store.setDataBoolean(TSDatum.RECIPIENT_HAS_VERIF, !element.skip_verification);
		store.setDataBoolean(TSDatum.RECIPIENT_HAS_GPS, !element.skip_gps);
		globals.needsGPS = !element.skip_gps;
		globals.needsVerif = !element.skip_verification;
	}

	public void dataRowCreate() {
		if (store.getDataInt(TSDatum.CURRENT_TIME_SHEET) > 0) {
			setTimeSheet();
		} else {
			GetTimeSheet timeSheetGet = apiService.restAdapter.create(GetTimeSheet.class);
			GetTimeSheetReq timeSheetReq = apiService.new GetTimeSheetReq();
			timeSheetGet.getTimeSheetId(timeSheetReq, getTimeSheetId);
		}
	}

	Callback<GetTimeSheetResp> getTimeSheetId = new Callback<GetTimeSheetResp>() {
		@Override
		public void failure(RetrofitError code) {
			nextButton.setEnabled(true);
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(GetTimeSheetResp sheetResponse, Response response) {
			store.setDataInt(TSDatum.CURRENT_TIME_SHEET, sheetResponse.createdTimesheet.id);
			setTimeSheet();
		}

	};

	public void nextActivity() {
		setActiveTime();
		if (store.getDataBoolean(TSDatum.PUNCHED_OUT)) {
			if (serviceChoice.hasDL()) {
				store.setDataInt(TSDatum.PAGE_NUMBER, 5);
				startActivity(new Intent(context, SelectCareOptionsActivity.class));
			}
			else {
				store.setDataInt(TSDatum.PAGE_NUMBER, 6);
				startActivity(new Intent(context, PDFShowActivity.class));
			}

		} else {
			store.setDataInt(TSDatum.PAGE_NUMBER, 4);
			startActivity(new Intent(context, TimePunchActivity.class));
		}
	}

	public void readySubmit() {
		if (recSet) {
			if (menuNext != null) {
				updateAdvanceButton(menuNext, true);
			}
			nextButton.setEnabled(true);
			nextButton.setTextColor(ContextCompat.getColor(context, R.color.white));
		}
	}

	public void updateAdvanceButton(MenuItem button, Boolean enable) {
		button.setEnabled(enable);
	}

	public void setTimeSheet() {
		SetTimeSheet saveTimeSheet = apiService.restAdapter.create(SetTimeSheet.class);
		TimeSheetReq saveTimesheetReq = apiService.new TimeSheetReq();
		JsonObject timeSheetData = new JsonObject();
		Integer selected = store.getDataInt(TSDatum.CURRENT_RECIPIENT);
		timeSheetData.addProperty("recipient", selected);
		timeSheetData.addProperty("sessionData", store.getDataInt(TSDatum.SESSION_ID));
		saveTimesheetReq.homecare_homecarebundle_timesheet = timeSheetData;
		int timeSheetId = store.getDataInt(TSDatum.CURRENT_TIME_SHEET);
		saveTimeSheet.setTimeSheet(timeSheetId, saveTimesheetReq, saveTimesheetResp);
	}

	Callback<TimeSheetResp> saveTimesheetResp = new Callback<TimeSheetResp>() {
		@Override
		public void failure(RetrofitError code) {
			nextButton.setEnabled(true);
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(TimeSheetResp sheetResponse, Response response) {
			if (store.getDataBoolean(TSDatum.PUNCHED_OUT) && store.getDataBoolean(TSDatum.RECIPIENT_HAS_VERIF)) {
				getVerifData();
			} else {
				nextActivity();
			}
		}
	};

	public void getVerifData() {
		getVerif = apiService.restAdapter.create(GetVerif.class);
		int rec_id = store.getDataInt(TSDatum.CURRENT_RECIPIENT);
		int pca_id = store.getDataInt(TSDatum.PCA_ID);
		getVerif.checkVerif(pca_id, rec_id, checkVerif);

	}

	public void showDisclaim() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		LayoutInflater inflation = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View dialogLayout = inflation.inflate(R.layout.dialog_image_remind, null);

		alertDialogBuilder.setView(dialogLayout);

		alertDialogBuilder.setCancelable(false);
		final Button saveButton = (Button) dialogLayout.findViewById(R.id.done);

		final AlertDialog alertDialog = alertDialogBuilder.create();
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getImage(verifFile, IMAGE_REQUEST);
				alertDialog.dismiss();
			}

		});
		alertDialog.show();
	}


	Callback<VerifResp> checkVerif = new Callback<VerifResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(VerifResp vResp, Response response) {
			if (vResp.verification_needed) {
				needVerif = true;
				vId = vResp.id;
				sendVerif();
			} else {
				nextActivity();
			}
		}
	};

	public void sendVerif() {
		Calendar c = Calendar.getInstance();
		Date timeVerif = c.getTime();
		String outTime = serverDateFormat.format(timeVerif);
		JsonObject sendVerif = new JsonObject();
		if (currentLocation != null) {
			sendVerif.addProperty("address", globals.getAddress(context, currentLocation));
			sendVerif.addProperty("latitude", currentLocation.getLatitude());
			sendVerif.addProperty("longitude", currentLocation.getLongitude());
		}
		sendVerif.addProperty("time", outTime);
		VerifData sendData = apiService.new VerifData();
		sendData.homecare_homecarebundle_verification = sendVerif;
		setVerif = apiService.restAdapter.create(SetVerif.class);
		setVerif.setVerif(vId, sendData, sendVerifResp);
	}

	Callback<VerifDataResp> sendVerifResp = new Callback<VerifDataResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
			nextButton.setEnabled(true);
		}

		@Override
		public void success(VerifDataResp fResp, Response response) {
			showDisclaim();
		}
	};

	protected void sendVImage() {
		sendVerifImage sendupOut = apiService.restAdapter.create(sendVerifImage.class);
		sendupOut.sendUpOutImage(vId, setOutput, vImageResp);
	}

	Callback<FileResp> vImageResp = new Callback<FileResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
			nextButton.setEnabled(true);
		}

		@Override
		public void success(FileResp inFileResp, Response response) {
			Toast.makeText(context, "Image Saved", Toast.LENGTH_SHORT).show();
            setVerifTimesheet();
		}
	};

    public void setVerifTimesheet() {
        SetTimeSheet saveTimeSheet = apiService.restAdapter.create(SetTimeSheet.class);
        TimeSheetReq saveTimesheetReq = apiService.new TimeSheetReq();
        JsonObject timeSheetData = new JsonObject();
        timeSheetData.addProperty("verification", vId);
        saveTimesheetReq.homecare_homecarebundle_timesheet = timeSheetData;
        int timeSheetId = store.getDataInt(TSDatum.CURRENT_TIME_SHEET);
        saveTimeSheet.setTimeSheet(timeSheetId, saveTimesheetReq, saveVTimesheetResp);
    }

    Callback<TimeSheetResp> saveVTimesheetResp = new Callback<TimeSheetResp>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
            }
			nextButton.setEnabled(true);
        }


        @Override
        public void success(TimeSheetResp sheetResponse, Response response) {
                nextActivity();
        }

    };

	public void getImage(String fileName, Integer ReqCode) {

		if (permService.getStoragePermission(context) && permService.getCameraPermission(context)) {
			Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
			//noinspection ResultOfMethodCallIgnored
			dir.mkdirs();
			File output = new File(dir, fileName);
			if (output.exists()) {
				//noinspection ResultOfMethodCallIgnored
				output.delete();
			}
			getOutput = output;
			setOutput = new TypedFile("multipart/form-data", output);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));
			intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
			startActivityForResult(intent, ReqCode);
		}
		else if (permService.getStoragePermission(context)) {
			getCameraPermission();
		}
		else {
			getStoragePermission();
		}
	}

	public void getCameraPermission() {
		boolean show_explain = Build.VERSION.SDK_INT > 23 && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA);
		if (!show_explain) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);
		}
		else {
			AlertDialog alert = globals.buildAlertNoPermission(context, "Camera");
			alert.show();
		}
	}

	public void getStoragePermission() {
		boolean show_explain = Build.VERSION.SDK_INT > 23 && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
		if (!show_explain) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST);
		}
		else {
			AlertDialog alert = globals.buildAlertNoPermission(context, "External Storage");
			alert.show();
		}
	}
	public void dataSubmit() {
		UpdateSession updateSession = apiService.restAdapter.create(UpdateSession.class);
		UpdateSessionReq sendSession = apiService.new UpdateSessionReq();
		Integer pageNum = 3;
		JsonObject sessionData = new JsonObject();
		sessionData.addProperty("continueTimesheetNumber", pageNum);
		sessionData.addProperty("currentTimesheetNumber", store.getDataInt(TSDatum.CURRENT_TIME_SHEET_NUM));
		sendSession.homecare_homecarebundle_sessiondata = sessionData;
		updateSession.updateSession(store.getDataInt(TSDatum.SESSION_ID), sendSession, updateResponse);
	}

	Callback<UpdateSessionResp> updateResponse = new Callback<UpdateSessionResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
			nextButton.setEnabled(true);
		}

		@Override
		public void success(UpdateSessionResp sessionResponse, Response response) {
		}

	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.next_bar) {
			dataRowCreate();
			return true;
		}
		if (id == android.R.id.home) {
			onBackPressed();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		if (!hasPreRec) {
			Intent intent;
			if (serviceChoice.hasRatio()) {
				intent = new Intent(context, RatioLocationActivity.class);
			}
			else {
				intent = new Intent(context, SelectServiceActivity.class);
			}
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
			finish();
		}
	}

	public void returnToLogin() {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}

	public void setActiveTime() {
		Integer time = (int) (long) System.currentTimeMillis();
		store.setDataInt(TSDatum.LAST_ACTIVITY, time);
	}

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();
	}


	
	@Override
	public void onConnectionSuspended(int arg0) {
	}

	protected void onStart() {
		super.onStart();
		if (mGoogleApiClient != null) {
			mGoogleApiClient.connect();
		}
	}

	protected void onStop() {
		if (mGoogleApiClient != null) {
			mGoogleApiClient.disconnect();
		}
		super.onStop();
	}

	@Override
	public void onConnectionFailed(@Nullable ConnectionResult arg0) {
		Toast.makeText(context, "Location Service Failed", Toast.LENGTH_SHORT).show();
		mGoogleApiClient.connect();
	}


	@Override
	public void onRequestPermissionsResult(int requestCode,
										   @NonNull String permissions[], @NonNull int[] grantResults) {
		if (requestCode == CONNECT_REQUEST) {
			if (grantResults.length > 0	&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				mGoogleApiClient.connect();
			} else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == LOC_REQUEST) {
			if (grantResults.length > 0	&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				createLocationRequest();
			} else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == UP_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				startLocationUpdates();
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == STORAGE_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				getImage(verifFile, IMAGE_REQUEST);
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Storage");
				alert.show();
			}
		}
		else if (requestCode == CAMERA_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				getImage(verifFile, IMAGE_REQUEST);
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Camera");
				alert.show();
			}
		}
	}

	protected void createLocationRequest() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOC_REQUEST);
			return;
	    }
		int UPDATE_INTERVAL = 10000; // 10 sec
		int FATEST_INTERVAL = 5000; // 5 sec
		int DISPLACEMENT = 100; // 100 meters
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT); // 100 meters

	}

	protected void startLocationUpdates() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, UP_REQUEST);
			return;
		}
		LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
	}

	@Override
	public void onLocationChanged(Location location) {
		currentLocation = location;
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.ACCESS_FINE_LOCATION},	CONNECT_REQUEST);
			return;
		}
		createLocationRequest();
		startLocationUpdates();
		if (currentLocation == null) {
            LocationServices.FusedLocationApi.flushLocations(mGoogleApiClient);
            currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
	}

	@Override
	public void onRestart() {
		super.onRestart();
		Integer restartTime = (int) (long) System.currentTimeMillis() - 120000;
		Integer lastTime = store.getDataInt(TSDatum.LAST_ACTIVITY);
		if (lastTime < restartTime) {
			returnToLogin();
		}
		if (needsGpsService) {
			if (mGoogleApiClient.isConnected()) {
				startLocationUpdates();
			} else {
				buildGoogleApiClient();
				createLocationRequest();
			}
		}
		super.onRestart();
	}

	private void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
				.setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
						startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
					}
				});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == IMAGE_REQUEST) {
	        if (resultCode == RESULT_OK) {
				if (getOutput.exists()) {
					sendVImage();
				} else {
					getImage(verifFile, IMAGE_REQUEST);
				}
			}
	    }
    }
}
