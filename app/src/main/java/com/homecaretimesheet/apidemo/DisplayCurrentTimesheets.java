package com.homecaretimesheet.apidemo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DisplayCurrentTimesheets extends AppCompatActivity {
    public TimeSheetStorage store;
    public Context context;
    public int sdkVersion;
    private int delete;

    private GridView gridView;
    private User user;
    private ResponseHandler respHandler;
    private ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = DisplayCurrentTimesheets.this;
        store =  new TimeSheetStorage(this);
        setContentView(R.layout.activity_display_pdfs);
        sdkVersion = android.os.Build.VERSION.SDK_INT;
        globalHCTS globals = (globalHCTS) getApplicationContext();
        user = globals.user;
        respHandler = new ResponseHandler();
        delete = getIntent().getIntExtra("delete", 0);
        Button cancel = (Button) findViewById(R.id.cancel_button);
        apiService = new ApiService(store.getDataString(TSDatum.API_TOKEN));
        if (delete < 1) {
         //   cancel.setVisibility(View.GONE);
            TextView heading = (TextView) findViewById(R.id.headingDisplay);
            heading.setText(R.string.continue_time_sheet);
        }
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        gridView = (GridView) findViewById(R.id.grid_layout);
        buildGrid();
    }

    private void buildGrid() {
        ArrayList<SessionData> sessionArr = new ArrayList<>();
        for (SessionData session : user.sessions) {
            sessionArr.add(session);
        }
        CustomGridViewAdapterPCA adapter = new CustomGridViewAdapterPCA(context, sessionArr);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (delete > 0) {
                    showSureDialog(position);
                } else {
                    continueTimesheet(position);
                }

            }
        });
    }


    public void showSureDialog(final int sessionIndex) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setTitle("Are you sure?")
                .setMessage(R.string.session_del)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        setOverwrite(sessionIndex);
                    }
                })

                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void setOverwrite(int sessionIndex) {
        int sessionId = user.sessions.get(sessionIndex).id;
        overwriteTimesheet(sessionId);
        user.sessions.remove(sessionIndex);
    }
    public void overwriteTimesheet(int sessionId) {
        ApiService.DeleteTimeSheets deleteTS = apiService.restAdapter.create(ApiService.DeleteTimeSheets.class);
        ApiService.TimeSheetReq deleting = apiService.new TimeSheetReq();
        JsonObject deleteData = new JsonObject();
        deleteData.addProperty("sessionId", sessionId);
        deleting.homecare_homecarebundle_timesheet = deleteData;
        deleteTS.deleteTimeSheets(deleting, deleteResp);
    }

    Callback<ApiService.TimeSheetResp> deleteResp = new Callback<ApiService.TimeSheetResp>() {
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
            }
        }

        @Override
        public void success(ApiService.TimeSheetResp fileResp, Response response) {
            store.clearDatum(TSDatum.SESSION_ID);
            returnToContinue();
        }
    };

    public void overWriteAll() {
        if (user.sessions.size() > 0) {
            overwriteNextTimesheet();
        }
        else {
            returnToContinue();
        }
    }
    public void overwriteNextTimesheet() {
        int sessionId = user.sessions.get(0).id;
        user.sessions.remove(0);

        ApiService.DeleteTimeSheets deleteTS = apiService.restAdapter.create(ApiService.DeleteTimeSheets.class);
        ApiService.TimeSheetReq deleting = apiService.new TimeSheetReq();
        JsonObject deleteData = new JsonObject();
        deleteData.addProperty("sessionId", sessionId);
        deleting.homecare_homecarebundle_timesheet = deleteData;
        deleteTS.deleteTimeSheets(deleting, deleteAllResp);
    }

    Callback<ApiService.TimeSheetResp> deleteAllResp = new Callback<ApiService.TimeSheetResp>() {
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
            }
            else {
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
            }
        }

        @Override
        public void success(ApiService.TimeSheetResp fileResp, Response response) {
            store.clearDatum(TSDatum.SESSION_ID);
            overWriteAll();
        }
    };

    @Override
    protected void onRestart() {
        super.onRestart();
        Integer restartTime = (int) (long) System.currentTimeMillis() - 120000;
        Integer lastTime = store.getDataInt(TSDatum.LAST_ACTIVITY);
        if (lastTime < restartTime) {
            returnToLogin();
        }
    }

    public void continueTimesheet(int sessionId) {
        ResponseHandler responseHandler = new ResponseHandler();
        responseHandler.setCurrentSessionPCA(context, sessionId);
        continueSubmit();
    }

    void continueSubmit() {
        setActiveTime();
        if (store.getDataBoolean(TSDatum.CLIENT_SIGNED)) {
            startActivity(new Intent(context, PDFShowActivity.class));
            finish();
        }
        else {
            Integer pageNum = store.getDataInt(TSDatum.PAGE_NUMBER);

            Class<?> activityClass = SelectServiceActivity.class;
            switch(pageNum){
                case 1:	activityClass = SelectServiceActivity.class;
                    break;
                case 2:	activityClass = RatioLocationActivity.class;
                    break;
                case 3: activityClass = SelectRecipientActivity.class;
                    break;
                case 4:	activityClass = TimePunchActivity.class;
                    break;
                case 5: activityClass = SelectCareOptionsActivity.class;
                    break;
                case 6: activityClass = PDFShowActivity.class;
                    break;
            }
            Intent intent = new Intent(context, activityClass);
            startActivity(intent);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    public void returnToContinue() {
        if (user.sessions.size() > 0) {
            respHandler.setCurrentSessionPCA(context, 0);
        }
        Intent intent = new Intent(context, ContinueSelectActivity.class);
        startActivity(intent);
        finish();
    }


    public void setActiveTime() {
        Integer time = (int) (long) System.currentTimeMillis();
        store.setDataInt(TSDatum.LAST_ACTIVITY, time);
    }


    @Override
    public void onBackPressed() {
        returnToContinue();
    }
    public void returnToLogin() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

}
