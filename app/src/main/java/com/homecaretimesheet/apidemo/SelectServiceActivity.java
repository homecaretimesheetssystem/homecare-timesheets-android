package com.homecaretimesheet.apidemo;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.homecaretimesheet.apidemo.ApiService.CreateSession;
import com.homecaretimesheet.apidemo.ApiService.CreateSessionReq;
import com.homecaretimesheet.apidemo.ApiService.CreateSessionResp;
import com.homecaretimesheet.apidemo.ApiService.UpdateSession;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionReq;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionResp;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class SelectServiceActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
	private TimeSheetStorage store;
	private MenuItem menuNext = null;
	private Context context;
	private Button nextButton;
	private ApiService apiService;
	private CreateSession makeSession;
	private UpdateSession updateSession;

    private Boolean serviceSelected = false;
	private Boolean skipRatio2 = false;
	private globalHCTS globals;
	public boolean skipRatio = false;
	public LocationRequest mLocationRequest;
	public Boolean needVerif = false;
	public String verifFile = "verifImg";
	public ApiService.GetVerif getVerif;
	public ApiService.SetVerif setVerif;
	private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

	public int vId = 0;
	private static final int IMAGE_REQUEST = 1337;
	private int LOC_REQUEST = 11011;
	private int UP_REQUEST = 11012;
	private int CONNECT_REQUEST = 11013;
	private int CAMERA_REQUEST = 11014;
	public GoogleApiClient mGoogleApiClient;
	private Location currentLocation;
	public PermissionService permService;
	public TypedFile setOutput = null;
	public File getOutput = null;
	public int STORAGE_REQUEST = globalHCTS.STORAGE_REQUEST;
	public Recipient recip;
    public String services;
    
    protected void onCreate(Bundle SavedInstanceState) {
	   super.onCreate(SavedInstanceState);
		setContentView(R.layout.activity_select_service);
		context = SelectServiceActivity.this;
		store = new TimeSheetStorage(this);
		setActiveTime();
		apiService = new ApiService(store.getDataString(TSDatum.API_TOKEN));
		makeSession = apiService.restAdapter.create(CreateSession.class);
		updateSession = apiService.restAdapter.create(UpdateSession.class);
		globals = (globalHCTS) getApplicationContext();

		Spinner serviceContainer = (Spinner) findViewById(R.id.services);
		services = store.getDataString(TSDatum.SERVICES);
		List<String> items = new ArrayList<>();
		if (globals.isJSONValid(services)) {
			TypeToken<List<Service>> token = new TypeToken<List<Service>>() {
			};
			final List<Service> listServices = new Gson().fromJson(services, token.getType());
			for (Service element : listServices) {
				items.add(element.service_name.toUpperCase());
			}
		}
        if (items.size() > 0) {
			ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
			serviceContainer.setAdapter(adapter);
			serviceContainer.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					serviceSelected = true;
					String selection = (String) parent.getItemAtPosition(position);
					checkServiceChoice(selection);
					setActiveTime();
					readySubmit();
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
				
				}
			});	
        }
        	nextButton = (Button) findViewById(R.id.next);
		
		nextButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dataRowCreate();
				setActiveTime();
				nextButton.setEnabled(false);
			}
		});	
    }


    public void checkServiceChoice(String selection) {
		Integer selected;
		TypeToken<List<Service>> token = new TypeToken<List<Service>>() {
		};
		final List<Service> listServices = new Gson().fromJson(services, token.getType());
		for (Service element : listServices) {
			if (selection.equals(element.service_name.toUpperCase())) {
				selected = element.id;
				store.setDataInt(TSDatum.CURRENT_SERVICES, selected);
				int serviceInt = element.service_number;
				setServiceChoice(serviceInt);
				store.setDataString(TSDatum.CURRENT_SERVICES_TEXT, selection.toUpperCase());
			}
		}
	}

	public void setServiceChoice(int choice) {
		globals.serviceChoice = globalHCTS.ServiceChoice.fromInt(choice);
		store.setDataInt(TSDatum.CURRENT_SERVICE_INT, choice);
		skipRatio = !globals.serviceChoice.hasRatio();
		skipRatio2 = globals.user.recipients.size() == 1;
		if (skipRatio || skipRatio2) {
			store.setDataInt(TSDatum.CURRENT_TIME_SHEET_NUM, 1);
			String locations = store.getDataString(TSDatum.LOCATIONS);
			TypeToken<List<SharedCareLocation>> sToken = new TypeToken<List<SharedCareLocation>>(){};
			final List<SharedCareLocation> listLocations = new Gson().fromJson(locations, sToken.getType());
			String ratios = store.getDataString(TSDatum.RATIOS);
			TypeToken<List<Ratio>> token = new TypeToken<List<Ratio>>(){};
			final List<Ratio> listRatios = new Gson().fromJson(ratios, token.getType());
			for(Ratio rat : listRatios) {
				if (rat.ratio.equals("1:1")) {
					store.setDataInt(TSDatum.CURRENT_RATIO, rat.id);
				}
			}
			for(SharedCareLocation loc : listLocations) {
				if (loc.location.equals("N/A")) {
					store.setDataInt(TSDatum.CURRENT_CARE_LOCATION, loc.id );
					store.setDataString(TSDatum.CURRENT_LOCATION_TEXT, loc.location.toUpperCase());
				}
			}
			store.setDataInt(TSDatum.RATIO_INT, 1);
			if (skipRatio2) {
				String recs = store.getDataString(TSDatum.RECIPIENTS);
				TypeToken<List<Recipient>> token2 = new TypeToken<List<Recipient>>() {
				};
				final List<Recipient> recipList = new Gson().fromJson(recs, token2.getType());
				recip = recipList.get(0);
				String name = recip.first_name.toUpperCase() + " " + recip.last_name.toUpperCase();
				int selected = recip.id;
				store.setDataInt(TSDatum.CURRENT_RECIPIENT, selected);
				store.setDataString(TSDatum.CURRENT_RECIPIENT_NAME, name);
			}
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.major, menu);
		menuNext = menu.findItem(R.id.next_bar);
		return true;
	}

	
	public void readySubmit() {
		if (menuNext != null){
		  updateAdvanceButton(menuNext, serviceSelected);
		}
		nextButton.setEnabled(serviceSelected);
		nextButton.setTextColor(ContextCompat.getColor(context, R.color.white));
	}
	
	public void updateAdvanceButton(MenuItem button, Boolean enable) {
		button.setEnabled(enable);
	}
	
	public void dataRowCreate() {
		Integer sessionId = store.getDataInt(TSDatum.SESSION_ID);
		if (sessionId < 1) {
			CreateSessionReq getSession = apiService.new CreateSessionReq();
			makeSession.createSession(getSession, sessionResponse);
		}
		else {
			dataSubmit();
		}
		
	}

	public void dataSubmit() {
		UpdateSessionReq sendSession = apiService.new UpdateSessionReq();
		Integer pageNum = 2;
		JsonObject sessionData = new JsonObject();
		if (skipRatio || skipRatio2) {
			sessionData.addProperty("sharedCareLocation", store.getDataInt(TSDatum.CURRENT_CARE_LOCATION));
			pageNum = 3;
			if (skipRatio2) {
				sessionData.addProperty("ratio", store.getDataInt(TSDatum.CURRENT_RATIO));
				pageNum = 4;
			}
		}
		sessionData.addProperty("pca", store.getDataInt(TSDatum.PCA_ID));
		sessionData.addProperty("service", store.getDataInt(TSDatum.CURRENT_SERVICES));
		sessionData.addProperty("continueTimesheetNumber", pageNum);

		sendSession.homecare_homecarebundle_sessiondata = sessionData;
		updateSession.updateSession(store.getDataInt(TSDatum.SESSION_ID), sendSession, updateResponse);
	}
	
	
	Callback<CreateSessionResp> sessionResponse = new Callback<CreateSessionResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}	
			}
			nextButton.setEnabled(true);
		}

		@Override
		public void success(CreateSessionResp sessionResponse, Response response) {
	    store.setDataInt(TSDatum.SESSION_ID, sessionResponse.createdSessionData.id);
	    dataSubmit();
		}
		
	};

	Callback<UpdateSessionResp> updateResponse = new Callback<UpdateSessionResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}	
			}
			nextButton.setEnabled(true);
		}

		@Override
		public void success(UpdateSessionResp sessionResponse, Response response) {
			Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
			if (skipRatio2) {
				setElement(recip);
			}
			else {
				nextActivity();
			}
		}
	};

	@Override
	protected void onRestart(){
		super.onRestart();
		Integer restartTime = (int) (long) System.currentTimeMillis() - 120000;
		Integer lastTime = store.getDataInt(TSDatum.LAST_ACTIVITY);
		if (lastTime < restartTime) {
		returnToLogin();
		}
	}

	public void returnToLogin() {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}
	
	public void nextActivity() {
		setActiveTime();
		if (skipRatio2) {
            store.setDataInt(TSDatum.PAGE_NUMBER, 4);
			startActivity(new Intent(context, TimePunchActivity.class));
		}
		else if (skipRatio) {
			store.setDataInt(TSDatum.PAGE_NUMBER, 3);
			startActivity(new Intent(context, SelectRecipientActivity.class));
		}
		else {
			store.setDataInt(TSDatum.PAGE_NUMBER, 2);
			startActivity(new Intent(context, RatioLocationActivity.class));
		}
		finish();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.next_bar) {
			dataRowCreate();
			return true;
		}
		if (id == android.R.id.home) {
			onBackPressed();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(context, ContinueSelectActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}

	public void setActiveTime() {
		Integer time = (int) (long) System.currentTimeMillis();
		store.setDataInt(TSDatum.LAST_ACTIVITY, time);
	}

	private void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
				.setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
						startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
					}
				});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	public void setElement(Recipient element) {
		store.setDataBoolean(TSDatum.RECIPIENT_HAS_VERIF, !element.skip_verification);
		store.setDataBoolean(TSDatum.RECIPIENT_HAS_GPS, !element.skip_gps);
		globals.needsGPS = !element.skip_gps;
		globals.needsVerif = !element.skip_verification;
		if (!element.skip_gps) {
			final LocationManager manager2 = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			if (!manager2.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				buildAlertMessageNoGps();
			}
			buildGoogleApiClient();
			mGoogleApiClient.connect();
		}
		if (!element.skip_verification) {
			timeSheetRowCreate();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == IMAGE_REQUEST) {
			if (resultCode == RESULT_OK) {
				if (getOutput.exists()) {
					sendVImage();
				} else {
					getImage(verifFile, IMAGE_REQUEST);
				}
			}
		}
	}

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();
	}

	@Override
	public void onConnectionSuspended(int arg0) {
	}

	protected void onStop() {
		if (mGoogleApiClient != null) {
			mGoogleApiClient.disconnect();
		}
		super.onStop();
	}

	@Override
	public void onConnectionFailed(@Nullable ConnectionResult arg0) {
		Toast.makeText(context, "Location Service Failed", Toast.LENGTH_SHORT).show();
		mGoogleApiClient.connect();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   @NonNull String permissions[], @NonNull int[] grantResults) {
		if (requestCode == CONNECT_REQUEST) {
			if (grantResults.length > 0	&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				mGoogleApiClient.connect();
			} else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == LOC_REQUEST) {
			if (grantResults.length > 0	&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				createLocationRequest();
			} else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == UP_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				startLocationUpdates();
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == STORAGE_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				getImage(verifFile, IMAGE_REQUEST);
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Storage");
				alert.show();
			}
		}
		else if (requestCode == CAMERA_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				getImage(verifFile, IMAGE_REQUEST);
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Camera");
				alert.show();
			}
		}
	}

	protected void createLocationRequest() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOC_REQUEST);
			return;
		}
		int UPDATE_INTERVAL = 10000; // 10 sec
		int FATEST_INTERVAL = 5000; // 5 sec
		int DISPLACEMENT = 100; // 100 meters
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT); // 100 meters
	}

	protected void startLocationUpdates() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, UP_REQUEST);
			return;
		}
		LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
	}

	@Override
	public void onLocationChanged(Location location) {
		currentLocation = location;
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.ACCESS_FINE_LOCATION},	CONNECT_REQUEST);
			return;
		}
		createLocationRequest();
		startLocationUpdates();
		if (currentLocation == null) {
			LocationServices.FusedLocationApi.flushLocations(mGoogleApiClient);
			currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		}
	}

	public void getImage(String fileName, Integer ReqCode) {
		if (permService.getStoragePermission(context)) {
			Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
			//noinspection ResultOfMethodCallIgnored
			dir.mkdirs();
			File output = new File(dir, fileName);
			if (output.exists()) {
				//noinspection ResultOfMethodCallIgnored
				output.delete();
			}
			getOutput = output;
			setOutput = new TypedFile("multipart/form-data", output);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));
			intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
			startActivityForResult(intent, ReqCode);
		}
		else if (permService.getStoragePermission(context)) {
			getCameraPermission();
		}
		else {
			getStoragePermission();
		}
	}
	protected void sendVImage() {
		ApiService.sendVerifImage sendupOut = apiService.restAdapter.create(ApiService.sendVerifImage.class);
		sendupOut.sendUpOutImage(vId, setOutput, vImageResp);
	}

	Callback<ApiService.FileResp> vImageResp = new Callback<ApiService.FileResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(ApiService.FileResp inFileResp, Response response) {
			Toast.makeText(context, "Image Saved", Toast.LENGTH_SHORT).show();
			setVerifTimesheet();
		}
	};

    public void getCameraPermission() {
		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);
	}

	public void getStoragePermission() {
		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, STORAGE_REQUEST);
	}

	public void setVerifTimesheet() {
		ApiService.SetTimeSheet saveTimeSheet = apiService.restAdapter.create(ApiService.SetTimeSheet.class);
		ApiService.TimeSheetReq saveTimesheetReq = apiService.new TimeSheetReq();
		JsonObject timeSheetData = new JsonObject();
		timeSheetData.addProperty("verification", vId);
		saveTimesheetReq.homecare_homecarebundle_timesheet = timeSheetData;
		int timeSheetId = store.getDataInt(TSDatum.CURRENT_TIME_SHEET);
		saveTimeSheet.setTimeSheet(timeSheetId, saveTimesheetReq, saveVTimesheetResp);
	}

	Callback<ApiService.TimeSheetResp> saveVTimesheetResp = new Callback<ApiService.TimeSheetResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(ApiService.TimeSheetResp sheetResponse, Response response) {
			nextActivity();
		}
	};

	public void sendVerif() {
		Calendar c = Calendar.getInstance();
		Date timeVerif = c.getTime();
		String outTime = serverDateFormat.format(timeVerif);
		JsonObject sendVerif = new JsonObject();
		if (currentLocation != null) {
			sendVerif.addProperty("address", globals.getAddress(context, currentLocation));
			sendVerif.addProperty("latitude", currentLocation.getLatitude());
			sendVerif.addProperty("longitude", currentLocation.getLongitude());
		}
		sendVerif.addProperty("time", outTime);
		ApiService.VerifData sendData = apiService.new VerifData();
		sendData.homecare_homecarebundle_verification = sendVerif;
		setVerif = apiService.restAdapter.create(ApiService.SetVerif.class);
		setVerif.setVerif(vId, sendData, sendVerifResp);
	}

	Callback<ApiService.VerifDataResp> sendVerifResp = new Callback<ApiService.VerifDataResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(ApiService.VerifDataResp fResp, Response response) {
			showDisclaim();
		}
	};


	public void showDisclaim() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		LayoutInflater inflation = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View dialogLayout = inflation.inflate(R.layout.dialog_image_remind, null);

		alertDialogBuilder.setView(dialogLayout);

		alertDialogBuilder.setCancelable(false);
		final Button saveButton = (Button) dialogLayout.findViewById(R.id.done);

		final AlertDialog alertDialog = alertDialogBuilder.create();
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getImage(verifFile, IMAGE_REQUEST);
				alertDialog.dismiss();
			}

		});
		alertDialog.show();
	}

	public void getVerifData() {
		getVerif = apiService.restAdapter.create(ApiService.GetVerif.class);
		int rec_id = store.getDataInt(TSDatum.CURRENT_RECIPIENT);
		int pca_id = store.getDataInt(TSDatum.PCA_ID);
		getVerif.checkVerif(pca_id, rec_id, checkVerif);
	}

	Callback<ApiService.VerifResp> checkVerif = new Callback<ApiService.VerifResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(ApiService.VerifResp vResp, Response response) {
			if (vResp.verification_needed) {
				needVerif = true;
				vId = vResp.id;
				sendVerif();
			} else {
				nextActivity();
			}
		}
	};

	public void timeSheetRowCreate() {
		if (store.getDataInt(TSDatum.CURRENT_TIME_SHEET) > 0) {
			setTimeSheet();
		} else {
			ApiService.GetTimeSheet timeSheetGet = apiService.restAdapter.create(ApiService.GetTimeSheet.class);
			ApiService.GetTimeSheetReq timeSheetReq = apiService.new GetTimeSheetReq();
			timeSheetGet.getTimeSheetId(timeSheetReq, getTimeSheetId);
		}
	}

	Callback<ApiService.GetTimeSheetResp> getTimeSheetId = new Callback<ApiService.GetTimeSheetResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(ApiService.GetTimeSheetResp sheetResponse, Response response) {
			store.setDataInt(TSDatum.CURRENT_TIME_SHEET, sheetResponse.createdTimesheet.id);
			setTimeSheet();
		}

	};

	public void setTimeSheet() {
		ApiService.SetTimeSheet saveTimeSheet = apiService.restAdapter.create(ApiService.SetTimeSheet.class);
		ApiService.TimeSheetReq saveTimesheetReq = apiService.new TimeSheetReq();
		JsonObject timeSheetData = new JsonObject();
		Integer selected = store.getDataInt(TSDatum.CURRENT_RECIPIENT);
		timeSheetData.addProperty("recipient", selected);
		timeSheetData.addProperty("sessionData", store.getDataInt(TSDatum.SESSION_ID));
		saveTimesheetReq.homecare_homecarebundle_timesheet = timeSheetData;
		int timeSheetId = store.getDataInt(TSDatum.CURRENT_TIME_SHEET);
		saveTimeSheet.setTimeSheet(timeSheetId, saveTimesheetReq, saveTimesheetResp);
	}

	Callback<ApiService.TimeSheetResp> saveTimesheetResp = new Callback<ApiService.TimeSheetResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(ApiService.TimeSheetResp sheetResponse, Response response) {
			if (needVerif) {
				getVerifData();
			} else {
				nextActivity();
			}
		}
	};
}
