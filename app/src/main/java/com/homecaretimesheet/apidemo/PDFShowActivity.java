package com.homecaretimesheet.apidemo;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.homecaretimesheet.apidemo.ApiService.UpdateSession;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionReq;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionResp;

public class PDFShowActivity extends AppCompatActivity {
	public TimeSheetStorage store;
	public Context context;
    public int sdkVersion;

    private Date timeInSet = null;
    private Date timeOutSet = null;
    private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
	private SimpleDateFormat timePunchFormat = new SimpleDateFormat("hh:mm", Locale.US);
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
    private Button next;
    private ApiService apiService;
    private Bitmap sigC;
    private Bitmap sigP;
    private int sigCount = 0;
    private int sigGot = 0;
    private Bitmap bitmap;
    private Canvas canvas;
    private Paint paint;
    private ImageView overlaid;
	private Boolean pSigned = false;
	private Boolean cSigned = false;
	private ResponseHandler respHandler;
	private boolean needsGps;
	User user;
	globalHCTS.ServiceChoice serviceChoice;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    context = PDFShowActivity.this;
	    store =  new TimeSheetStorage(this);
		setContentView(R.layout.activity_pdf_show);
		sdkVersion = android.os.Build.VERSION.SDK_INT;
		next = (Button) findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				nextActivity();
			}
		});

		respHandler = new ResponseHandler();
		globalHCTS globals = (globalHCTS) getApplicationContext();
		user = globals.user;
		needsGps = globals.needsGPS;
		serviceChoice = globals.serviceChoice;
		final int sessIndex = store.getDataInt(TSDatum.SESSION_INDEX);
		Button saveBttn = (Button) findViewById(R.id.save);
		saveBttn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (user.userSet < 2) {
					startActivity(new Intent(context, ContinueSelectActivity.class));
					finish();
				}
				else {
					startActivity(new Intent(context, RepPDFActivity.class));
					finish();
				}
			}
		});
		if (user.userSet > 1) {
			saveBttn.setText(R.string.save);
			if (user.sessions.size() > 1) {
				FrameLayout rootView = (FrameLayout) findViewById(R.id.rootView);
				rootView.setOnTouchListener(new OnSwipeTouchListener(context) {
					@Override
					public void onSwipeLeft() {
						super.onSwipeLeft();
						int newIndex;
						if (sessIndex > 0) {
							newIndex = sessIndex - 1;
						} else {
							newIndex = user.sessions.size() - 1;
						}
						respHandler.setCurrentSessionRec(context, newIndex);
						startActivity(new Intent(context, PDFShowActivity.class));
						finish();
					}

					@Override
					public void onSwipeRight() {
						super.onSwipeRight();
						int maxIndex = user.sessions.size() - 1;
						int newIndex;
						if (sessIndex < maxIndex) {
							newIndex = sessIndex + 1;
						} else {
							newIndex = 0;
						}
						respHandler.setCurrentSessionRec(context, newIndex);
						startActivity(new Intent(context, PDFShowActivity.class));
						finish();
					}

				});
			}
		}
		setUnder();
		apiService = new ApiService(store.getDataString(TSDatum.API_TOKEN));
		setActiveTime();
		bitmap = Bitmap.createBitmap(600, 750, Config.ARGB_8888);
		bitmap.eraseColor(ContextCompat.getColor(context, R.color.clear));
		paint = new Paint();
		canvas = new Canvas(bitmap);
		setOverlay();

	    pSigned = store.getDataBoolean(TSDatum.PCA_SIGNED);
	    cSigned = store.getDataBoolean(TSDatum.CLIENT_SIGNED);

	    if (cSigned) {
			sigCount++;
	    	String rec_image = store.getDataString(TSDatum.AMAZON_URL) + store.getDataString(TSDatum.REC_SIG);
	    	rec_image = rec_image.replace("\"", "");
	    	new DownloadImageTaskR()
	    	.execute(rec_image);
	    }
	    if (pSigned) {
	    	sigCount++;
	    	String pca_image = store.getDataString(TSDatum.AMAZON_URL) + store.getDataString(TSDatum.PCA_SIG);
	    	pca_image = pca_image.replace("\"", "");
	    	new DownloadImageTaskP()
	    	.execute(pca_image);

	    }
    	if (cSigned && pSigned) {
			if (store.getDataInt(TSDatum.CURRENT_TIME_SHEET_NUM) < store.getDataInt(TSDatum.RATIO_INT) && user.userSet < 2) {
				next.setText(R.string.next_ts);
			}
			else {
				next.setText(R.string.send_ts);
			}
    	}
    	else if (pSigned || user.userSet > 1) {
    		next.setText(R.string.add_r_sign);
    	}
     }
	 	
	private void setUnder() {
		Bitmap under = BitmapFactory.decodeResource(context.getResources(), R.drawable.pdfimg);

		if (serviceChoice != null) {
            if (serviceChoice.equals(globalHCTS.ServiceChoice.PCA)) {
			   under = BitmapFactory.decodeResource(context.getResources(), R.drawable.pca);
		    }
		    else if (serviceChoice.equals(globalHCTS.ServiceChoice.HOMEMAKING)) {
			   under = BitmapFactory.decodeResource(context.getResources(), R.drawable.homemaking);
		    }
			else if (serviceChoice.equals(globalHCTS.ServiceChoice.RESPITE)) {
				under = BitmapFactory.decodeResource(context.getResources(), R.drawable.respite);
			}
			else if (serviceChoice.equals(globalHCTS.ServiceChoice.ENVIRONMENTAL)) {
				under = BitmapFactory.decodeResource(context.getResources(), R.drawable.environmentalmodifications);
			}
			else if (serviceChoice.equals(globalHCTS.ServiceChoice.CSUPPORT)) {
				under = BitmapFactory.decodeResource(context.getResources(), R.drawable.consumersupport);
			}
			else if (serviceChoice.equals(globalHCTS.ServiceChoice.CAREGIVING)) {
				under = BitmapFactory.decodeResource(context.getResources(), R.drawable.caregivingexpense);
			}
			else if (serviceChoice.equals(globalHCTS.ServiceChoice.TREATMENT)) {
				under = BitmapFactory.decodeResource(context.getResources(), R.drawable.treatmentandtraining);
			}
			else if (serviceChoice.equals(globalHCTS.ServiceChoice.SDSUPPORT)) {
				under = BitmapFactory.decodeResource(context.getResources(), R.drawable.selfdirectionsupport);
			}
			else if (serviceChoice.equals(globalHCTS.ServiceChoice.PSUPPORT)) {
				under = BitmapFactory.decodeResource(context.getResources(), R.drawable.personalsupport);
			}
			else if (serviceChoice.equals(globalHCTS.ServiceChoice.ASSISTANCE)) {
				under = BitmapFactory.decodeResource(context.getResources(), R.drawable.personalassistance);
			}
		}
        ImageView underView = (ImageView) findViewById(R.id.under);
		underView.setImageBitmap(Bitmap.createScaledBitmap(under, 600, 775, true));
	}

	private void setOverlay() {
	    paint.setColor(ContextCompat.getColor(context, R.color.black2));
	    paint.setStyle(Style.FILL);

		paint.setAntiAlias(true);
	    String check = Html.fromHtml("&#x2713;").toString();
	    paint.setTypeface(Typeface.SANS_SERIF);
	    paint.setTextSize(18);

		/*String curService = store.getDataString(TSDatum.CURRENT_SERVICES_TEXT);
	    if (curService.equals("PERSONAL CARE SERVICE")) {
	        canvas.drawText(check, 182, 92, paint);
		}
		else {
		   	canvas.drawText(check, 323, 92, paint);
		}
		  */
		paint.setTextSize(16);
		String agencyName = store.getDataString(TSDatum.AGENCY_NAME);
		String agencyPhone = store.getDataString(TSDatum.AGENCY_PHONE);
		canvas.drawText(agencyName, 120f, 122f, paint);
		canvas.drawText(agencyPhone, 432f, 122f, paint);
		paint.setTypeface(Typeface.DEFAULT_BOLD);
		if (serviceChoice.hasDL()) {
			String allCareOpts = store.getDataString(TSDatum.CARE_OPTIONS);
			TypeToken<ArrayList<CareOption>> token = new TypeToken<ArrayList<CareOption>>() {
			};
			ArrayList<CareOption> listCareOpts = new Gson().fromJson(allCareOpts, token.getType());
			String selCareOpts = store.getDataString(TSDatum.CURRENT_CARE_OPTIONS);
			String[] selCareOptArray = selCareOpts.replaceAll("\\[|\\]| ", "").split(",");
			ArrayList<Integer> selOpts = new ArrayList<>();
			for (String aSelCareOptArray : selCareOptArray) {
				selOpts.add(Integer.valueOf(aSelCareOptArray));
			}
			int i = 0;

			for (CareOption ele : listCareOpts) {
				int y;
				int x;
				if (selOpts.contains(ele.id)) {
					if (!ele.is_iadl) {
						y = (int) (211 + (28.2 * i));
						x = 264;
					} else {
						y = (int) (211 + (28.2 * (i - 10)));
						x = 498;
					}
					canvas.drawText(check, x, y, paint);
				}
				i++;
			}
		}
        String timeIn = store.getDataString(TSDatum.TIME_IN);
		String timeOut = store.getDataString(TSDatum.TIME_OUT);
		try {
		    timeInSet = serverDateFormat.parse(timeIn);
        } catch (ParseException e1) {
		    e1.printStackTrace();
        }
		try {
            timeOutSet = serverDateFormat.parse(timeOut);
        } catch (ParseException e1) {
			e1.printStackTrace();
		}
		String dateEnding = dateFormat.format(timeOutSet);

		canvas.drawText(dateEnding, 208, 160, paint);
		Calendar c = Calendar.getInstance();
		c.setTime(timeInSet);
		int amPmIn = c.get(Calendar.AM_PM);
		int topR;
		int leftR;
		int bottomR;
		int rightR;
		if (amPmIn == Calendar.AM) {
		   	topR = 383;
		   	leftR = (int) 501.2;
		   	bottomR = 393;
		   	rightR = (int) 523.2;
		}
		else {
		   	topR = 393;
		   	leftR = 502;
		   	bottomR = 403;
		   	rightR = 522;
		}
		String timeInPunch = timePunchFormat.format(timeInSet);
		String timeOutPunch = timePunchFormat.format(timeOutSet);
		canvas.drawText(timeInPunch, 452,(int) 402.2, paint);
		canvas.drawText(timeOutPunch, 452, 430, paint);

	    RectF oval = new RectF(leftR, topR, rightR, bottomR);
		paint.setStyle(Style.STROKE);

		canvas.drawOval(oval, paint);
		c.setTime(timeOutSet);
		int amPmOut = c.get(Calendar.AM_PM);
		int topR2;
		int leftR2;
		int bottomR2;
		int rightR2;
		if (amPmOut == Calendar.AM) {
		    topR2 = (int) 409.4;
		    leftR2 = (int) 501.2;
		    bottomR2 = (int) 419.4;
		    rightR2 = (int) 523.2;
		}
		else {
		   	topR2 = (int) 419.4;
		   	leftR2 = 502;
		   	bottomR2 = (int) 429.4;
		   	rightR2 = 522;
		}
		    	
		RectF oval2 = new RectF(leftR2, topR2, rightR2, bottomR2);
		canvas.drawOval(oval2, paint);
		if (serviceChoice.hasRatio()) {
			int ratioInt = store.getDataInt(TSDatum.RATIO_INT);
			if (ratioInt > 0) {
				int topR3 = 315;
				int bottomR3 = 343;
				int leftR3;
				int rightR3;
				if (ratioInt == 3) {
					leftR3 = 500;
					rightR3 = 532;
				} else if (ratioInt == 2) {
					leftR3 = 465;
					rightR3 = 497;
				} else {
					leftR3 = (int) 430.5;
					rightR3 = (int) 462.5;
				}
				RectF oval3 = new RectF(leftR3, topR3, rightR3, bottomR3);
				canvas.drawOval(oval3, paint);
			}

			paint.setTypeface(Typeface.DEFAULT);
			String location = store.getDataString(TSDatum.CURRENT_LOCATION_TEXT);
			int xLoc = 447;
			int yLoc = 375;
			paint.setTextSize(24);
			paint.setStyle(Style.FILL);
			if (location.equals("HOME")) {
				paint.setTextSize(24);
				xLoc = 436;
				yLoc = 375;
			} else if (location.equals("COMMUNITY")) {
				paint.setTextSize(17);
				xLoc = (int) 422.5;
				yLoc = 381;
			}

			canvas.drawText(location, xLoc, yLoc, paint);
		}
		paint.setStyle(Style.FILL);
		String shiftString = getFullShift();
	    paint.setTextSize(20);
	    canvas.drawText(shiftString, 458, (int) 465.5, paint);
	    paint.setTextSize(16);
		if (needsGps) {
			String timeInAddress = store.getDataString(TSDatum.TIME_IN_ADD);
			String timeOutAddress = store.getDataString(TSDatum.TIME_OUT_ADD);
			if (timeInAddress != null && !timeInAddress.equals("ERROR")) {
				canvas.drawText(timeInAddress, 135, (int) 544.5, paint);
			}
			if (timeOutAddress != null && !timeOutAddress.equals("ERROR")) {
				canvas.drawText(timeOutAddress, 149, (int) 578.5, paint);
			}
		}
	    String allRecs = store.getDataString(TSDatum.RECIPIENTS);
	    TypeToken<ArrayList<Recipient>> token3 = new TypeToken<ArrayList<Recipient>>(){};
	    ArrayList<Recipient> listRecs = new Gson().fromJson(allRecs, token3.getType());
	    int currentRec = store.getDataInt(TSDatum.CURRENT_RECIPIENT);
        for (Recipient ele : listRecs) {
            if (ele.id == currentRec) {
                String name = ele.first_name.toUpperCase() + " " + ele.last_name.toUpperCase();
                canvas.drawText(name, 70, 665, paint);
			    String maNum = fixText(ele.ma_number, 90);
                canvas.drawText(maNum, 230, 665, paint);
			}
		}
		String pcaFirst = store.getDataString(TSDatum.PCA_FIRST);
	    String pcaLast = store.getDataString(TSDatum.PCA_LAST);
		String pcaUmpi = store.getDataString(TSDatum.PCA_UMPI);
		String pca_name =  pcaFirst + " " + pcaLast;
		pca_name = fixText(pca_name, 150);
		pcaUmpi = fixText(pcaUmpi, 90);
		canvas.drawText(pca_name, 70, 693, paint);
		canvas.drawText(pcaUmpi, 230, 693, paint);
		   
		canvas.save();
		overlaid = (ImageView) findViewById(R.id.overlay);
		overlaid.setImageBitmap(bitmap);
	}
	 
	@Override
	protected void onStart() {
	 	super.onStart();
		dataSubmit();
	}

	public void nextActivity() {
	   	setActiveTime();
	   	if (store.getDataBoolean(TSDatum.CLIENT_SIGNED) && store.getDataBoolean(TSDatum.PCA_SIGNED)) {
	   		Intent intent = new Intent(this, PDFActivity.class);
	   		startActivity(intent);
	   		finish();
	   	}
	   	else {
	   		if (store.getDataInt(TSDatum.USER_TYPE) > 1 || store.getDataBoolean(TSDatum.PCA_SIGNED)) {
	   			startActivity(new Intent(context, GetClientSig.class));
				finish();
	   		}
	   		else {
				showSureDialog();
	   		}
	   	}
	}


	public void showSureDialog() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		alertDialogBuilder.setTitle("Proceed")
				.setMessage(R.string.check_timesheet)
				.setCancelable(false)
				.setPositiveButton(R.string.cont, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						startActivity(new Intent(context, GetPcaSig.class));
						finish();
					}
				})

				.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
	public void setActiveTime() {
		Integer time = (int) (long) System.currentTimeMillis();
		store.setDataInt(TSDatum.LAST_ACTIVITY, time);
	}

	public void returnToLogin() {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}
	    
	@Override
	protected void onRestart(){
		super.onRestart();
		Integer restartTime = (int) (long) System.currentTimeMillis() - 120000;
		Integer lastTime = store.getDataInt(TSDatum.LAST_ACTIVITY);
		if (lastTime < restartTime) {
			returnToLogin();
		}
	}
		
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.next_bar) {
			nextActivity();
			return true;
		}
		if (id == android.R.id.home) {
			if (user.userSet > 1) {
				Intent intent = new Intent(context, RepPDFActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				finish();
			}
			else if (!pSigned) {
				Intent intent = new Intent(context, SelectCareOptionsActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				finish();
			}
		}
		return super.onOptionsItemSelected(item);
	}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.send, menu);
		MenuItem menuNext = menu.findItem(R.id.next_bar);
		if (menuNext != null && pSigned && cSigned) {
			menuNext.setEnabled(true);
		}
		return true;
	}

	public void dataSubmit() {
		UpdateSession updateSession = apiService.restAdapter.create(UpdateSession.class);
		UpdateSessionReq sendSession = apiService.new UpdateSessionReq();
		Integer pageNum = 6;
		JsonObject sessionData = new JsonObject();
		sessionData.addProperty("continueTimesheetNumber", pageNum);
		sendSession.homecare_homecarebundle_sessiondata = sessionData;
		updateSession.updateSession(store.getDataInt(TSDatum.SESSION_ID), sendSession, updateResponse);
	}
		
	Callback<UpdateSessionResp> updateResponse = new Callback<UpdateSessionResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
			}
			else {
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(UpdateSessionResp sessionResponse, Response response) {
			next.setEnabled(true);
		}
	};
		
	public void setSigP() {
		paint.setColor(ContextCompat.getColor(context, R.color.black2));
		paint.setStyle(Style.FILL);
		Rect destRect = new Rect(324, 679, 474, 700);
		Rect srcRect = new Rect(0, 0, sigP.getWidth(), sigP.getHeight());
		canvas.drawBitmap(sigP, srcRect, destRect, paint);
	    String pTime = store.getDataString(TSDatum.PCA_SIG_TIME);
	    Date pcaSigCal = null;
	    try {
	    	pcaSigCal = serverDateFormat.parse(pTime);
	    } catch (ParseException e3) {
	    	e3.printStackTrace();
	    }
	    if (pcaSigCal != null) {
	    	String pcaSigTime = dateFormat.format(pcaSigCal);
	    	paint.setTextSize(12);
	    	canvas.drawText(pcaSigTime, 476, 693, paint);
	    }
		setSigs();
	}
		
	public void setSigC() {
	    paint.setColor(ContextCompat.getColor(context, R.color.black2));
	    paint.setStyle(Style.FILL);
	    Rect destRect = new Rect(324, 652, 474, 673);
		Rect srcRect = new Rect(0, 0, sigC.getWidth(), sigC.getHeight());
	    canvas.drawBitmap(sigC, srcRect, destRect, paint);
	    String cTime = store.getDataString(TSDatum.CLIENT_SIG_TIME);
	    Date clientSigCal = null;
	    try {
	    	clientSigCal = serverDateFormat.parse(cTime);
	    } catch (ParseException e3) {
	    	e3.printStackTrace();
	    }
	    if (clientSigCal != null) {
	    	String clientSigTime = dateFormat.format(clientSigCal);
	    	paint.setTextSize(12);
	    	canvas.drawText(clientSigTime, 476, 665, paint);
	    }
	    setSigs();
	}
		   
	public void setSigs() {
		sigGot++;
		if (sigGot == sigCount) {
			canvas.save();
		    overlaid.setImageBitmap(bitmap);
		}
	}
		
	private class DownloadImageTaskP extends AsyncTask<String, Bitmap, Bitmap> {
		public DownloadImageTaskP() {  }

		protected Bitmap doInBackground(String... urls) {
		    String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
			    InputStream in = new java.net.URL(urldisplay).openStream();
			    mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
			    e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
		    sigP = result;
			setSigP();
		}
	}
	   
	private class DownloadImageTaskR extends AsyncTask<String, Bitmap, Bitmap> {
		public DownloadImageTaskR() {	  }

		protected Bitmap doInBackground(String... urls) {
		    String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
			    InputStream in = new java.net.URL(urldisplay).openStream();
			    mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
			    e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
		    sigC = result;
			setSigC();
		}
	}

	public String fixText(String text, float width) {
		char[] chars = text.toCharArray();
		paint.setSubpixelText(true);
		int nextPos = paint.breakText(chars, 0, chars.length, width, null);
		text = text.substring(0, nextPos);
		return text;
	}

	public String getFullShift() {
		long shiftLength = timeOutSet.getTime() - timeInSet.getTime();
		Integer fullShift = ((int) (long) shiftLength)/1000;
		Integer minutes = (fullShift / 60 % 60) - (fullShift / 60 % 15);
		Integer hours = fullShift / (60 * 60);
		String hour = hours.toString();
		String minute;
		if (minutes > 10) {
			minute = minutes.toString();
		}
		else if (minutes == 0) {
			minute = "00";
		}
		else {
			minute = "0" + minutes.toString();
		}
		return (hour + ":" + minute);
	}
	@Override
	public void onBackPressed() {
       if (user.userSet > 1) {
		   startActivity(new Intent(context, RepPDFActivity.class));
		   finish();
	   }
	   else {
		   Intent intent;
		   if (!pSigned && serviceChoice.hasDL()) {
			   intent = new Intent(context, SelectCareOptionsActivity.class);

		   }
		   else {
			   intent = new Intent(context, TimePunchActivity.class);
		   }
		   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		   startActivity(intent);
		   finish();
	   }
	}
}
