package com.homecaretimesheet.apidemo;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.SpinnerAdapter;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.homecaretimesheet.apidemo.ApiService.UpdateSession;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionReq;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionResp;

public class RatioLocationActivity extends AppCompatActivity {
	private TimeSheetStorage store;
	private MenuItem menuNext = null;
	private Context context;
	private ApiService apiService;
	private UpdateSession updateSession;
	private User user;
	private globalHCTS globals;
	
	private Spinner locationContainer;
	private Boolean ratioSelected = false;
	private Boolean locationSelected = false;
	private Boolean skipRecipient = false;
	private Button nextButton;
	protected SpinnerAdapter loc_adapter;
	protected SpinnerAdapter na_adapter;
	private int ratioInt = 1;

	protected void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		setContentView(R.layout.activity_ratio_location);
		context = RatioLocationActivity.this;
		store = new TimeSheetStorage(this);
		globals = (globalHCTS) getApplicationContext();
		user = globals.user;
		skipRecipient = store.getDataInt(TSDatum.RECIPIENT_COUNT) == 1;
		setActiveTime();
  	    apiService = new ApiService(store.getDataString(TSDatum.API_TOKEN));
		updateSession = apiService.restAdapter.create(UpdateSession.class);
		String locations = store.getDataString(TSDatum.LOCATIONS);
		TypeToken<List<SharedCareLocation>> sToken = new TypeToken<List<SharedCareLocation>>(){};
		final List<SharedCareLocation> listLocations = new Gson().fromJson(locations, sToken.getType());
		List<String> loc_items = new ArrayList<>();
        for (SharedCareLocation element : listLocations) {
            loc_items.add(element.location.toUpperCase());
        }
		String[] na_items = new String[]{"N/A"};
		na_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, na_items);
		loc_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, loc_items);

		Spinner ratioContainer = (Spinner) findViewById(R.id.ratios);
		String ratios = store.getDataString(TSDatum.RATIOS);
		Integer ratioCount = store.getDataInt(TSDatum.RECIPIENT_COUNT);

		TypeToken<List<Ratio>> token = new TypeToken<List<Ratio>>(){};
	    final List<Ratio> listRatios = new Gson().fromJson(ratios, token.getType());
	    List<String> items = new ArrayList<>();
        for (Ratio element : listRatios) {
		    String[] splits = element.ratio.split(":");
			int ratC = Integer.valueOf(splits[1]);
			if (ratC <= ratioCount) {
			    items.add(element.ratio);
			}
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
		ratioContainer.setAdapter(adapter);
		ratioContainer.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				ratioSelected = true;
				String selection = (String) parent.getItemAtPosition(position);
				String selected;
				Integer selectedId;
                String string1 = "1:1";
                String string2 = "1:2";
                String string3 = "1:3";
				store.setDataInt(TSDatum.CURRENT_TIME_SHEET_NUM, 1);
                for (Ratio element : listRatios) {
                    if (element.ratio.equals(selection)) {
                        selected = element.ratio;
						selectedId = element.id;
                        if (selected.equals(string1) ) {
							ratioInt = 1;
                            locationContainer.setAdapter(na_adapter);
                            store.setDataInt(TSDatum.CURRENT_CARE_LOCATION, 1);
                            locationSelected = true;
						} else if (selected.equals(string2)) {
							ratioInt = 2;
							locationContainer.setAdapter(loc_adapter);
                        } else if (selected.equals(string3)) {
							ratioInt = 3;
							locationContainer.setAdapter(loc_adapter);
                        }
                        store.setDataInt(TSDatum.CURRENT_RATIO, selectedId);
                        readySubmit();
					}
				}
				setActiveTime();
			}
				
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});
			
		locationContainer = (Spinner) findViewById(R.id.shared_care_locs);
		locationContainer.setAdapter(na_adapter);
		locationContainer.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				locationSelected = true;
				String selection = (String) parent.getItemAtPosition(position);
				Integer selected;
                for (SharedCareLocation element : listLocations) {
                    if (element.location.equals(selection.toUpperCase())) {
                        selected = element.id;
                        store.setDataInt(TSDatum.CURRENT_CARE_LOCATION, selected);
						store.setDataString(TSDatum.CURRENT_LOCATION_TEXT, selection.toUpperCase());
                        readySubmit();
					}
				}
				setActiveTime();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});
			
		nextButton = (Button) findViewById(R.id.next);
		nextButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dataSubmit();
				setActiveTime();
				nextButton.setEnabled(false);
			}
		});
	}
	   
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.major, menu);
		menuNext = menu.findItem(R.id.next_bar);
		return true;
	}

	public void readySubmit() {
		if (ratioSelected && locationSelected) {
			if (menuNext != null){
				updateAdvanceButton(menuNext, true);
			}
			nextButton.setEnabled(true);
			nextButton.setTextColor(ContextCompat.getColor(context, R.color.white));
		}
	}
		
	public void updateAdvanceButton(MenuItem button, Boolean enable) {
		button.setEnabled(enable);
	}
		
	public void nextActivity() {
		setActiveTime();
		if (skipRecipient) {
			store.setDataInt(TSDatum.PAGE_NUMBER, 4);
			startActivity(new Intent(context, TimePunchActivity.class));
		}
		else {
			store.setDataInt(TSDatum.PAGE_NUMBER, 3);
			startActivity(new Intent(context, SelectRecipientActivity.class));
		}
	}
		
	public void dataSubmit() {
		UpdateSessionReq sendSession = apiService.new UpdateSessionReq();
		Integer pageNum = 3;
		store.setDataInt(TSDatum.RATIO_INT, ratioInt);
		JsonObject sessionData = new JsonObject();
		if (skipRecipient) {
			Recipient element = user.recipients.get(0);
			sessionData.addProperty("recipient", element.id);
			setElement(element);
		}
		sessionData.addProperty("ratio", store.getDataInt(TSDatum.CURRENT_RATIO));
		sessionData.addProperty("sharedCareLocation", store.getDataInt(TSDatum.CURRENT_CARE_LOCATION));
		sessionData.addProperty("continueTimesheetNumber", pageNum);
		sendSession.homecare_homecarebundle_sessiondata = sessionData;
		updateSession.updateSession(store.getDataInt(TSDatum.SESSION_ID), sendSession, updateResponse);
	}
		
	Callback<UpdateSessionResp> updateResponse = new Callback<UpdateSessionResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
			nextButton.setEnabled(true);
		}

		@Override
		public void success(UpdateSessionResp sessionResponse, Response response) {
			Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
			nextActivity();
		}
	};

	public void setElement(Recipient element) {
		store.setDataInt(TSDatum.CURRENT_RECIPIENT, element.id);
		String name = element.first_name.toUpperCase() + " " + element.last_name.toUpperCase();
		store.setDataString(TSDatum.CURRENT_RECIPIENT_NAME, name);
		store.setDataBoolean(TSDatum.RECIPIENT_HAS_VERIF, !element.skip_verification);
		store.setDataBoolean(TSDatum.RECIPIENT_HAS_GPS, !element.skip_gps);
		globals.needsGPS = !element.skip_gps;
		globals.needsVerif = !element.skip_verification;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.next_bar) {
			dataSubmit();
			return true;
		}
		if (id == android.R.id.home) {
			onBackPressed();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(context, SelectServiceActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}


	@Override
	protected void onRestart(){
		super.onRestart();
		Integer restartTime = (int) (long) System.currentTimeMillis() - 120000;
		Integer lastTime = store.getDataInt(TSDatum.LAST_ACTIVITY);
		if (lastTime < restartTime) {
			returnToLogin();
		}
	}
		
	public void returnToLogin() {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}
		
	public void setActiveTime() {
		Integer time = (int) (long) System.currentTimeMillis();
		store.setDataInt(TSDatum.LAST_ACTIVITY, time);
	}
}