package com.homecaretimesheet.apidemo;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class ResponseHandler {

    public boolean getResp(JsonObject loginResponse, Context context) {
        int userTypeInt;
        int timeSheetNum = 0;
        TimeSheetStorage store = new TimeSheetStorage(context);
        store.setDataBoolean(TSDatum.LOGGED_IN, true);
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        UserData userData = gson.fromJson(loginResponse.get("user"), UserData.class);
        final globalHCTS globalUser = (globalHCTS) context.getApplicationContext();
        User user = globalUser.user;
        user.setUser(userData);
        userTypeInt = user.userSet;
        if (userTypeInt < 2) {
            LoginPcaResp2 data = gson.fromJson(loginResponse, LoginPcaResp2.class);

            Agency agency = data.agency;
            user.setAgency(agency);
            store.setDataInt(TSDatum.AGENCY_ID, agency.id);
            store.setDataString(TSDatum.AGENCY_NAME, agency.agency_name.toUpperCase());
            store.setDataString(TSDatum.AGENCY_PHONE, agency.phone_number);
            store.setDataString(TSDatum.AGENCY_STATE, agency.state.state_name.toUpperCase());
            if (agency.image_name != null) {
                store.setDataString(TSDatum.AGENCY_ICON, agency.image_name);
            }
            user.setAmazon(data.amazonUrl);
            store.setDataString(TSDatum.AMAZON_URL, data.amazonUrl);
            if (data.sessionData != null) {
                List<SessionData> sessions = data.sessionData;
                user.setSessions(sessions);
            } else {
                user.sessions = new ArrayList<>();
            }


            if (data.careOptions != null) {
                JsonArray careOptions = gson.toJsonTree(data.careOptions).getAsJsonArray();
                store.setDataString(TSDatum.CARE_OPTIONS, careOptions.toString());
                user.setCareOpts(data.careOptions);
            }
            if (data.recipients != null) {
                JsonArray recipients = gson.toJsonTree(data.recipients).getAsJsonArray();
                user.setRecips(data.recipients);
                store.setDataInt(TSDatum.RECIPIENT_COUNT, recipients.size());
                store.setDataString(TSDatum.RECIPIENTS, recipients.toString());
            }
            if (data.ratios != null) {
                user.setRatios(data.ratios);
                JsonArray ratios = gson.toJsonTree(data.ratios).getAsJsonArray();
                store.setDataString(TSDatum.RATIOS, ratios.toString());
            }
            if (data.sharedCareLocations != null) {
                JsonArray locations = gson.toJsonTree(data.sharedCareLocations).getAsJsonArray();
                store.setDataString(TSDatum.LOCATIONS, locations.toString());
                user.setCareLocations(data.sharedCareLocations);
            }
            PCA pca = data.pca;
            JsonArray services;
            if (pca.services.size() > 0) {
                user.setServices(pca.services);
                services = gson.toJsonTree(pca.services).getAsJsonArray();
            }
            else {
                user.setServices(agency.services);
                services = gson.toJsonTree(agency.services).getAsJsonArray();
            }
            store.setDataString(TSDatum.SERVICES, services.toString());
            store.setDataString(TSDatum.PCA_FIRST, pca.first_name.toUpperCase());
            store.setDataString(TSDatum.PCA_LAST, pca.last_name.toUpperCase());
            store.setDataString(TSDatum.PCA_UMPI, pca.umpi);
            store.setDataInt(TSDatum.PCA_ID, pca.id);
            user.setPCA(pca);

        } else {
            LoginRecResp2 data = gson.fromJson(loginResponse, LoginRecResp2.class);
            user.setAgency(data.agency);
            user.setAmazon(data.amazonUrl);
            store.setDataString(TSDatum.AMAZON_URL, data.amazonUrl);
            Agency agency = data.agency;
            store.setDataInt(TSDatum.AGENCY_ID, agency.id);
            store.setDataString(TSDatum.AGENCY_NAME, agency.agency_name.toUpperCase());
            store.setDataString(TSDatum.AGENCY_PHONE, agency.phone_number);
            store.setDataString(TSDatum.AGENCY_STATE, agency.state.state_name.toUpperCase());
            JsonArray services = gson.toJsonTree((user.agency.services)).getAsJsonArray();
            store.setDataString(TSDatum.SERVICES, services.toString());
            if (data.careOptions != null) {
                JsonArray careOptions = gson.toJsonTree(data.careOptions).getAsJsonArray();
                store.setDataString(TSDatum.CARE_OPTIONS, careOptions.toString());
                user.setCareOpts(data.careOptions);
            }
            store.setDataString(TSDatum.SESSION_DATA, loginResponse.toString());
            List<SessionData> sessions =  data.sessionData;
            if (sessions != null && sessions.size() > 0) {
                for (SessionData session : sessions) {
                    for (TimeSheet timeSheet : session.timesheets) {
                        if (timeSheet.pca_signature != null && timeSheet.finished.equals(0)) {
                            timeSheetNum++;
                        }
                    }
                }
                user.setSessions(sessions);
            }
            else {
                ArrayList<SessionData> sessionArr = new ArrayList<>();
                user.setSessions(sessionArr);
            }
            store.setDataInt(TSDatum.TIME_SHEET_COUNT, timeSheetNum);
        }
        store.setDataInt(TSDatum.USER_TYPE, userTypeInt);
        return true;
    }


    public void setCurrentSessionRec(Context context, int sessionIndex) {
        clearSession(context);
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        TimeSheetStorage store = new TimeSheetStorage(context);
        final globalHCTS globalUser = (globalHCTS) context.getApplicationContext();
        User user = globalUser.user;
        SessionData session = user.sessions.get(sessionIndex);
        store.setDataInt(TSDatum.SESSION_INDEX, sessionIndex);

        List<TimeSheet> timeSheets = session.timesheets;
        TimeSheet timesheet = timeSheets.get(0);

        globalUser.needsGPS = !timesheet.recipient.skip_gps;
        globalUser.serviceChoice = globalHCTS.ServiceChoice.fromInt(session.service.service_number);
        store.setDataInt(TSDatum.CURRENT_SERVICE_INT, session.service.service_number);
        store.setDataString(TSDatum.TIME_SHEETS, timeSheets.toString());
        store.setDataInt(TSDatum.CURRENT_TIME_SHEET, timesheet.id);
        ArrayList<Integer> selected = new ArrayList<>();
        List<TScareOption> careOptions = timesheet.careOptionTimesheets;
        for (TScareOption element : careOptions) {
            selected.add(element.careOption.id);
        }
        store.setDataString(TSDatum.CURRENT_CARE_OPTIONS, selected.toString());
        if (session.timeIn != null) {
            TimeIn timeI = session.timeIn;
            store.setDataString(TSDatum.TIME_IN, timeI.timeIn);
            store.setDataString(TSDatum.TIME_IN_ADD, timeI.timeInAddress);
        }
        if (session.timeOut != null) {
            TimeOut timeO = session.timeOut;
            store.setDataString(TSDatum.TIME_OUT, timeO.timeOut);
            store.setDataString(TSDatum.TIME_OUT_ADD, timeO.timeOutAddress);
        }
        if (session.sharedCareLocation != null) {
            store.setDataInt(TSDatum.CURRENT_CARE_LOCATION, session.sharedCareLocation.id);
            store.setDataString(TSDatum.CURRENT_LOCATION_TEXT, session.sharedCareLocation.location.toUpperCase());
        }
        if (session.service != null) {
            store.setDataInt(TSDatum.CURRENT_SERVICES, session.service.id);
            store.setDataString(TSDatum.CURRENT_SERVICES_TEXT, session.service.service_name.toUpperCase());
        }
        if (session.id != null) {
            store.setDataInt(TSDatum.SESSION_ID, session.id);
        }
        if (session.continueTimesheetNumber != null) {
            store.setDataInt(TSDatum.PAGE_NUMBER, 6);
        }
        if (timesheet.file != null) {
            if (timesheet.file.timesheetPcaSig != null) {
                store.setDataInt(TSDatum.FILE_ROW_ID, timesheet.file.id);
                store.setDataString(TSDatum.PCA_SIG, timesheet.file.timesheetPcaSig);
                store.setDataBoolean(TSDatum.PCA_SIGNED, true);
                store.setDataString(TSDatum.PCA_SIG_TIME, timesheet.pca_signature.pcaSigTime);
            }
            if (timesheet.file.timesheetRecSig != null) {
                store.setDataInt(TSDatum.FILE_ROW_ID, timesheet.file.id);
                store.setDataString(TSDatum.REC_SIG, timesheet.file.timesheetRecSig);
                store.setDataBoolean(TSDatum.CLIENT_SIGNED, true);
                if (timesheet.recipient_signature != null) {
                    store.setDataString(TSDatum.CLIENT_SIG_TIME, timesheet.recipient_signature.recipientSigTime);
                }
            }
        }
        if (session.ratio != null) {
            store.setDataInt(TSDatum.CURRENT_RATIO, session.ratio.id);
            String[] ratioArr = session.ratio.ratio.split(":");
            int ratioInt = Integer.valueOf(ratioArr[1]);
            store.setDataInt(TSDatum.RATIO_INT, ratioInt);
        }
        JsonObject recipient = gson.toJsonTree(timesheet.recipient).getAsJsonObject();
        JsonArray recipients = new JsonArray();
        recipients.add(recipient);
        store.setDataString(TSDatum.RECIPIENTS, recipients.toString());
        store.setDataInt(TSDatum.CURRENT_RECIPIENT, timesheet.recipient.id);
        String rec_name = timesheet.recipient.first_name.toUpperCase() + " " + timesheet.recipient.last_name.toUpperCase();
        store.setDataString(TSDatum.CURRENT_RECIPIENT_NAME, rec_name);

        JsonObject sessionInfo = gson.toJsonTree(session).getAsJsonObject();
        store.setDataString(TSDatum.SESSION_DATA, sessionInfo.toString());

        store.setDataString(TSDatum.PCA_FIRST, session.pca.first_name.toUpperCase());
        store.setDataString(TSDatum.PCA_LAST, session.pca.last_name.toUpperCase());
        store.setDataString(TSDatum.PCA_UMPI, session.pca.umpi);
        store.setDataInt(TSDatum.PCA_ID, session.pca.id);
    }


    public void setCurrentSessionPCA(Context context, int sessionIndex) {
        clearSession(context);
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        TimeSheetStorage store = new TimeSheetStorage(context);
        store.setDataInt(TSDatum.SESSION_INDEX, sessionIndex);
        final globalHCTS globalUser = (globalHCTS) context.getApplicationContext();
        User user = globalUser.user;
        globalHCTS.ServiceChoice serviceChoice;
        if (user.sessions.size() > 0) {
            SessionData session = user.sessions.get(sessionIndex);
            int curPage = session.continueTimesheetNumber;
            Log.d("Current Page", session.continueTimesheetNumber.toString());
            if (session.service != null) {
                int serviceInt = session.service.service_number;
                globalUser.serviceChoice = globalHCTS.ServiceChoice.fromInt(serviceInt);
                store.setDataInt(TSDatum.CURRENT_SERVICE_INT, session.service.service_number);
            }
            else {
                globalUser.serviceChoice = globalHCTS.ServiceChoice.fromInt(1);
            }
            serviceChoice = globalUser.serviceChoice;
            if (session.timesheets != null && session.timesheets.size() > 0) {
                List<TimeSheet> timesheets = session.timesheets;
                store.setDataString(TSDatum.TIME_SHEETS, timesheets.toString());
                int i = timesheets.size() - 1;
                TimeSheet timesheet = timesheets.get(i);
                store.setDataInt(TSDatum.CURRENT_TIME_SHEET, timesheet.id);
                if (timesheet.file != null) {
                    store.setDataInt(TSDatum.FILE_ROW_ID, timesheet.file.id);

                    if (timesheet.file.timesheetPcaSig != null) {
                        store.setDataBoolean(TSDatum.PCA_SIGNED, true);
                        store.setDataString(TSDatum.PCA_SIG, timesheet.file.timesheetPcaSig);
                        store.setDataString(TSDatum.PCA_SIG_TIME, timesheet.pca_signature.pcaSigTime);
                        curPage = 6;
                    }
                    if (timesheet.file.timesheetRecSig != null) {
                        store.setDataBoolean(TSDatum.CLIENT_SIGNED, true);
                        store.setDataString(TSDatum.REC_SIG, timesheet.file.timesheetRecSig);
                        store.setDataString(TSDatum.CLIENT_SIG_TIME, timesheet.recipient_signature.recipientSigTime);
                        curPage = 6;
                    }
                }
                store.setDataInt(TSDatum.CURRENT_RECIPIENT, timesheet.recipient.id);
                String recName = timesheet.recipient.first_name.toUpperCase() + " " + timesheet.recipient.last_name.toUpperCase();
                store.setDataString(TSDatum.CURRENT_RECIPIENT_NAME, recName);
                globalUser.needsGPS = !timesheet.recipient.skip_gps;
                globalUser.needsVerif = !timesheet.recipient.skip_verification;
                if (timesheet.careOptionTimesheets != null && serviceChoice != null && serviceChoice.hasDL()) {
                    int careOptCount = 0;
                    ArrayList<Integer> selected = new ArrayList<>();
                    for (TScareOption element : timesheet.careOptionTimesheets) {
                        selected.add(element.careOption.id);
                        careOptCount++;
                    }
                    store.setDataString(TSDatum.CURRENT_CARE_OPTIONS, selected.toString());
                    if (careOptCount > 0) {
                        curPage = 6;
                    } else {
                        curPage = 5;
                    }
                }

            }

            if (session.timeIn != null) {
                store.setDataString(TSDatum.TIME_IN, session.timeIn.timeIn);
                store.setDataString(TSDatum.TIME_IN_ADD, session.timeIn.timeInAddress);
                store.setDataInt(TSDatum.EXPECTED_SHIFT_LENGTH, session.timeIn.estimatedShiftLength);
                store.setDataBoolean(TSDatum.PUNCHED_IN, true);
            } else {
                curPage = 4;
            }
            if (session.timeOut != null) {
                store.setDataString(TSDatum.TIME_OUT, session.timeOut.timeOut);
                store.setDataString(TSDatum.TIME_OUT_ADD, session.timeOut.timeOutAddress);
                store.setDataBoolean(TSDatum.PUNCHED_OUT, true);
            } else {
                curPage = 4;
            }
            if (session.sharedCareLocation != null) {
                store.setDataInt(TSDatum.CURRENT_CARE_LOCATION, session.sharedCareLocation.id);
                store.setDataString(TSDatum.CURRENT_LOCATION_TEXT, session.sharedCareLocation.location.toUpperCase());
            } else if (serviceChoice != null && serviceChoice.hasRatio()) {
                curPage = 2;
                Log.d("No SharedCareLocation", "true");
            }
            if (session.ratio != null) {
                store.setDataInt(TSDatum.CURRENT_RATIO, session.ratio.id);
                String[] ratioArr = session.ratio.ratio.split(":");
                int count = Integer.valueOf(ratioArr[1]);
                store.setDataInt(TSDatum.RATIO_INT, count);
            } else if (serviceChoice != null && serviceChoice.hasRatio()) {
                curPage = 2;
            }
            if (session.service != null) {
                globalUser.serviceChoice = globalHCTS.ServiceChoice.fromInt(session.service.service_number);
                store.setDataInt(TSDatum.CURRENT_SERVICES, session.service.id);
                store.setDataString(TSDatum.CURRENT_SERVICES_TEXT, session.service.service_name.toUpperCase());
            } else {
                curPage = 1;
            }
            if (session.id != null) {
                store.setDataInt(TSDatum.SESSION_ID, session.id);
            }
            if (session.continueTimesheetNumber != null) {
                store.setDataInt(TSDatum.PAGE_NUMBER, curPage);
            }
            int currentTimesheetNum = 1;
            if (session.currentTimesheetNumber != null) {
                currentTimesheetNum = session.currentTimesheetNumber;
            }
            Log.d("curPage", Integer.valueOf(curPage).toString());
            store.setDataInt(TSDatum.CURRENT_TIME_SHEET_NUM, currentTimesheetNum);
            JsonObject sessionInfo = gson.toJsonTree(session).getAsJsonObject();
            store.setDataString(TSDatum.SESSION_DATA, sessionInfo.toString());
        }
    }

    void clearSession(Context context) {
        TimeSheetStorage store = new TimeSheetStorage(context);
        store.setDataBoolean(TSDatum.LOGGED_IN, true);
        store.clearDatum(TSDatum.PAGE_NUMBER);
        store.clearDatum(TSDatum.TIME_SHEETS);
        store.clearDatum(TSDatum.DATE_ENDING);
        store.clearDatum(TSDatum.HOSPITAL_DATES);
        store.clearDatum(TSDatum.CURRENT_RATIO);
        store.clearDatum(TSDatum.CURRENT_SERVICES);
        store.clearDatum(TSDatum.CURRENT_CARE_LOCATION);
        store.clearDatum(TSDatum.CURRENT_CARE_OPTIONS);
        store.clearDatum(TSDatum.CURRENT_TIME_SHEET);
        store.clearDatum(TSDatum.EXPECTED_SHIFT_LENGTH);
        store.clearDatum(TSDatum.TIME_IN_PHOTO_ID);
        store.clearDatum(TSDatum.TIME_OUT_PHOTO_ID);
        store.clearDatum(TSDatum.SHARED_CARE_LOCATION);
        store.clearDatum(TSDatum.TIME_IN);
        store.clearDatum(TSDatum.TIME_OUT);
        store.clearDatum(TSDatum.TIME_IN_ADD);
        store.clearDatum(TSDatum.TIME_OUT_ADD);
        store.clearDatum(TSDatum.SESSION_DATA);
        store.clearDatum(TSDatum.SESSION_ID);
        store.clearDatum(TSDatum.FILE_ROW_ID);
        store.clearDatum(TSDatum.SESSION_FILE_ROW_ID);
        store.clearDatum(TSDatum.SELECTED_RECIPIENTS);
        store.setDataInt(TSDatum.FINISHED, 0);
        store.setDataBoolean(TSDatum.PUNCHED_OUT, false);
        store.setDataBoolean(TSDatum.PUNCHED_IN, false);
        store.setDataBoolean(TSDatum.PCA_SIGNED, false);
        store.setDataBoolean(TSDatum.CLIENT_SIGNED, false);
    }
}