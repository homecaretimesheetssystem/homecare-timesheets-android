package com.homecaretimesheet.apidemo;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.homecaretimesheet.apidemo.ApiService.AuthResponse;
import com.homecaretimesheet.apidemo.ApiService.DataService;
import com.homecaretimesheet.apidemo.ApiService.LoginService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {
	Context context;
	private int sdkVersion;
	private TimeSheetStorage store;
	private ApiService apiService;
	protected LoginService loginsend;
	protected DataService datafetch;
	private EditText EditUsername;
	private EditText EditPassword;
	private Button Login;
    private Dialog dialog;	
	private String api_token;
	private String username;
	private String passwd;
	private Boolean netChecked;
	private ResponseHandler responseHandler;
	public Boolean testing;
	public PermissionService permissionService;
	private int INET_PERMISSION = globalHCTS.INTERNET_REQUEST;
	private int NET_PERMISSION = globalHCTS.NETWORK_REQUEST;

	private static String clientId = AuthData.client;
	private static String clientSecret = AuthData.secret;
	private static String testId = AuthData.testClient;
	private static String testSecret = AuthData.testSecret;

	globalHCTS globals;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		store = new TimeSheetStorage(this);
		store.clearData();
		globals = (globalHCTS) getApplicationContext();
		testing = globals.testing;
		responseHandler = new ResponseHandler();
		sdkVersion = android.os.Build.VERSION.SDK_INT;
        context = MainActivity.this;
		globals.setPerms(context);
		permissionService = new PermissionService();
		setActiveTime();
		EditUsername = (EditText) findViewById(R.id.EditUsername);
		if (store.hasDataString(TSDatum.USERNAME)) {
			username = store.getDataString(TSDatum.USERNAME);
			EditUsername.setText(username);
		}

		EditUsername.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}

			@Override
			public void afterTextChanged(Editable s) {
				setActiveTime();
				username = EditUsername.getText().toString();
				updateAdvanceButton();
			}
		});
		
		EditPassword = (EditText) findViewById(R.id.EditPassword);

		EditPassword.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}

			@Override
			public void afterTextChanged(Editable s) {
				setActiveTime();
				passwd = EditPassword.getText().toString();
				updateAdvanceButton();
			}
		});
		
		Login = (Button) findViewById(R.id.login);
		
		Login.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setActiveTime();
				loginSubmit();
				Login.setEnabled(false);
			}
		});
		netChecked = false;
	}

	@Override
	public void onBackPressed() {
		finish();
	}

    @Override
    protected void onStart() {
        super.onStart();
			netChecked = checkNet();
    }

	public void getNetPermission() {
		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, NET_PERMISSION);
	}

	public void getINetPermission() {
		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, INET_PERMISSION);
	}


	public boolean checkNet() {
		boolean retCheck = false;
		if (permissionService.getNetworkPermission(context)) {
			Utils.verifyNetworkConnection(context, sdkVersion);
			retCheck = Utils.isNetworkConnected(context);
			if (!retCheck) {
				Toast.makeText(context, "No network connection", sdkVersion).show();
				updateAdvanceButton();
			}
			if (!permissionService.getInternetPermission(context)) {
				getINetPermission();
			}
		}
		else {
			getNetPermission();
		}
		return retCheck;
	}

	public void setActiveTime() {
		Integer time = (int) (long) System.currentTimeMillis();
		store.setDataInt(TSDatum.LAST_ACTIVITY, time);
	}

	private void updateAdvanceButton() {
		enableButton(Login, allFieldsEntered());
	}
	
	private Boolean allFieldsEntered() {
        return !(username == null || passwd == null || !netChecked);
	}
	
	private void enableButton(Button btn, Boolean enable) {
		if (enable) {
			btn.setTextColor(Color.WHITE);
			btn.setEnabled(true);
		} else {
			btn.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
			btn.setEnabled(false);
		}
	}
	
	@SuppressLint("InflateParams")
	private void loginSubmit() {
		setActiveTime();
		enableButton(Login, false);
        String grant_type = "password";
        apiService = new ApiService();
		loginsend = apiService.restAdapter.create(LoginService.class);
		Log.d("API NEW", apiService.new_api.toString());
		if (apiService.new_api) {
			ApiService.LoginServiceNew newlogin = apiService.restAdapter.create(ApiService.LoginServiceNew.class);
			ApiService.AuthBody auth = apiService.new AuthBody();
			auth.username = username;
			auth.password = passwd;
			auth.client_id = AuthData.testClient;
			newlogin.authenticate(auth,logResponse);
		}
		else if (testing) {
		    loginsend.authenticate(testId, testSecret, grant_type, username, passwd, logResponse);
		}
		else {
			loginsend.authenticate(clientId, clientSecret, grant_type, username, passwd, logResponse);
		}
		dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.loading);
    	dialog.setCancelable(false);
		dialog.show();
		if (dialog.getWindow() != null) {
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		}
		dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}
	
	Callback<AuthResponse> logResponse = new Callback<AuthResponse>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
				dialog.dismiss();
				updateAdvanceButton();
				return;
			}	
			else {
				Toast.makeText(context, "Login Failed", sdkVersion).show();
				dialog.dismiss();
			}
			updateAdvanceButton();
		}

		@Override
		public void success(AuthResponse authResponse, Response response) {
			api_token = authResponse.access_token;
			store.setDataString(TSDatum.API_TOKEN, api_token);
			getData();
		}
	};

	public void getData() {
		apiService = new ApiService(api_token);
		datafetch = apiService.restAdapter.create(DataService.class);
        datafetch.getData(dataResponse);
	}
	
	Callback<JsonObject> dataResponse = new Callback<JsonObject>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", sdkVersion).show();
				dialog.dismiss();
			}
			else {
				Toast.makeText(context, "Account Info Failed", sdkVersion).show();
				dialog.dismiss();
			}
		}
		@Override
		public void success(JsonObject loginResponse, Response response) {
			responseHandler.getResp(loginResponse, context);
			if (store.getDataInt(TSDatum.USER_TYPE) == 2 || store.getDataInt(TSDatum.RECIPIENT_COUNT) > 0) {
				String agencyImg = store.getDataString(TSDatum.AGENCY_ICON);
				setActiveTime();
				if (!agencyImg.equals("ERROR") && !store.getDataBoolean(TSDatum.HAS_IMG)) {
					String img_url = store.getDataString(TSDatum.AMAZON_URL) + agencyImg;
					img_url = img_url.replace("\"", "");
					new DownloadImageTask((ImageView) findViewById(R.id.agency_icon))
							.execute(img_url);
				} else {
					nextActivity();
				}
			}
			else {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

				alertDialogBuilder.setTitle("No Recipients Alert")
						.setMessage(R.string.no_recip_alert)
						.setCancelable(false)
						.setPositiveButton(R.string.cont, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								resetLogin();
								dialog.cancel();
							}
						});
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
			}
		}
	};
	
    public void nextActivity() {
		int userTypeInt = store.getDataInt(TSDatum.USER_TYPE);
    	setActiveTime();
    	if (userTypeInt > 1) {
			startActivity(new Intent(context, RepPDFActivity.class));
    	}
    	else {
    		Intent intent = new Intent(context, ContinueSelectActivity.class);
			intent.putExtra("FROM_ID", 1);
    		startActivity(intent);
    	}
    	finish();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	private String saveToInternalStorage(Bitmap bitmapImage){
		ContextWrapper cw = new ContextWrapper(getApplicationContext());
		File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
		File mypath=new File(directory,"agency.png");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(mypath);
			bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				assert fos != null;
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String localPath = directory.getAbsolutePath();
		store.setDataString(TSDatum.LOCAL_PATH, localPath);
		return localPath;
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				e.getStackTrace();
			}
			return mIcon11;
		}



		protected void onPostExecute(Bitmap result) {
			saveToInternalStorage(result);
			store.setDataBoolean(TSDatum.HAS_IMG, true);
            dialog.dismiss();
            nextActivity();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   @NonNull String permissions[], @NonNull int[] grantResults) {
		if (requestCode == INET_PERMISSION) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				updateAdvanceButton();
			} else {
				final AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(getString(R.string.app_name) + " requires Internet Permissions")
						.setCancelable(false)
						.setPositiveButton("Go to Permissions", new DialogInterface.OnClickListener() {
							public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
								Intent i = new Intent();
								i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
								i.addCategory(Intent.CATEGORY_DEFAULT);
								i.setData(Uri.parse("package:" + context.getPackageName()));
								i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
								i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
								context.startActivity(i);
							}
						});
				final AlertDialog alert = builder.create();
				alert.show();
			}
		}
		if (requestCode == NET_PERMISSION) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				netChecked = checkNet();
				updateAdvanceButton();
			} else {
				final AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(getString(R.string.app_name) + " requires Network Permissions")
						.setCancelable(false)
						.setPositiveButton("Go to Permissions", new DialogInterface.OnClickListener() {
							public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
								Intent i = new Intent();
								i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
								i.addCategory(Intent.CATEGORY_DEFAULT);
								i.setData(Uri.parse("package:" + context.getPackageName()));
								i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
								i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
								context.startActivity(i);
							}
						});
				final AlertDialog alert = builder.create();
				alert.show();
			}
		}
	}

	public void resetLogin() {
		Intent intent = new Intent(context, MainActivity.class);
		startActivity(intent);
		finish();
	}
}
