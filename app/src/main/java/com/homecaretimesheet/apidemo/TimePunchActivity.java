package com.homecaretimesheet.apidemo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonObject;
import com.homecaretimesheet.apidemo.ApiService.FileResp;
import com.homecaretimesheet.apidemo.ApiService.GetTimeInReq;
import com.homecaretimesheet.apidemo.ApiService.GetTimeInResp;
import com.homecaretimesheet.apidemo.ApiService.GetTimeOutReq;
import com.homecaretimesheet.apidemo.ApiService.GetTimeOutResp;
import com.homecaretimesheet.apidemo.ApiService.SendTimeInReq;
import com.homecaretimesheet.apidemo.ApiService.SendTimeInResp;
import com.homecaretimesheet.apidemo.ApiService.SendTimeOutReq;
import com.homecaretimesheet.apidemo.ApiService.SendTimeOutResp;
import com.homecaretimesheet.apidemo.ApiService.SetTimeSheet;
import com.homecaretimesheet.apidemo.ApiService.TimeSheetReq;
import com.homecaretimesheet.apidemo.ApiService.TimeSheetResp;
import com.homecaretimesheet.apidemo.ApiService.UpdateSession;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionReq;
import com.homecaretimesheet.apidemo.ApiService.UpdateSessionResp;
import com.homecaretimesheet.apidemo.ApiService.getTimeIn;
import com.homecaretimesheet.apidemo.ApiService.getTimeOut;
import com.homecaretimesheet.apidemo.ApiService.sendTimeIn;
import com.homecaretimesheet.apidemo.ApiService.sendTimeOut;
import com.homecaretimesheet.apidemo.ApiService.sendVerifImage;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

@SuppressLint("InflateParams")
public class TimePunchActivity extends AppCompatActivity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
	private TimeSheetStorage store;
	private int sdkVersion;
	private MenuItem menuNext = null;
	private Context context;
	private SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
	private SimpleDateFormat humanReadableFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm", Locale.US);
	private SimpleDateFormat textDateFormat = new SimpleDateFormat("hh:mm:ss aa", Locale.US);
	private SimpleDateFormat dateOnlyFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
	private TextView timeInView;
	private TextView timeOutView;
	private TextView timeCounter;
	private Date timeInSet = null;
	private Date timeOutSet = null;
	private Date timeOutCheck = null;
	private TimerTask countTask;
	private Location currentLocation;
	private String mLatitudeText = null;
	private String mLongitudeText = null;
	private JsonObject sendUpIn;
	private JsonObject sendUpOut;
	public Integer minCount = 0;
	public Integer hourCount = 0;
	public ApiService apiService;
	public getTimeIn timeInGet;
	public getTimeOut timeOutGet;
	public GetTimeInReq timeInId;
	public GetTimeOutReq timeOutId;
	public Integer timeInRowId = 0;
	public Integer timeOutRowId = 0;
	public sendTimeIn timeInSend;
	public sendTimeOut timeOutSend;
	public SendTimeInReq timeInData;
	public SendTimeOutReq timeOutData;
	public Integer fileRowId = 0;
	public UpdateSessionReq sendSession;
	public Integer serviceId;
//	public Boolean needVerif = false;
	public String verifFile = "verifImg";
//	public GetVerif getVerif;
//	public SetVerif setVerif;
	public int vId = 0;
	private static final int IMAGE_REQUEST = 1337;
	public TypedFile setOutput = null;
	public File getOutput = null;
	public boolean showingDialog = false;
	public Boolean punchedIn;
	public Boolean punchedOut;
	public Boolean checkedLoc = false;
	private GoogleApiClient mGoogleApiClient;
	private boolean needGpsService = false;
	private globalHCTS.ServiceChoice serviceChoice;

	private LocationRequest mLocationRequest;

	private int CONNECT_REQUEST = 10101;
	private int TIME_IN_REQUEST = 10102;
	private int UPDATE_REQUEST = 10103;
	private int STORAGE_REQUEST = 10104;
	private int CAMERA_REQUEST = 11014;
	private int billable;
	private int totalShift;
	globalHCTS globals;
	String colon = ":";
	PermissionService permService;

	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		setContentView(R.layout.activity_time_punch);
		context = TimePunchActivity.this;
		sdkVersion = android.os.Build.VERSION.SDK_INT;
		store = new TimeSheetStorage(this);
		serverDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		globals = (globalHCTS) getApplicationContext();
		permService = new PermissionService();
		serviceChoice = globals.serviceChoice;
		needGpsService = globals.needsGPS;
		textDateFormat.setTimeZone(TimeZone.getDefault());
		serviceId = store.getDataInt(TSDatum.CURRENT_SERVICES);
		setActiveTime();
		timeInView = (TextView) findViewById(R.id.time_in);
		timeOutView = (TextView) findViewById(R.id.time_out);
		timeCounter = (TextView) findViewById(R.id.counter);
		apiService = new ApiService(store.getDataString(TSDatum.API_TOKEN));
		String weekDay;
		colon = getString(R.string.colon);
		SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);
		Calendar calendar = Calendar.getInstance();
		weekDay = dayFormat.format(calendar.getTime());
		punchedIn = store.getDataBoolean(TSDatum.PUNCHED_IN);
		punchedOut = store.getDataBoolean(TSDatum.PUNCHED_OUT);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(weekDay);
			getSupportActionBar().setHomeButtonEnabled(!punchedIn);
		}
		if (punchedIn) {
			String timeIn = store.getDataString(TSDatum.TIME_IN);
			try {
				timeInSet = serverDateFormat.parse(timeIn);
				addTimeIn(timeInSet);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			if (punchedOut) {
				String timeOut = store.getDataString(TSDatum.TIME_OUT);
				try {
					timeOutSet = serverDateFormat.parse(timeOut);
					addTimeOut(timeOutSet);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				editElapsed();
			} else {
				if (!checkTime()) {
					addTimeOutButton();
					addCounter(timeInSet);
				} else {
					checkTimeOut();
				}
			}
		} else {
			timeInView.setClickable(true);
			timeInView.setBackground(ContextCompat.getDrawable(context, R.drawable.border));
			timeInView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!needGpsService || currentLocation != null) {
						buildExpectedShift();
					} else {
						createLocationRequest();
						if (checkedLoc) {
							buildExpectedShift();
						} else {
							showLocationAlert();
						}
					}
				}
			});
		}
		if (needGpsService) {
			buildGoogleApiClient();
			createLocationRequest();

			final LocationManager manager2 = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			if (!manager2.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				buildAlertMessageNoGps();
			}
		}
		fileRowId = store.getDataInt(TSDatum.SESSION_FILE_ROW_ID);
	}



	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();
	}

	@SuppressLint("InflateParams")
	public void buildExpectedShift() {
		setActiveTime();
		store.setDataBoolean(TSDatum.PUNCHED_IN, true);
		punchedIn = true;
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		LayoutInflater inflation = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View dialogLayout = inflation.inflate(R.layout.dialog_expected_shift, null);
		Spinner hourSpinner = (Spinner) dialogLayout.findViewById(R.id.hours);
		final Spinner minSpinner = (Spinner) dialogLayout.findViewById(R.id.minutes);
		final Button saveButton = (Button) dialogLayout.findViewById(R.id.done);
		saveButton.setTextColor(ContextCompat.getColor(context, android.R.color.white));
		String[] hourList = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};
		String[] fullMins = new String[]{"00", "15", "30", "45"};
		String[] zeroMins = new String[]{"00"};
		String[] nonZeroMins = new String[]{"15", "30", "45"};
		ArrayAdapter<String> hour_adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, hourList);
		final ArrayAdapter<String> full_min_adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, fullMins);
		final ArrayAdapter<String> zero_min_adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, zeroMins);
		final ArrayAdapter<String> non_zero_min_adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, nonZeroMins);


		alertDialogBuilder.setView(dialogLayout);

		alertDialogBuilder.setCancelable(false);
		final AlertDialog alertDialog = alertDialogBuilder.create();

		hourSpinner.setAdapter(hour_adapter);
		hourSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				String selection = (String) parent.getItemAtPosition(position);
				hourCount = Integer.valueOf(selection);
				if (hourCount.equals(24)) {
					minSpinner.setAdapter(zero_min_adapter);
				} else if (hourCount.equals(0)) {
					minSpinner.setAdapter(non_zero_min_adapter);
				} else {
					minSpinner.setAdapter(full_min_adapter);
				}
				setActiveTime();
				saveButton.setEnabled(true);

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		minSpinner.setAdapter(non_zero_min_adapter);
		minSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				String selection = (String) parent.getItemAtPosition(position);
				minCount = Integer.valueOf(selection);
				saveButton.setEnabled(true);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		saveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setActiveTime();
				Integer fullCount = ((hourCount * 60) + minCount) * 60;
				storeCount(fullCount);
				setTimeIn();
				alertDialog.dismiss();
			}
		});
		alertDialog.show();
	}

	public void showLocationAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("No Location Detected")
				.setMessage(R.string.get_location)
				.setCancelable(false)
				.setPositiveButton(R.string.cont, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.major, menu);
		menuNext = menu.findItem(R.id.next_bar);
		if (punchedOut) {
			menuNext.setEnabled(true);
		}
		return true;
	}

	public void readySubmit() {
		if (menuNext != null) {
			updateAdvanceButton(menuNext, punchedOut);
		}
	}

	public void updateAdvanceButton(MenuItem button, Boolean enable) {
		button.setEnabled(enable);
	}

	public void nextActivity() {
		setActiveTime();
		if (serviceChoice.hasDL()) {
			startActivity(new Intent(context, SelectCareOptionsActivity.class));
			store.setDataInt(TSDatum.PAGE_NUMBER, 5);
		}
		else {
			startActivity(new Intent(context, PDFShowActivity.class));
			store.setDataInt(TSDatum.PAGE_NUMBER, 6);
		}
		finish();
	}

	public void addTimeIn(Date timeinSet) {
		String timeInString = textDateFormat.format(timeinSet);
        String timeInC = getString(R.string.time_in_c) + " " + timeInString;
		timeInView.setText(timeInC);
		timeInView.setBackgroundResource(0);
	}

	public void sendPunch(Boolean inOut) {
		if (inOut) {
			timeInSend();
			if (getSupportActionBar() != null) {
				getSupportActionBar().setHomeButtonEnabled(false);
			}
		} else {
			timeOutSend();
		}
	}

	public void addTimeOut(Date timeoutSet) {
		String timeOutString = textDateFormat.format(timeoutSet);
        String timeOutC = getString(R.string.time_out_c) + " " + timeOutString;
		timeOutView.setText(timeOutC);
		timeOutView.setVisibility(View.VISIBLE);
		timeOutView.setBackgroundResource(0);
	}


	public void addTimeOutButton() {
		timeOutView = (TextView) findViewById(R.id.time_out);
		timeOutView.setBackground(ContextCompat.getDrawable(context, R.drawable.border));
		timeOutView.setClickable(true);
		timeOutView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!needGpsService) {
					checkSureDialog();
				} else {
					if (currentLocation != null) {
						checkSureDialog();
					} else {
						createLocationRequest();
						showLocationAlert();
					}
				}
			}
		});
		timeOutView.setVisibility(View.VISIBLE);
	}

	public void checkSureDialog() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		alertDialogBuilder.setTitle("Time Out")
				.setMessage(R.string.surepunch)
				.setCancelable(false)
				.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						actuallyPunchOut();
					}
				})

				.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public void actuallyPunchOut() {
		timeOutCheck = new Date(System.currentTimeMillis());
		addElapsed();
		if (checkTime()) {
			checkTimeOut();
		}
		else {
			timeOutSet = timeOutCheck;
			setTimeOut();
		}
	}

    public boolean checkTime() {
        Integer lengthInSecs = store.getDataInt(TSDatum.EXPECTED_SHIFT_LENGTH);
        Date endTime = new Date(System.currentTimeMillis());
        long diff = (endTime.getTime() - timeInSet.getTime()) / 1000;
        return (diff > lengthInSecs);
    }

	protected void onStart() {
		super.onStart();
		if (needGpsService) {
			if (mGoogleApiClient != null) {
				mGoogleApiClient.connect();
			}
		}
	}

	protected void onStop() {
		if (mGoogleApiClient != null) {
			mGoogleApiClient.disconnect();
		}
		super.onStop();
	}

    public void checkTimeOut() {
        if (!showingDialog) {
			Integer lengthInSecs = store.getDataInt(TSDatum.EXPECTED_SHIFT_LENGTH);
			Date endTime = new Date(System.currentTimeMillis());
			if (checkTime()) {
				long diff = (long) lengthInSecs;
				Calendar c = Calendar.getInstance();
				c.setTimeInMillis(timeInSet.getTime() + (diff * 1000));
				timeOutCheck = c.getTime();
			} else {
				timeOutCheck = endTime;
			}
			showingDialog = true;

			final String showTime = humanReadableFormat.format(timeOutCheck);
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
			alertDialogBuilder.setTitle("Time Out");
			alertDialogBuilder
					.setMessage("You have been clocked out at " + showTime)
					.setCancelable(false)
					.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							timeOutSet = timeOutCheck;
							editElapsed();
							setTimeOut();
							dialog.dismiss();
						}
					})
					.setNegativeButton("Edit", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							editTimeOut();
							dialog.dismiss();
						}
					});

			AlertDialog alertDialog = alertDialogBuilder.create();
			alertDialog.show();
		}
    }

    public void editTimeOut() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflation = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogLayout = inflation.inflate(R.layout.date_time_dialog, null);
        final TimePicker tp = (TimePicker) dialogLayout.findViewById(R.id.timePicker1);
                tp.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
					@Override
					public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
					//	setTime(hourOfDay + ":" + minute);
					}
				});
        final DatePicker dp = (DatePicker) dialogLayout.findViewById(R.id.datePicker1);
        dp.setMinDate(timeInSet.getTime());
        dp.setMaxDate(timeOutCheck.getTime());
		final String inDate = dateOnlyFormat.format(timeInSet);
		final String outDate = dateOnlyFormat.format(timeOutCheck);
		if (inDate.equals(outDate)) {
			dp.setVisibility(View.GONE);
		}
        Button cancel = (Button) dialogLayout.findViewById(R.id.cancel_button);
        cancel.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));

        Button doneEdit = (Button) dialogLayout.findViewById(R.id.proceed_button);
        doneEdit.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));

        alertDialogBuilder.setView(dialogLayout);

        alertDialogBuilder.setCancelable(false);
        final AlertDialog alertDialog = alertDialogBuilder.create();

        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                checkTimeOut();
            }
        });
        doneEdit.setOnClickListener(new OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
				String editedTime;
				if (inDate.equals(outDate)) {
					editedTime = dateOnlyFormat.format(timeInSet) + "T";
				}
				else {
					int monthInt = dp.getMonth() + 1;
					String monthStr;
					if (monthInt < 10) {
						monthStr = "0" + monthInt;
					} else {
						monthStr = Integer.valueOf(monthInt).toString();
					}
					int dayInt = dp.getDayOfMonth();
					String dayStr;
					if (dayInt < 10) {
						dayStr = "0" + dayInt;
					} else {
						dayStr = Integer.valueOf(dayInt).toString();
					}
					editedTime = dp.getYear() + "-" + (monthStr) + "-" + dayStr + "T";
				}
                int hourInt;
                int minInt;
                if (sdkVersion > 22) {
                    hourInt = tp.getHour();
                    minInt = tp.getMinute();
                } else {
                    //noinspection deprecation
                    hourInt = tp.getCurrentHour();
                    //noinspection deprecation
                    minInt = tp.getCurrentMinute();
                }
                String hourStr;
                if (hourInt < 10) {
                    hourStr = "0" + hourInt;
                } else {
                    hourStr = "" + hourInt;
                }
                String minStr;
                if (minInt < 10) {
                    minStr = "0" + minInt;
                } else {
                    minStr = "" + minInt;
                }
                TimeZone tz = TimeZone.getDefault();
                Date now = new Date();
                int offsetFromUtc = tz.getOffset(now.getTime()) / 3600000;
                String m2tTimeZoneIs = Integer.toString(offsetFromUtc);
                editedTime += hourStr + ":" + minStr + ":00" + m2tTimeZoneIs;
                Date editSet = null;
                try {
                    editSet = serverDateFormat.parse(editedTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (editSet == null) {
                    Toast.makeText(context, "Invalid Time Entered", Toast.LENGTH_SHORT).show();
                }
                else if (checkEdit(editSet)) {
                    timeOutCheck = editSet;
                    checkTimeOut();
                    alertDialog.dismiss();
                } else {
                    String inStr = textDateFormat.format(timeInSet);
                    String nowStr = textDateFormat.format(new Date());
                    Toast.makeText(context, "Time Punch must be after " + inStr + " and before " + nowStr, Toast.LENGTH_SHORT).show();
                }
            }
        });
        alertDialog.show();
    }


    public boolean checkEdit(Date newTime) {
        boolean retBool = false;
        Date now = new Date();
        if (now.getTime() > newTime.getTime() && newTime.getTime() > timeInSet.getTime()) {
           retBool = true;
        }
        return retBool;
    }

	public void setTimeIn() {
		Calendar c = Calendar.getInstance();

		timeInSet = c.getTime();
		String inTime = serverDateFormat.format(timeInSet);
		sendUpIn = new JsonObject();
		if (needGpsService) {
			String addy = "";
			if (currentLocation != null) {
				addy = globals.getAddress(context, currentLocation);
				sendUpIn.addProperty("latitude", mLatitudeText);
				sendUpIn.addProperty("longitude", mLongitudeText);
			}
			sendUpIn.addProperty("timeInAddress", addy);
			sendUpIn.addProperty("timeInPictureTime", inTime);
			store.setDataString(TSDatum.TIME_IN_ADD, addy);
		}
		sendUpIn.addProperty("timeIn", inTime);
		sendUpIn.addProperty("estimatedShiftLength", store.getDataInt(TSDatum.EXPECTED_SHIFT_LENGTH));
        store.setDataString(TSDatum.TIME_IN, inTime);
		store.setDataBoolean(TSDatum.PUNCHED_IN, true);
		punchedIn = true;
		sendPunch(true);
		readySubmit();
	}

	public void setTimeOut() {
		String outTime = serverDateFormat.format(timeOutSet);
		sendUpOut = new JsonObject();
		if (needGpsService) {
			String addy = "";
			if (currentLocation != null) {
				addy = globals.getAddress(context, currentLocation);
				sendUpOut.addProperty("latitude", currentLocation.getLatitude());
				sendUpOut.addProperty("longitude", currentLocation.getLongitude());
			}
			sendUpOut.addProperty("timeOutPictureTime", outTime);
			sendUpOut.addProperty("timeOutAddress", addy);
			store.setDataString(TSDatum.TIME_OUT_ADD, addy);
		}

		sendUpOut.addProperty("billableHours", billable);
		sendUpOut.addProperty("totalHours", totalShift);
		sendUpOut.addProperty("timeOut", outTime);
        store.setDataString(TSDatum.TIME_OUT, outTime);
		store.setDataBoolean(TSDatum.PUNCHED_OUT, true);
		punchedOut = true;
		sendPunch(false);
	}


	public void addCounter(final Date timeIn) {
		timeCounter.setVisibility(View.VISIBLE);
		GetElapsedTime(timeIn);
		Timer T = new Timer();
		countTask = new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {
						GetElapsedTime(timeIn);
					}
				});
			}
		};
		T.scheduleAtFixedRate(countTask, 500, 250);
	}


	public void GetElapsedTime(Date startTime) {
		Date endTime = new Date(System.currentTimeMillis());
		long diff = (endTime.getTime() - startTime.getTime()) / 1000;
		long diffSeconds = diff % 60;
		long diffMinutes = diff / 60 % 60;
		long diffHours = diff / (60 * 60) % 24;
		setCounter(diffSeconds, diffMinutes, diffHours);
        if (checkTime()) {
          checkTimeOut();
        }
	}

	public void setCounter(long seconds, long minutes, long hours) {
		final String second;
		final String minute;
		final String hour = Integer.valueOf((int) (long) hours).toString();
		if (seconds > 10) {
			second = Integer.valueOf((int) (long) seconds).toString();
		} else if (seconds == 0) {
			second = "00";
		} else {
			second = "0" + Integer.valueOf((int) (long) seconds).toString();
		}
		if (minutes > 10) {
			minute = Integer.valueOf((int) (long) minutes).toString();
		} else if (minutes == 0) {
			minute = "00";
		} else {
            minute = "0" + Integer.valueOf((int) (long) minutes).toString();
        }
        String elapsed = getString(R.string.elapsed) + " " + hour + colon + minute + colon + second;
		timeCounter.setText(elapsed);
	}

	public void storeCount(Integer count) {
		store.setDataInt(TSDatum.EXPECTED_SHIFT_LENGTH, count);
	}

	public void addElapsed() {
		if (countTask != null) {
			countTask.cancel();
		}
        long shiftLength = timeOutCheck.getTime() - timeInSet.getTime();
		Integer fullShift = ((int) (long) shiftLength) / 1000;
        setElapsed(fullShift);
        Integer lengthInSecs = store.getDataInt(TSDatum.EXPECTED_SHIFT_LENGTH);
        if (fullShift > lengthInSecs) {
            fullShift = lengthInSecs;
		}
        setBillable(fullShift);
	}

    public void editElapsed() {
        long shiftLength = timeOutSet.getTime() - timeInSet.getTime();
        Integer fullShift = ((int) (long) shiftLength) / 1000;
        setElapsed(fullShift);
        setBillable(fullShift);
    }

    public void setElapsed(Integer shiftLeng) {
        timeCounter.setVisibility(View.VISIBLE);
        Integer seconds = shiftLeng % 60;
        Integer minutes = shiftLeng / 60 % 60;
        Integer hours = shiftLeng / (60 * 60);
        String hour = hours.toString();
        String minute;
        if (minutes > 10) {
            minute = minutes.toString();
        } else if (minutes == 0) {
            minute = "00";
        } else {
            minute = "0" + minutes.toString();
        }
        String second;
        if (seconds > 10) {
            second = seconds.toString();
        }
        else if (seconds == 0) {
            second = "00";
        }
        else {
            second = "0" + seconds.toString();
        }
        String setTime = getString(R.string.shift_length) + colon + " " + hour + colon + minute + colon + second;
        timeCounter.setText(setTime);
    }

    public void setBillable (Integer shiftLeng) {
        TextView billing = (TextView) findViewById(R.id.billable);
        billing.setVisibility(View.VISIBLE);
        Integer minutes = (shiftLeng / 60 % 60) - (shiftLeng / 60 % 15);
        Integer hours = shiftLeng / (60 * 60);
        String hour = hours.toString();
        String minute;
        if (minutes > 10) {
            minute = minutes.toString();
        } else if (minutes == 0) {
            minute = "00";
        } else {
            minute = "0" + minutes.toString();
        }
        String setTime = getString(R.string.billable) + colon + " " + hour + colon + minute;
		int billMins = 0;
		if (minutes > 0) {
			billMins = minutes/60;
		}
		totalShift = shiftLeng;
		billable = hours + billMins;
        billing.setText(setTime);
    }

	@Override
	protected void onPause() {
		super.onPause();
		if (countTask != null) {
			countTask.cancel();
		}
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.next_bar) {
			nextActivity();
			return true;
		}
		if (id == android.R.id.home) {
			onBackPressed();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		Intent intent;
		if (!punchedIn) {
			intent = new Intent(context, SelectRecipientActivity.class);
		}
		else {
			intent = new Intent(context, ContinueSelectActivity.class);
		}
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}

	Callback<GetTimeInResp> getTimeInId = new Callback<GetTimeInResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(GetTimeInResp timeInResp, Response response) {
			timeInRowId = timeInResp.createdTimeIn.id;
			updateInSession();
		}
	};

	Callback<GetTimeOutResp> getTimeOutId = new Callback<GetTimeOutResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
		    }
            else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(GetTimeOutResp timeOutResp, Response response) {
			timeOutRowId = timeOutResp.createdTimeOut.id;
			sendTimeOutData(timeOutRowId);
		}
	};

	Callback<SendTimeInResp> timeInResp = new Callback<SendTimeInResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(SendTimeInResp timeInResp, Response response) {
			addTimeIn(timeInSet);
			addTimeOutButton();
			addCounter(timeInSet);
			checkSeconds(timeInResp.seconds);
		}
	};

	public void updateInSession() {
		UpdateSession updateSession = apiService.restAdapter.create(UpdateSession.class);
		sendSession = apiService.new UpdateSessionReq();
		JsonObject sessionData = new JsonObject();
		sessionData.addProperty("timeIn", timeInRowId);
		sessionData.addProperty("continueTimesheetNumber", 4);
		sendSession.homecare_homecarebundle_sessiondata = sessionData;
		updateSession.updateSession(store.getDataInt(TSDatum.SESSION_ID), sendSession, updateInResponse);
	}

	Callback<UpdateSessionResp> updateInResponse = new Callback<UpdateSessionResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(UpdateSessionResp sessionResponse, Response response) {
			Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
			sendTimeInData(timeInRowId);
		}
	};


	Callback<SendTimeOutResp> timeOutResp = new Callback<SendTimeOutResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(SendTimeOutResp timeOutResp, Response response) {
			addTimeOut(timeOutSet);
			dataOutSubmit();
			store.setDataBoolean(TSDatum.PUNCHED_OUT, true);
			punchedOut = true;
		}
	};

	public void checkSeconds(int numSeconds) {
		if (numSeconds > 144000) {
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

			alertDialogBuilder.setTitle("Shift Length Alert")
					.setMessage(R.string.estover)
					.setCancelable(false)
					.setPositiveButton(R.string.cont, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});
			AlertDialog alertDialog = alertDialogBuilder.create();
			alertDialog.show();
		}
	}

	public void sendTimeInData(Integer rowId) {
		timeInSend = apiService.restAdapter.create(sendTimeIn.class);
		timeInData = apiService.new SendTimeInReq();
		timeInData.homecare_homecarebundle_timein = sendUpIn;
		timeInSend.sendTimeInData(rowId, timeInData, timeInResp);
	}

	public void dataOutSubmit() {
		UpdateSession updateSession = apiService.restAdapter.create(UpdateSession.class);
		sendSession = apiService.new UpdateSessionReq();
		Integer pageNum = 4;
		JsonObject sessionData = new JsonObject();
		sessionData.addProperty("timeOut", timeOutRowId);
		sessionData.addProperty("dateEnding", store.getDataString(TSDatum.TIME_OUT));
		sessionData.addProperty("continueTimesheetNumber", pageNum);
		sendSession.homecare_homecarebundle_sessiondata = sessionData;
		updateSession.updateSession(store.getDataInt(TSDatum.SESSION_ID), sendSession, updateOutResponse);
	}

	Callback<UpdateSessionResp> updateOutResponse = new Callback<UpdateSessionResp>() {
		@Override
		public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
					returnToLogin();
				}
			}
		}

		@Override
		public void success(UpdateSessionResp sessionResponse, Response response) {
			Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
			readySubmit();
		}

	};


	public void sendTimeOutData(Integer rowId) {
		timeOutSend = apiService.restAdapter.create(sendTimeOut.class);
		timeOutData = apiService.new SendTimeOutReq();
		timeOutData.homecare_homecarebundle_timeout = sendUpOut;
		timeOutSend.sendTimeOutData(rowId, timeOutData, timeOutResp);
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this,	new String[]{Manifest.permission.ACCESS_FINE_LOCATION},	CONNECT_REQUEST);
			return;
		}
		createLocationRequest();
		startLocationUpdates();
		currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if (currentLocation != null) {
			mLatitudeText = String.valueOf(currentLocation.getLatitude());
			mLongitudeText = String.valueOf(currentLocation.getLongitude());
		}
		else {
			LocationServices.FusedLocationApi.flushLocations(mGoogleApiClient);
			currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		}

	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   @NonNull String permissions[], @NonNull int[] grantResults) {
		if (requestCode == CONNECT_REQUEST) {
			if (grantResults.length > 0	&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				mGoogleApiClient.connect();
			} else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == TIME_IN_REQUEST) {
			if (grantResults.length > 0	&& grantResults[0] != PackageManager.PERMISSION_GRANTED) {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == UPDATE_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				startLocationUpdates();
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Location");
				alert.show();
			}
		}
		else if (requestCode == STORAGE_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				getImage(verifFile, IMAGE_REQUEST);
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Storage");
				alert.show();
			}
		}
		else if (requestCode == CAMERA_REQUEST) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				getImage(verifFile, IMAGE_REQUEST);
			}
			else {
				AlertDialog alert = globals.buildAlertNoPermission(context, "Camera");
				alert.show();
			}
		}
	}

	public void timeInSend() {
		timeInGet = apiService.restAdapter.create(getTimeIn.class);
		timeInId = apiService.new GetTimeInReq();
		timeInGet.createTimeIn(timeInId, getTimeInId);
	}

	public void timeOutSend() {
		timeOutGet = apiService.restAdapter.create(getTimeOut.class);
		timeOutId = apiService.new GetTimeOutReq();
		timeOutGet.createTimeOut(timeOutId, getTimeOutId);
	}

	@Override
	public void onConnectionSuspended(int arg0) {	}

	@Override
	public void onConnectionFailed(@Nullable ConnectionResult arg0) {
		Toast.makeText(context, "Location Service Failed", Toast.LENGTH_SHORT).show();
		mGoogleApiClient.connect();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Integer restartTime = (int) (long) System.currentTimeMillis() - 120000;
		Integer lastTime = store.getDataInt(TSDatum.LAST_ACTIVITY);
		if (lastTime < restartTime) {
			returnToLogin();
		}
        else {
            Intent intent = new Intent(context, TimePunchActivity.class);
            startActivity(intent);
            finish();
        }
		if (mGoogleApiClient.isConnected()) {
			startLocationUpdates();
		}
		else {
			buildGoogleApiClient();
			createLocationRequest();
		}
	}

	protected void createLocationRequest() {
		int UPDATE_INTERVAL = 10000; // 10 sec
		int FATEST_INTERVAL = 5000; // 5 sec
		int DISPLACEMENT = 100; // 100 meters
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT); // 100 meters
	}

	public void returnToLogin() {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
	}

	public void setActiveTime() {
		Integer time = (int) (long) System.currentTimeMillis();
		store.setDataInt(TSDatum.LAST_ACTIVITY, time);
	}

	protected void startLocationUpdates() {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, UPDATE_REQUEST);
			return;
		}
		LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
	public void onLocationChanged(Location location) {
	    currentLocation = location;
		if (currentLocation != null) {
		    mLatitudeText = String.valueOf(currentLocation.getLatitude());
			mLongitudeText = String.valueOf(currentLocation.getLongitude());
        }
    }

	public void showDisclaim() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		LayoutInflater inflation = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View dialogLayout = inflation.inflate(R.layout.dialog_image_remind, null);
			
		alertDialogBuilder.setView(dialogLayout);
		alertDialogBuilder.setCancelable(false);
		final Button saveButton = (Button) dialogLayout.findViewById(R.id.done);
        saveButton.setEnabled(true);
		final AlertDialog alertDialog = alertDialogBuilder.create();
		saveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
			    getImage(verifFile, IMAGE_REQUEST);
			    alertDialog.dismiss();
		    }
        });
		alertDialog.show();
	}

    public void setVerifTimesheet() {
        SetTimeSheet saveTimeSheet = apiService.restAdapter.create(SetTimeSheet.class);
        TimeSheetReq saveTimesheetReq = apiService.new TimeSheetReq();
        JsonObject timeSheetData = new JsonObject();
        timeSheetData.addProperty("verification", vId);
        saveTimesheetReq.homecare_homecarebundle_timesheet = timeSheetData;
        int timeSheetId = store.getDataInt(TSDatum.CURRENT_TIME_SHEET);
        saveTimeSheet.setTimeSheet(timeSheetId, saveTimesheetReq, saveVTimesheetResp);
    }

    Callback<TimeSheetResp> saveVTimesheetResp = new Callback<TimeSheetResp>() {
        @Override
        public void failure(RetrofitError code) {
            if (code.isNetworkError()) {
                Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
                Response r = code.getResponse();
                if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
            }
        }

        @Override
        public void success(TimeSheetResp sheetResponse, Response response) {
        }
    };

	protected void sendVImage() {
		sendVerifImage sendupOut = apiService.restAdapter.create(sendVerifImage.class);
		sendupOut.sendUpOutImage(vId, setOutput, vImageResp);
	}

    Callback<FileResp> vImageResp = new Callback<FileResp>() {

		@Override
	    public void failure(RetrofitError code) {
			if (code.isNetworkError()) {
				Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(context, "Save Failed", Toast.LENGTH_SHORT).show();
				Response r = code.getResponse();
				if (r != null && r.getStatus() == 401) {
                    returnToLogin();
                }
            }
        }

		@Override
		public void success(FileResp inFileResp, Response response) {
                    setVerifTimesheet();
					}			
    };

	public void getImage(String fileName, Integer ReqCode) {
		if (!permService.getStoragePermission(context)) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_REQUEST);
			return;
		}
		else if (!permService.getCameraPermission(context)) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);
			return;
		}
		Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
	    File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
	            //noinspection ResultOfMethodCallIgnored
        dir.mkdirs();
	    File output = new File(dir, fileName);
		if (output.exists()) {
	    	//noinspection ResultOfMethodCallIgnored
			output.delete();
		}
		getOutput = output;
		setOutput = new TypedFile("multipart/form-data", output);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));
		intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
		startActivityForResult(intent, ReqCode);
    }
				
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (getOutput.exists()) {
                    sendVImage();
                } else {
                    showDisclaim();
                }
            }
        }
    }

	private void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
				.setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
						startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
					}
				});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
    protected void onResume() {
		super.onResume();
    }
}